package org.bdware.analysis;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.tree.AbstractInsnNode;

public class BasicBlock {
    public List<AbstractInsnNode> list;
    public int blockID;
    public int lineNum = -1;
    public int insnCount;

    public BasicBlock(int id) {
        list = new ArrayList<>();
        blockID = id;
    }

    public void add(AbstractInsnNode insn) {
        list.add(insn);
    }

    public int size() {

        return list.size();
    }

    public List<AbstractInsnNode> getInsn() {
        return list;
    }

    boolean inList = false;

    public AbstractInsnNode lastInsn() {
        return list.get(list.size() - 1);
    }

    public void setLineNum(int line) {
        lineNum = line;
    }

    public boolean inList() {
        return inList;
    }

    public void setInList(boolean b) {
        inList = b;
    }
}
