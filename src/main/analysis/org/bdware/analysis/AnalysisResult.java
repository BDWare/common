package org.bdware.analysis;

import org.objectweb.asm.tree.AbstractInsnNode;

public abstract class AnalysisResult {
    public AnalysisResult() {}

    public abstract AnalysisResult merge(AbstractInsnNode insn);

    public abstract void printResult();

    public abstract boolean covers(AnalysisResult result);

    public abstract void mergeResult(AnalysisResult r);

    public abstract AnalysisResult clone();
}
