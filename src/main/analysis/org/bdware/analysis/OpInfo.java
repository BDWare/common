package org.bdware.analysis;

public enum OpInfo implements CFType {
    NOP(0x00, "nop", kInstrCanContinue, 0), // --
    ACONST_NULL(0x01, "aconst_null", kInstrCanContinue, 1), // --
    ICONST_M1(0x02, "iconst_m1", kInstrCanContinue, 1), // --
    ICONST_0(0x03, "iconst_0", kInstrCanContinue, 1), // --
    ICONST_1(0x04, "iconst_1", kInstrCanContinue, 1), // --
    ICONST_2(0x05, "iconst_2", kInstrCanContinue, 1), // --
    ICONST_3(0x06, "iconst_3", kInstrCanContinue, 1), // --
    ICONST_4(0x07, "iconst_4", kInstrCanContinue, 1), // --
    ICONST_5(0x08, "iconst_5", kInstrCanContinue, 1), // --
    LCONST_0(0x09, "lconst_0", kInstrCanContinue, 2), // --
    LCONST_1(0x0a, "lconst_1", kInstrCanContinue, 2), // --
    FCONST_0(0x0b, "fconst_0", kInstrCanContinue, 1), // --
    FCONST_1(0x0c, "fconst_1", kInstrCanContinue, 1), // --
    FCONST_2(0x0d, "fconst_2", kInstrCanContinue, 1), // --
    DCONST_0(0x0e, "dconst_0", kInstrCanContinue, 2), // --
    DCONST_1(0x0f, "dconst_1", kInstrCanContinue, 2), // --
    BIPUSH(0x10, "bipush", kInstrCanContinue, 1), // --
    SIPUSH(0x11, "sipush", kInstrCanContinue, 1), // --
    LDC(0x12, "ldc", kInstrCanContinue, 1), // --
    LDC_W(0x13, "ldc_w", kInstrCanContinue, 1), // --
    LDC2_W(0x14, "ldc2_w", kInstrCanContinue, 2), // --
    ILOAD(0x15, "iload", kInstrCanContinue, 1), // --
    LLOAD(0x16, "lload", kInstrCanContinue, 2), // --
    FLOAD(0x17, "fload", kInstrCanContinue, 1), // --
    DLOAD(0x18, "dload", kInstrCanContinue, 2), // --
    ALOAD(0x19, "aload", kInstrCanContinue, 1), // --
    ILOAD_0(0x1a, "iload_0", kInstrCanContinue, 1), // --
    ILOAD_1(0x1b, "iload_1", kInstrCanContinue, 1), // --
    ILOAD_2(0x1c, "iload_2", kInstrCanContinue, 1), // --
    ILOAD_3(0x1d, "iload_3", kInstrCanContinue, 1), // --
    LLOAD_0(0x1e, "lload_0", kInstrCanContinue, 2), // --
    LLOAD_1(0x1f, "lload_1", kInstrCanContinue, 2), // --
    LLOAD_2(0x20, "lload_2", kInstrCanContinue, 2), // --
    LLOAD_3(0x21, "lload_3", kInstrCanContinue, 2), // --
    FLOAD_0(0x22, "fload_0", kInstrCanContinue, 1), // --
    FLOAD_1(0x23, "fload_1", kInstrCanContinue, 1), // --
    FLOAD_2(0x24, "fload_2", kInstrCanContinue, 1), // --
    FLOAD_3(0x25, "fload_3", kInstrCanContinue, 1), // --
    DLOAD_0(0x26, "dload_0", kInstrCanContinue, 2), // --
    DLOAD_1(0x27, "dload_1", kInstrCanContinue, 2), // --
    DLOAD_2(0x28, "dload_2", kInstrCanContinue, 2), // --
    DLOAD_3(0x29, "dload_3", kInstrCanContinue, 2), // --
    ALOAD_0(0x2a, "aload_0", kInstrCanContinue, 1), // --
    ALOAD_1(0x2b, "aload_1", kInstrCanContinue, 1), // --
    ALOAD_2(0x2c, "aload_2", kInstrCanContinue, 1), // --
    ALOAD_3(0x2d, "aload_3", kInstrCanContinue, 1), // --
    IALOAD(0x2e, "iaload", kInstrCanContinue, -1), // --
    LALOAD(0x2f, "laload", kInstrCanContinue, 0), // --
    FALOAD(0x30, "faload", kInstrCanContinue, -1), // --
    DALOAD(0x31, "daload", kInstrCanContinue, 0), // --
    AALOAD(0x32, "aaload", kInstrCanContinue, -1), // --
    BALOAD(0x33, "baload", kInstrCanContinue, -1), // --
    CALOAD(0x34, "caload", kInstrCanContinue, -1), // --
    SALOAD(0x35, "saload", kInstrCanContinue, -1), // --
    ISTORE(0x36, "istore", kInstrCanContinue, -1), // --
    LSTORE(0x37, "lstore", kInstrCanContinue, -2), // --
    FSTORE(0x38, "fstore", kInstrCanContinue, -1), // --
    DSTORE(0x39, "dstore", kInstrCanContinue, -2), // --
    ASTORE(0x3a, "astore", kInstrCanContinue, -1), // --
    ISTORE_0(0x3b, "istore_0", kInstrCanContinue, -1), // --
    ISTORE_1(0x3c, "istore_1", kInstrCanContinue, -1), // --
    ISTORE_2(0x3d, "istore_2", kInstrCanContinue, -1), // --
    ISTORE_3(0x3e, "istore_3", kInstrCanContinue, -1), // --
    LSTORE_0(0x3f, "lstore_0", kInstrCanContinue, -2), // --
    LSTORE_1(0x40, "lstore_1", kInstrCanContinue, -2), // --
    LSTORE_2(0x41, "lstore_2", kInstrCanContinue, -2), // --
    LSTORE_3(0x42, "lstore_3", kInstrCanContinue, -2), // --
    FSTORE_0(0x43, "fstore_0", kInstrCanContinue, -1), // --
    FSTORE_1(0x44, "fstore_1", kInstrCanContinue, -1), // --
    FSTORE_2(0x45, "fstore_2", kInstrCanContinue, -1), // --
    FSTORE_3(0x46, "fstore_3", kInstrCanContinue, -1), // --
    DSTORE_0(0x47, "dstore_0", kInstrCanContinue, -2), // --
    DSTORE_1(0x48, "dstore_1", kInstrCanContinue, -2), // --
    DSTORE_2(0x49, "dstore_2", kInstrCanContinue, -2), // --
    DSTORE_3(0x4a, "dstore_3", kInstrCanContinue, -2), // --
    ASTORE_0(0x4b, "astore_0", kInstrCanContinue, -1), // --
    ASTORE_1(0x4c, "astore_1", kInstrCanContinue, -1), // --
    ASTORE_2(0x4d, "astore_2", kInstrCanContinue, -1), // --
    ASTORE_3(0x4e, "astore_3", kInstrCanContinue, -1), // --
    IASTORE(0x4f, "iastore", kInstrCanContinue, -3), // --
    LASTORE(0x50, "lastore", kInstrCanContinue, -4), // --
    FASTORE(0x51, "fastore", kInstrCanContinue, -3), // --
    DASTORE(0x52, "dastore", kInstrCanContinue, -4), // --
    AASTORE(0x53, "aastore", kInstrCanContinue, -3), // --
    BASTORE(0x54, "bastore", kInstrCanContinue, -3), // --
    CASTORE(0x55, "castore", kInstrCanContinue, -3), // --
    SASTORE(0x56, "sastore", kInstrCanContinue, -3), // --
    POP(0x57, "pop", kInstrCanContinue, -1), // --
    POP2(0x58, "pop2", kInstrCanContinue, -2), // --
    DUP(0x59, "dup", kInstrCanContinue, 1), // --
    DUP_X1(0x5a, "dup_x1", kInstrCanContinue, 1), // --
    DUP_X2(0x5b, "dup_x2", kInstrCanContinue, 1), // --
    DUP2(0x5c, "dup2", kInstrCanContinue, 2), // --
    DUP2_X1(0x5d, "dup2_x1", kInstrCanContinue, 2), // --
    DUP2_X2(0x5e, "dup2_x2", kInstrCanContinue, 2), // --
    SWAP(0x5f, "swap", kInstrCanContinue, 0), // --
    IADD(0x60, "iadd", kInstrCanContinue, -1), // --
    LADD(0x61, "ladd", kInstrCanContinue, -2), // --
    FADD(0x62, "fadd", kInstrCanContinue, -1), // --
    DADD(0x63, "dadd", kInstrCanContinue, -2), // --
    ISUB(0x64, "isub", kInstrCanContinue, -1), // --
    LSUB(0x65, "lsub", kInstrCanContinue, -2), // --
    FSUB(0x66, "fsub", kInstrCanContinue, -1), // --
    DSUB(0x67, "dsub", kInstrCanContinue, -2), // --
    IMUL(0x68, "imul", kInstrCanContinue, -1), // --
    LMUL(0x69, "lmul", kInstrCanContinue, -2), // --
    FMUL(0x6a, "fmul", kInstrCanContinue, -1), // --
    DMUL(0x6b, "dmul", kInstrCanContinue, -2), // --
    IDIV(0x6c, "idiv", kInstrCanContinue, -1), // --
    LDIV(0x6d, "ldiv", kInstrCanContinue, -2), // --
    FDIV(0x6e, "fdiv", kInstrCanContinue, -1), // --
    DDIV(0x6f, "ddiv", kInstrCanContinue, -2), // --
    IREM(0x70, "irem", kInstrCanContinue, -1), // --
    LREM(0x71, "lrem", kInstrCanContinue, -2), // --
    FREM(0x72, "frem", kInstrCanContinue, -1), // --
    DREM(0x73, "drem", kInstrCanContinue, -2), // --
    INEG(0x74, "ineg", kInstrCanContinue, 0), // --
    LNEG(0x75, "lneg", kInstrCanContinue, 0), // --
    FNEG(0x76, "fneg", kInstrCanContinue, 0), // --
    DNEG(0x77, "dneg", kInstrCanContinue, 0), // --
    ISHL(0x78, "ishl", kInstrCanContinue, -1), // --
    LSHL(0x79, "lshl", kInstrCanContinue, -1), // --
    ISHR(0x7a, "ishr", kInstrCanContinue, -1), // --
    LSHR(0x7b, "lshr", kInstrCanContinue, -1), // --
    IUSHR(0x7c, "iushr", kInstrCanContinue, -1), // --
    LUSHR(0x7d, "lushr", kInstrCanContinue, -1), // --
    IAND(0x7e, "iand", kInstrCanContinue, -1), // --
    LAND(0x7f, "land", kInstrCanContinue, -2), // --
    IOR(0x80, "ior", kInstrCanContinue, -1), // --
    LOR(0x81, "lor", kInstrCanContinue, -2), // --
    IXOR(0x82, "ixor", kInstrCanContinue, -1), // --
    LXOR(0x83, "lxor", kInstrCanContinue, -2), // --
    IINC(0x84, "iinc", kInstrCanContinue, 0), // --
    I2L(0x85, "i2l", kInstrCanContinue, 1), // --
    I2F(0x86, "i2f", kInstrCanContinue, 0), // --
    I2D(0x87, "i2d", kInstrCanContinue, 1), // --
    L2I(0x88, "l2i", kInstrCanContinue, -1), // --
    L2F(0x89, "l2f", kInstrCanContinue, -1), // --
    L2D(0x8a, "l2d", kInstrCanContinue, 0), // --
    F2I(0x8b, "f2i", kInstrCanContinue, 0), // --
    F2L(0x8c, "f2l", kInstrCanContinue, 1), // --
    F2D(0x8d, "f2d", kInstrCanContinue, 1), // --
    D2I(0x8e, "d2i", kInstrCanContinue, -1), // --
    D2L(0x8f, "d2l", kInstrCanContinue, 0), // --
    D2F(0x90, "d2f", kInstrCanContinue, -1), // --
    I2B(0x91, "i2b", kInstrCanContinue, 0), // --
    I2C(0x92, "i2c", kInstrCanContinue, 0), // --
    I2S(0x93, "i2s", kInstrCanContinue, 0), // --
    LCMP(0x94, "lcmp", kInstrCanContinue, -2), // --
    FCMPL(0x95, "fcmpl", kInstrCanContinue, -1), // --
    FCMPG(0x96, "fcmpg", kInstrCanContinue, -1), // --
    DCMPL(0x97, "dcmpl", kInstrCanContinue, -3), // --
    DCMPG(0x98, "dcmpg", kInstrCanContinue, -3), // --
    IFEQ(0x99, "ifeq", kInstrCanBranch, -1), // --
    IFNE(0x9a, "ifne", kInstrCanBranch, -1), // --
    IFLT(0x9b, "iflt", kInstrCanBranch, -1), // --
    IFGE(0x9c, "ifge", kInstrCanBranch, -1), // --
    IFGT(0x9d, "ifgt", kInstrCanBranch, -1), // --
    IFLE(0x9e, "ifle", kInstrCanBranch, -1), // --
    IF_ICMPEQ(0x9f, "if_icmpeq", kInstrCanBranch, -2), // --
    IF_ICMPNE(0xa0, "if_icmpne", kInstrCanBranch, -2), // --
    IF_ICMPLT(0xa1, "if_icmplt", kInstrCanBranch, -2), // --
    IF_ICMPGE(0xa2, "if_icmpge", kInstrCanBranch, -2), // --
    IF_ICMPGT(0xa3, "if_icmpgt", kInstrCanBranch, -2), // --
    IF_ICMPLE(0xa4, "if_icmple", kInstrCanBranch, -2), // --
    IF_ACMPEQ(0xa5, "if_acmpeq", kInstrCanBranch, -2), // --
    IF_ACMPNE(0xa6, "if_acmpne", kInstrCanBranch, -2), // --
    GOTO(0xa7, "goto", kInstrCanBranch, 0), // --
    JSR(0xa8, "jsr", kInstrCanContinue, 1), // ??--
    RET(0xa9, "ret", kInstrCanContinue, -1), // ??--
    TABLESWITCH(0xaa, "tableswitch", kInstrCanBranch, 1), // --
    LOOKUPSWITCH(0xab, "lookupswitch", kInstrCanBranch, 0), // --
    IRETURN(0xac, "ireturn", kInstrCanReturn, -1), // --
    LRETURN(0xad, "lreturn", kInstrCanReturn, -2), // --
    FRETURN(0xae, "freturn", kInstrCanReturn, -1), // --
    DRETURN(0xaf, "dreturn", kInstrCanReturn, -2), // --
    ARETURN(0xb0, "areturn", kInstrCanReturn, -1), // --
    RETURN(0xb1, "return", kInstrCanReturn, 0), // --
    GETSTATIC(0xb2, "getstatic", kInstrCanContinue, 0), // dynamic changing+ --
    PUTSTATIC(0xb3, "putstatic", kInstrCanContinue, 0), // dynamic changing- --
    GETFIELD(0xb4, "getfield", kInstrCanContinue, 0), // dynamic changing_+ --
    PUTFIELD(0xb5, "putfield", kInstrCanContinue, 0), // dynamic changing-- --
    INVOKEVIRTUAL(0xb6, "invokevirtual", kInstrInvoke | kInstrCanThrow, 0), // dynamic changing-- --
    INVOKESPECIAL(0xb7, "invokespecial", kInstrInvoke | kInstrCanThrow, 0), // dynamic changing-- --
    INVOKESTATIC(0xb8, "invokestatic", kInstrInvoke | kInstrCanThrow, 0), // dynamic changing-- --
    INVOKEINTERFACE(0xb9, "invokeinterface", kInstrInvoke | kInstrCanThrow, 0), // dynamic
                                                                                // changing-- --
    INVOKEDYNAMIC(0xba, "invokedynamic", kInstrInvoke | kInstrCanThrow, 0), // dynamic changing-- --
    NEW(0xbb, "new", kInstrCanContinue, 1), // --
    NEWARRAY(0xbc, "newarray", kInstrCanContinue, 0), // --
    ANEWARRAY(0xbd, "anewarray", kInstrCanContinue, 0), // --
    ARRAYLENGTH(0xbe, "arraylength", kInstrCanContinue, 0), // --
    ATHROW(0xbf, "athrow", kInstrCanReturn, 0), // --
    CHECKCAST(0xc0, "checkcast", kInstrCanContinue, 0), // --
    INSTANCEOF(0xc1, "instanceof", kInstrCanContinue, 0), // --
    MONITORENTER(0xc2, "monitorenter", kInstrCanContinue, -1), // --
    MONITOREXIT(0xc3, "monitorexit", kInstrCanContinue, -1), // --
    WIDE(0xc4, "wide", kInstrCanContinue, 0), // --
    MULTIANEWARRAY(0xc5, "multianewarray", kInstrCanContinue, 0), // dynamic changing--
    IFNULL(0xc6, "ifnull", kInstrCanBranch, -1), // --
    IFNONNULL(0xc7, "ifnonnull", kInstrCanBranch, -1), // --
    GOTO_W(0xc8, "goto_w", kInstrCanBranch, 0), // --
    JSR_W(0xc9, "jsr_w", kInstrCanContinue, 1);// ??--

    public final static OpInfo ops[] = new OpInfo[256];

    static {
        OpInfo[] ops = OpInfo.ops;
        for (OpInfo op : OpInfo.values()) {
            if (op.opcode >= 0) {
                ops[op.opcode] = op;
            }
        }
    }
    public boolean changeFrame;
    int changeStack;
    private int flags;
    private String displayName;
    private int opcode;

    OpInfo() {}

    OpInfo(int opcode, String displayName, int branchType, int changStack) {
        flags = branchType;
        this.displayName = displayName;
        this.opcode = opcode;
        this.changeStack = changeStack;
    }

    public boolean canBranch() {
        return 0 != (flags & kInstrCanBranch);
    }

    public boolean canContinue() {
        return 0 != (flags & kInstrCanContinue);
    }

    public boolean canReturn() {
        return 0 != (flags & kInstrCanReturn);
    }

    public boolean canSwitch() {
        return 0 != (flags & kInstrCanSwitch);
    }

    public boolean canThrow() {
        return 0 != (flags & kInstrCanThrow);
    }


    public String toString() {
        return displayName;
    }
}
