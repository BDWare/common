package org.bdware.analysis.gas;

public enum FeeSchedule {
    // BDaload(20L),BDstore(200L),
    BDgetMethod(10L), BDsetMethod(20L), BDnew(20L), BDcallUtil(30L), BDcallFuntion(40L), BDcall(
            10L), BDjump(15L), BDInsn(1L);

    long fee;

    FeeSchedule(long value) {
        fee = value;
    }

    public long getFee() {
        return fee;
    }

    public long getValue(String name) {
        long value = 0;
        switch (name) {
            case "BDgetMethod":
                value = BDgetMethod.getFee();
                break;
            case "BDsetMethod":
                value = BDsetMethod.getFee();
                break;
            case "BDcallUtil":
                value = BDcallUtil.getFee();
                break;
            case "BDcallFuntion":
                value = BDcallFuntion.getFee();
                break;
            case "BDcall":
                value = BDcall.getFee();
                break;
            case "BDjump":
                value = BDjump.getFee();
                break;
            case "BDInsn":
                value = BDInsn.getFee();
                break;
            default:
                break;
        }
        return value;
    }

    public String getString(FeeSchedule feeName) {
        return feeName.toString();

    }
}
