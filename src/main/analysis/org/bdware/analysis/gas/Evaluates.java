package org.bdware.analysis.gas;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Evaluates {
    Map<Integer, HashMap<String, Integer>> callFunctionResult;
    Map<Integer, Long> CountResult = new HashMap<>();

    public Map<Integer, Long> getCallFee() {
        for (Entry<Integer, HashMap<String, Integer>> c : callFunctionResult.entrySet()) {
            Integer cfrKey = c.getKey();
            long value = 0;
            HashMap<String, Integer> map = c.getValue();
            for (Entry<String, Integer> cmap : map.entrySet()) {

                long fee = getValue(cmap.getKey());
                value = fee * cmap.getValue() + value;
            }
            CountResult.put(cfrKey, value);
        }
        return CountResult;
    }

    public long getValue(String name) {
        long value = 0;
        switch (name) {
            case "BDgetMethod":
                value = FeeSchedule.BDgetMethod.getFee();
                break;
            case "BDsetMethod":
                value = FeeSchedule.BDsetMethod.getFee();
                break;
            case "BDnew":
                value = FeeSchedule.BDnew.getFee();
                break;
            case "BDcallUtil":
                value = FeeSchedule.BDcallUtil.getFee();
                break;
            case "BDcallFuntion":
                value = FeeSchedule.BDcallFuntion.getFee();
                break;
            case "BDcall":
                value = FeeSchedule.BDcall.getFee();
                break;
            case "BDjump":
                value = FeeSchedule.BDjump.getFee();
                break;
            case "BDInsn":
                value = FeeSchedule.BDInsn.getFee();
                break;
            default:
                break;
        }
        return value;
    }

    long sum;
    Set<HashMap<String, Long>> set = new HashSet<>();

    public void getGas(HashMap<String, Set<Map<Integer, HashMap<String, Integer>>>> branchCount) {
        for (Entry<String, Set<Map<Integer, HashMap<String, Integer>>>> varInsn : branchCount
                .entrySet()) {
            sum = 0;
            for (Map<Integer, HashMap<String, Integer>> m : varInsn.getValue()) {
                blockGas(m);
            }
            map.put(varInsn.getKey(), sum);
        }
    }

    private void blockGas(Map<Integer, HashMap<String, Integer>> m) {
        for (Entry<Integer, HashMap<String, Integer>> map : m.entrySet()) {
            for (Entry<String, Integer> insnMap : map.getValue().entrySet()) {
                long value = getValue(insnMap.getKey()) * insnMap.getValue();
                sum = sum + value;
            }
        }
    }

    public static HashMap<String, Long> map = new HashMap<>();

    public void getInsnGas(Map<Integer, Set<Map<Integer, HashMap<String, Integer>>>> ppMap) {
        for (Entry<Integer, Set<Map<Integer, HashMap<String, Integer>>>> varInsn : ppMap
                .entrySet()) {
            sum = 0;
            for (Map<Integer, HashMap<String, Integer>> m : varInsn.getValue()) {
                blockGas(m);
            }
            map.put(varInsn.getKey().toString(), sum);
        }
    }
}
