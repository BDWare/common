package org.bdware.analysis.gas;

import org.bdware.analysis.BasicBlock;
import org.bdware.analysis.CFGraph;

import java.util.Set;

public abstract class DFS {
    private boolean[] marked = new boolean[2048];

    public abstract Set<BasicBlock> getSuc(BasicBlock BB);

    public void dfs(CFGraph cfg, BasicBlock start) {
        marked[start.blockID] = true;
        // System.out.println("[start.blockID]" + start.blockID);
        Set<BasicBlock> sucBlocks = getSuc(start);
        for (BasicBlock bb : sucBlocks) {
            if (!marked[bb.blockID])
                dfs(cfg, bb);
        }
    }

    public boolean isArrival(BasicBlock end) {
        return marked[end.blockID];
    }
}
