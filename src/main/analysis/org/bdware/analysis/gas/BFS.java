package org.bdware.analysis.gas;

import java.util.Collection;
import java.util.List;

import org.bdware.analysis.BasicBlock;

public abstract class BFS {
    protected List<BasicBlock> toAnalysis;

    public abstract Collection<BasicBlock> getSuc(BasicBlock BB);

    public void analysis() {
        BasicBlock current = null;
        for (int i = 0; i < toAnalysis.size(); i++) {
            current = toAnalysis.get(i);
            // current.setInList(false);
            if (current.inList()) {
                Collection<BasicBlock> sucs = getSuc(current);
                for (BasicBlock next : sucs) {
                    if (!next.inList()) {
                        toAnalysis.add(next);
                        next.setInList(true);
                    }
                }
            }
        }
    }

    public void setToAnalysis(List<BasicBlock> l) {
        toAnalysis = l;
    }

}
