package org.bdware.analysis;

public interface AnalysisTarget {
    public boolean inList();

    public void setInList(boolean b);
}
