package org.bdware.analysis.taint;

import java.util.Set;

import org.bdware.analysis.BasicBlock;

public class DirectGraphDFS {
    private boolean[] marked;

    public DirectGraphDFS(TaintCFG cfg, BasicBlock start) {
        marked = new boolean[cfg.getBlocks().size()];
        dfs(cfg, start);
    }

    private void dfs(TaintCFG cfg, BasicBlock start) {
        marked[start.blockID] = true;
        Set<BasicBlock> sucBlocks = cfg.getSucBlocks(start);
        for (BasicBlock bb : sucBlocks) {
            if (!marked[bb.blockID])
                dfs(cfg, bb);
        }
    }

    public boolean isArrival(BasicBlock end) {
        return marked[end.blockID];
    }

}
