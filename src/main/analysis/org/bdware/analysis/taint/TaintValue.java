package org.bdware.analysis.taint;

import org.objectweb.asm.tree.analysis.Value;

public class TaintValue implements Value {
    public int size;
    // public boolean isTainted;
    public long isTainted;

    public TaintValue(int size) {
        this.size = size;
        isTainted = 0L;
    }

    public TaintValue(int size, long isTainted) {
        this.size = size;
        this.isTainted = isTainted;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        String s = Long.toBinaryString(isTainted);
        char[] c = s.toCharArray();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < c.length; i++) {
            if (c[i] == '1')
                buf.append("1/");
            else
                buf.append("0/");
        }
        buf.append("--");
        return buf.toString();
    }

    public TaintValue clone() {
        TaintValue ret = new TaintValue(size);
        ret.isTainted = isTainted;
        return ret;
    }

    public String toReadableTaint() {
        return TaintResult.interpreter.taintBits.parse(isTainted);
    }

    public void merge(TaintValue target) {
        if (target != null)
            isTainted |= target.isTainted;
    }
}
