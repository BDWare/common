package org.bdware.analysis.taint;

import java.util.List;

import org.bdware.analysis.AnalysisTarget;
import org.bdware.analysis.BasicBlock;
import org.objectweb.asm.tree.AbstractInsnNode;

public class TaintBB extends BasicBlock implements AnalysisTarget {
    boolean inList = false;

    public TaintBB(int id) {
        super(id);
        preResult = new TaintResult();
        sucResult = new TaintResult();
    }

    public TaintResult preResult;
    public TaintResult sucResult;

    @Override
    public boolean inList() {
        return inList;
    }

    @Override
    public void setInList(boolean b) {
        inList = b;
    }

    public String getResult() {
        return (sucResult.frame2Str() + " Ret:" + sucResult.ret.toString());
    }

    public String getResultWithTaintBit() {
        return "Ret:" + sucResult.ret.toReadableTaint();
    }

    public TaintResult forwardAnalysis() {
        TaintResult oldSuc = sucResult;
        sucResult = (TaintResult) preResult.clone();
        TaintResult currentResult = sucResult;
        List<AbstractInsnNode> insns = getInsn();
        if (TaintConfig.isDebug)
            System.out.println(
                    "[TaintBB] Enter B" + blockID + ":" + getResult() + " size:" + insns.size());

        for (int i = 0; i < insns.size(); i++) {
            currentResult = (TaintResult) currentResult.merge(insns.get(i));
        }
        currentResult.mergeResult(oldSuc);

        if (TaintConfig.isDebug)
            System.out.println(
                    "[TaintBB] Leave B" + blockID + ":" + getResult() + " size:" + insns.size());
        // already done.
        return currentResult;
    }

    public AbstractInsnNode lastInsn() {
        return list.get(list.size() - 1);
    }

    public AbstractInsnNode firstInsn() {
        return list.get(0);
    }

    public List<AbstractInsnNode> AllInsn() {
        return list;
    }



}
