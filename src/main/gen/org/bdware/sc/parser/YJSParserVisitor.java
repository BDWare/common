// Generated from YJSParser.g4 by ANTLR 4.9.2
package org.bdware.sc.parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced by {@link YJSParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for operations with no return
 *        type.
 */
public interface YJSParserVisitor<T> extends ParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by {@link YJSParser#program}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitProgram(YJSParser.ProgramContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#contractDeclar}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitContractDeclar(YJSParser.ContractDeclarContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#annotations}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAnnotations(YJSParser.AnnotationsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#annotation}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAnnotation(YJSParser.AnnotationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#annotationArgs}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAnnotationArgs(YJSParser.AnnotationArgsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#annotationLiteral}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#clzOrFunctionDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#interfaceDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#eventDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEventDeclaration(YJSParser.EventDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#eventGlobalOrLocal}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#eventSemantics}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEventSemantics(YJSParser.EventSemanticsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#sourceElement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSourceElement(YJSParser.SourceElementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#importStmts}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitImportStmts(YJSParser.ImportStmtsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#importStmt}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitImportStmt(YJSParser.ImportStmtContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#exportStmt}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExportStmt(YJSParser.ExportStmtContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#versionName}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVersionName(YJSParser.VersionNameContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#statement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatement(YJSParser.StatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#block}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBlock(YJSParser.BlockContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#statementList}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatementList(YJSParser.StatementListContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#sharableDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSharableDeclaration(YJSParser.SharableDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#sharableStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSharableStatement(YJSParser.SharableStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#sharableModifier}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSharableModifier(YJSParser.SharableModifierContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#variableStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVariableStatement(YJSParser.VariableStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#variableDeclarationList}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#variableDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVariableDeclaration(YJSParser.VariableDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#emptyStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEmptyStatement(YJSParser.EmptyStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#expressionStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpressionStatement(YJSParser.ExpressionStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#ifStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIfStatement(YJSParser.IfStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code DoStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDoStatement(YJSParser.DoStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code WhileStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitWhileStatement(YJSParser.WhileStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code ForStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForStatement(YJSParser.ForStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code ForVarStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForVarStatement(YJSParser.ForVarStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code ForInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForInStatement(YJSParser.ForInStatementContext ctx);

    /**
     * Visit a parse tree produced by the {@code ForVarInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForVarInStatement(YJSParser.ForVarInStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#varModifier}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVarModifier(YJSParser.VarModifierContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#continueStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitContinueStatement(YJSParser.ContinueStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#breakStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBreakStatement(YJSParser.BreakStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#returnStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitReturnStatement(YJSParser.ReturnStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#withStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitWithStatement(YJSParser.WithStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#switchStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSwitchStatement(YJSParser.SwitchStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#caseBlock}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCaseBlock(YJSParser.CaseBlockContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#caseClauses}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCaseClauses(YJSParser.CaseClausesContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#caseClause}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCaseClause(YJSParser.CaseClauseContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#defaultClause}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDefaultClause(YJSParser.DefaultClauseContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#throwStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitThrowStatement(YJSParser.ThrowStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#tryStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTryStatement(YJSParser.TryStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#catchProduction}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCatchProduction(YJSParser.CatchProductionContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#finallyProduction}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFinallyProduction(YJSParser.FinallyProductionContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#debuggerStatement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDebuggerStatement(YJSParser.DebuggerStatementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#functionDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#classDeclaration}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassDeclaration(YJSParser.ClassDeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#classTail}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassTail(YJSParser.ClassTailContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#classElement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassElement(YJSParser.ClassElementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#methodDefinition}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMethodDefinition(YJSParser.MethodDefinitionContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#formalParameterList}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFormalParameterList(YJSParser.FormalParameterListContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#formalParameterArg}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFormalParameterArg(YJSParser.FormalParameterArgContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#lastFormalParameterArg}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#functionBody}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunctionBody(YJSParser.FunctionBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#sourceElements}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSourceElements(YJSParser.SourceElementsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#arrayLiteral}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrayLiteral(YJSParser.ArrayLiteralContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#elementList}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitElementList(YJSParser.ElementListContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#lastElement}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLastElement(YJSParser.LastElementContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#objectLiteral}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitObjectLiteral(YJSParser.ObjectLiteralContext ctx);

    /**
     * Visit a parse tree produced by the {@code PropertyExpressionAssignment} labeled alternative
     * in {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPropertyExpressionAssignment(YJSParser.PropertyExpressionAssignmentContext ctx);

    /**
     * Visit a parse tree produced by the {@code ComputedPropertyExpressionAssignment} labeled
     * alternative in {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx);

    /**
     * Visit a parse tree produced by the {@code PropertyShorthand} labeled alternative in
     * {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPropertyShorthand(YJSParser.PropertyShorthandContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#propertyName}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPropertyName(YJSParser.PropertyNameContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#arguments}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArguments(YJSParser.ArgumentsContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#lastArgument}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLastArgument(YJSParser.LastArgumentContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#expressionSequence}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpressionSequence(YJSParser.ExpressionSequenceContext ctx);

    /**
     * Visit a parse tree produced by the {@code TemplateStringExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code TernaryExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTernaryExpression(YJSParser.TernaryExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code LogicalAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code PreIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ObjectLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code InExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInExpression(YJSParser.InExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code LogicalOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code NotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNotExpression(YJSParser.NotExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code PreDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ArgumentsExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ThisExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitThisExpression(YJSParser.ThisExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code UnaryMinusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code AssignmentExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignmentExpression(YJSParser.AssignmentExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code PostDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code TypeofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTypeofExpression(YJSParser.TypeofExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code InstanceofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInstanceofExpression(YJSParser.InstanceofExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code UnaryPlusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ArrowFunctionExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code EqualityExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEqualityExpression(YJSParser.EqualityExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code BitXOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitXOrExpression(YJSParser.BitXOrExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code SuperExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSuperExpression(YJSParser.SuperExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code MultiplicativeExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code BitShiftExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitShiftExpression(YJSParser.BitShiftExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ParenthesizedExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code AdditiveExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAdditiveExpression(YJSParser.AdditiveExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code RelationalExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRelationalExpression(YJSParser.RelationalExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code PostIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code BitNotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitNotExpression(YJSParser.BitNotExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code NewExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNewExpression(YJSParser.NewExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code LiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLiteralExpression(YJSParser.LiteralExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code ArrayLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code MemberDotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMemberDotExpression(YJSParser.MemberDotExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code MemberIndexExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code IdentifierExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentifierExpression(YJSParser.IdentifierExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code BitAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitAndExpression(YJSParser.BitAndExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code BitOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitOrExpression(YJSParser.BitOrExpressionContext ctx);

    /**
     * Visit a parse tree produced by the {@code AssignmentOperatorExpression} labeled alternative
     * in {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignmentOperatorExpression(YJSParser.AssignmentOperatorExpressionContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#arrowFunctionParameters}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#arrowFunctionBody}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#assignmentOperator}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignmentOperator(YJSParser.AssignmentOperatorContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#literal}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLiteral(YJSParser.LiteralContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#numericLiteral}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNumericLiteral(YJSParser.NumericLiteralContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#identifierName}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentifierName(YJSParser.IdentifierNameContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#reservedWord}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitReservedWord(YJSParser.ReservedWordContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#keyword}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitKeyword(YJSParser.KeywordContext ctx);

    /**
     * Visit a parse tree produced by {@link YJSParser#eos}.
     * 
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEos(YJSParser.EosContext ctx);
}
