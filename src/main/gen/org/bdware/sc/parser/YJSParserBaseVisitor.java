// Generated from YJSParser.g4 by ANTLR 4.9.2
package org.bdware.sc.parser;

import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

/**
 * This class provides an empty implementation of {@link YJSParserVisitor}, which can be extended to
 * create a visitor which only needs to handle a subset of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for operations with no return
 *        type.
 */
public class YJSParserBaseVisitor<T> extends AbstractParseTreeVisitor<T>
        implements YJSParserVisitor<T> {
    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitProgram(YJSParser.ProgramContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitContractDeclar(YJSParser.ContractDeclarContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAnnotations(YJSParser.AnnotationsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAnnotation(YJSParser.AnnotationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAnnotationArgs(YJSParser.AnnotationArgsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEventDeclaration(YJSParser.EventDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEventSemantics(YJSParser.EventSemanticsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSourceElement(YJSParser.SourceElementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitImportStmts(YJSParser.ImportStmtsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitImportStmt(YJSParser.ImportStmtContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitExportStmt(YJSParser.ExportStmtContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitVersionName(YJSParser.VersionNameContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitStatement(YJSParser.StatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBlock(YJSParser.BlockContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitStatementList(YJSParser.StatementListContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSharableDeclaration(YJSParser.SharableDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSharableStatement(YJSParser.SharableStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSharableModifier(YJSParser.SharableModifierContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitVariableStatement(YJSParser.VariableStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitVariableDeclaration(YJSParser.VariableDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEmptyStatement(YJSParser.EmptyStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitExpressionStatement(YJSParser.ExpressionStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitIfStatement(YJSParser.IfStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitDoStatement(YJSParser.DoStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitWhileStatement(YJSParser.WhileStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitForStatement(YJSParser.ForStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitForVarStatement(YJSParser.ForVarStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitForInStatement(YJSParser.ForInStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitForVarInStatement(YJSParser.ForVarInStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitVarModifier(YJSParser.VarModifierContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitContinueStatement(YJSParser.ContinueStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBreakStatement(YJSParser.BreakStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitReturnStatement(YJSParser.ReturnStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitWithStatement(YJSParser.WithStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSwitchStatement(YJSParser.SwitchStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitCaseBlock(YJSParser.CaseBlockContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitCaseClauses(YJSParser.CaseClausesContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitCaseClause(YJSParser.CaseClauseContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitDefaultClause(YJSParser.DefaultClauseContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitThrowStatement(YJSParser.ThrowStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitTryStatement(YJSParser.TryStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitCatchProduction(YJSParser.CatchProductionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitFinallyProduction(YJSParser.FinallyProductionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitDebuggerStatement(YJSParser.DebuggerStatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitClassDeclaration(YJSParser.ClassDeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitClassTail(YJSParser.ClassTailContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitClassElement(YJSParser.ClassElementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitMethodDefinition(YJSParser.MethodDefinitionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitFormalParameterList(YJSParser.FormalParameterListContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitFormalParameterArg(YJSParser.FormalParameterArgContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitFunctionBody(YJSParser.FunctionBodyContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSourceElements(YJSParser.SourceElementsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArrayLiteral(YJSParser.ArrayLiteralContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitElementList(YJSParser.ElementListContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLastElement(YJSParser.LastElementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitObjectLiteral(YJSParser.ObjectLiteralContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPropertyExpressionAssignment(YJSParser.PropertyExpressionAssignmentContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPropertyShorthand(YJSParser.PropertyShorthandContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPropertyName(YJSParser.PropertyNameContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArguments(YJSParser.ArgumentsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLastArgument(YJSParser.LastArgumentContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitExpressionSequence(YJSParser.ExpressionSequenceContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitTernaryExpression(YJSParser.TernaryExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitInExpression(YJSParser.InExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitNotExpression(YJSParser.NotExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitThisExpression(YJSParser.ThisExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAssignmentExpression(YJSParser.AssignmentExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitTypeofExpression(YJSParser.TypeofExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitInstanceofExpression(YJSParser.InstanceofExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEqualityExpression(YJSParser.EqualityExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBitXOrExpression(YJSParser.BitXOrExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitSuperExpression(YJSParser.SuperExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBitShiftExpression(YJSParser.BitShiftExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAdditiveExpression(YJSParser.AdditiveExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitRelationalExpression(YJSParser.RelationalExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBitNotExpression(YJSParser.BitNotExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitNewExpression(YJSParser.NewExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLiteralExpression(YJSParser.LiteralExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitMemberDotExpression(YJSParser.MemberDotExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitIdentifierExpression(YJSParser.IdentifierExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBitAndExpression(YJSParser.BitAndExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitBitOrExpression(YJSParser.BitOrExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAssignmentOperatorExpression(YJSParser.AssignmentOperatorExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitAssignmentOperator(YJSParser.AssignmentOperatorContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitLiteral(YJSParser.LiteralContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitNumericLiteral(YJSParser.NumericLiteralContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitIdentifierName(YJSParser.IdentifierNameContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitReservedWord(YJSParser.ReservedWordContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitKeyword(YJSParser.KeywordContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation returns the result of calling {@link #visitChildren} on
     * {@code ctx}.
     * </p>
     */
    @Override
    public T visitEos(YJSParser.EosContext ctx) {
        return visitChildren(ctx);
    }
}
