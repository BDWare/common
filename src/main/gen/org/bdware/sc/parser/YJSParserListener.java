// Generated from YJSParser.g4 by ANTLR 4.9.2
package org.bdware.sc.parser;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by {@link YJSParser}.
 */
public interface YJSParserListener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link YJSParser#program}.
     * 
     * @param ctx the parse tree
     */
    void enterProgram(YJSParser.ProgramContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#program}.
     * 
     * @param ctx the parse tree
     */
    void exitProgram(YJSParser.ProgramContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#contractDeclar}.
     * 
     * @param ctx the parse tree
     */
    void enterContractDeclar(YJSParser.ContractDeclarContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#contractDeclar}.
     * 
     * @param ctx the parse tree
     */
    void exitContractDeclar(YJSParser.ContractDeclarContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#annotations}.
     * 
     * @param ctx the parse tree
     */
    void enterAnnotations(YJSParser.AnnotationsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#annotations}.
     * 
     * @param ctx the parse tree
     */
    void exitAnnotations(YJSParser.AnnotationsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#annotation}.
     * 
     * @param ctx the parse tree
     */
    void enterAnnotation(YJSParser.AnnotationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#annotation}.
     * 
     * @param ctx the parse tree
     */
    void exitAnnotation(YJSParser.AnnotationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#annotationArgs}.
     * 
     * @param ctx the parse tree
     */
    void enterAnnotationArgs(YJSParser.AnnotationArgsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#annotationArgs}.
     * 
     * @param ctx the parse tree
     */
    void exitAnnotationArgs(YJSParser.AnnotationArgsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#annotationLiteral}.
     * 
     * @param ctx the parse tree
     */
    void enterAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#annotationLiteral}.
     * 
     * @param ctx the parse tree
     */
    void exitAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#clzOrFunctionDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#clzOrFunctionDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#interfaceDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#interfaceDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#eventDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterEventDeclaration(YJSParser.EventDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#eventDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitEventDeclaration(YJSParser.EventDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#eventGlobalOrLocal}.
     * 
     * @param ctx the parse tree
     */
    void enterEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#eventGlobalOrLocal}.
     * 
     * @param ctx the parse tree
     */
    void exitEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#eventSemantics}.
     * 
     * @param ctx the parse tree
     */
    void enterEventSemantics(YJSParser.EventSemanticsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#eventSemantics}.
     * 
     * @param ctx the parse tree
     */
    void exitEventSemantics(YJSParser.EventSemanticsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#sourceElement}.
     * 
     * @param ctx the parse tree
     */
    void enterSourceElement(YJSParser.SourceElementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#sourceElement}.
     * 
     * @param ctx the parse tree
     */
    void exitSourceElement(YJSParser.SourceElementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#importStmts}.
     * 
     * @param ctx the parse tree
     */
    void enterImportStmts(YJSParser.ImportStmtsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#importStmts}.
     * 
     * @param ctx the parse tree
     */
    void exitImportStmts(YJSParser.ImportStmtsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#importStmt}.
     * 
     * @param ctx the parse tree
     */
    void enterImportStmt(YJSParser.ImportStmtContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#importStmt}.
     * 
     * @param ctx the parse tree
     */
    void exitImportStmt(YJSParser.ImportStmtContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#exportStmt}.
     * 
     * @param ctx the parse tree
     */
    void enterExportStmt(YJSParser.ExportStmtContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#exportStmt}.
     * 
     * @param ctx the parse tree
     */
    void exitExportStmt(YJSParser.ExportStmtContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#versionName}.
     * 
     * @param ctx the parse tree
     */
    void enterVersionName(YJSParser.VersionNameContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#versionName}.
     * 
     * @param ctx the parse tree
     */
    void exitVersionName(YJSParser.VersionNameContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#statement}.
     * 
     * @param ctx the parse tree
     */
    void enterStatement(YJSParser.StatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#statement}.
     * 
     * @param ctx the parse tree
     */
    void exitStatement(YJSParser.StatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#block}.
     * 
     * @param ctx the parse tree
     */
    void enterBlock(YJSParser.BlockContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#block}.
     * 
     * @param ctx the parse tree
     */
    void exitBlock(YJSParser.BlockContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#statementList}.
     * 
     * @param ctx the parse tree
     */
    void enterStatementList(YJSParser.StatementListContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#statementList}.
     * 
     * @param ctx the parse tree
     */
    void exitStatementList(YJSParser.StatementListContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#sharableDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterSharableDeclaration(YJSParser.SharableDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#sharableDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitSharableDeclaration(YJSParser.SharableDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#sharableStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterSharableStatement(YJSParser.SharableStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#sharableStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitSharableStatement(YJSParser.SharableStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#sharableModifier}.
     * 
     * @param ctx the parse tree
     */
    void enterSharableModifier(YJSParser.SharableModifierContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#sharableModifier}.
     * 
     * @param ctx the parse tree
     */
    void exitSharableModifier(YJSParser.SharableModifierContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#variableStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterVariableStatement(YJSParser.VariableStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#variableStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitVariableStatement(YJSParser.VariableStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#variableDeclarationList}.
     * 
     * @param ctx the parse tree
     */
    void enterVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#variableDeclarationList}.
     * 
     * @param ctx the parse tree
     */
    void exitVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#variableDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterVariableDeclaration(YJSParser.VariableDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#variableDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitVariableDeclaration(YJSParser.VariableDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#emptyStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterEmptyStatement(YJSParser.EmptyStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#emptyStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitEmptyStatement(YJSParser.EmptyStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#expressionStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterExpressionStatement(YJSParser.ExpressionStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#expressionStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitExpressionStatement(YJSParser.ExpressionStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#ifStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterIfStatement(YJSParser.IfStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#ifStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitIfStatement(YJSParser.IfStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code DoStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterDoStatement(YJSParser.DoStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code DoStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitDoStatement(YJSParser.DoStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code WhileStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterWhileStatement(YJSParser.WhileStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code WhileStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitWhileStatement(YJSParser.WhileStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code ForStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterForStatement(YJSParser.ForStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code ForStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitForStatement(YJSParser.ForStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code ForVarStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterForVarStatement(YJSParser.ForVarStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code ForVarStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitForVarStatement(YJSParser.ForVarStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code ForInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterForInStatement(YJSParser.ForInStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code ForInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitForInStatement(YJSParser.ForInStatementContext ctx);

    /**
     * Enter a parse tree produced by the {@code ForVarInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterForVarInStatement(YJSParser.ForVarInStatementContext ctx);

    /**
     * Exit a parse tree produced by the {@code ForVarInStatement} labeled alternative in
     * {@link YJSParser#iterationStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitForVarInStatement(YJSParser.ForVarInStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#varModifier}.
     * 
     * @param ctx the parse tree
     */
    void enterVarModifier(YJSParser.VarModifierContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#varModifier}.
     * 
     * @param ctx the parse tree
     */
    void exitVarModifier(YJSParser.VarModifierContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#continueStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterContinueStatement(YJSParser.ContinueStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#continueStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitContinueStatement(YJSParser.ContinueStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#breakStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterBreakStatement(YJSParser.BreakStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#breakStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitBreakStatement(YJSParser.BreakStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#returnStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterReturnStatement(YJSParser.ReturnStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#returnStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitReturnStatement(YJSParser.ReturnStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#withStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterWithStatement(YJSParser.WithStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#withStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitWithStatement(YJSParser.WithStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#switchStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterSwitchStatement(YJSParser.SwitchStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#switchStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitSwitchStatement(YJSParser.SwitchStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#caseBlock}.
     * 
     * @param ctx the parse tree
     */
    void enterCaseBlock(YJSParser.CaseBlockContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#caseBlock}.
     * 
     * @param ctx the parse tree
     */
    void exitCaseBlock(YJSParser.CaseBlockContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#caseClauses}.
     * 
     * @param ctx the parse tree
     */
    void enterCaseClauses(YJSParser.CaseClausesContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#caseClauses}.
     * 
     * @param ctx the parse tree
     */
    void exitCaseClauses(YJSParser.CaseClausesContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#caseClause}.
     * 
     * @param ctx the parse tree
     */
    void enterCaseClause(YJSParser.CaseClauseContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#caseClause}.
     * 
     * @param ctx the parse tree
     */
    void exitCaseClause(YJSParser.CaseClauseContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#defaultClause}.
     * 
     * @param ctx the parse tree
     */
    void enterDefaultClause(YJSParser.DefaultClauseContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#defaultClause}.
     * 
     * @param ctx the parse tree
     */
    void exitDefaultClause(YJSParser.DefaultClauseContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#throwStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterThrowStatement(YJSParser.ThrowStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#throwStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitThrowStatement(YJSParser.ThrowStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#tryStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterTryStatement(YJSParser.TryStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#tryStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitTryStatement(YJSParser.TryStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#catchProduction}.
     * 
     * @param ctx the parse tree
     */
    void enterCatchProduction(YJSParser.CatchProductionContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#catchProduction}.
     * 
     * @param ctx the parse tree
     */
    void exitCatchProduction(YJSParser.CatchProductionContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#finallyProduction}.
     * 
     * @param ctx the parse tree
     */
    void enterFinallyProduction(YJSParser.FinallyProductionContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#finallyProduction}.
     * 
     * @param ctx the parse tree
     */
    void exitFinallyProduction(YJSParser.FinallyProductionContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#debuggerStatement}.
     * 
     * @param ctx the parse tree
     */
    void enterDebuggerStatement(YJSParser.DebuggerStatementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#debuggerStatement}.
     * 
     * @param ctx the parse tree
     */
    void exitDebuggerStatement(YJSParser.DebuggerStatementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#functionDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#functionDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#classDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void enterClassDeclaration(YJSParser.ClassDeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#classDeclaration}.
     * 
     * @param ctx the parse tree
     */
    void exitClassDeclaration(YJSParser.ClassDeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#classTail}.
     * 
     * @param ctx the parse tree
     */
    void enterClassTail(YJSParser.ClassTailContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#classTail}.
     * 
     * @param ctx the parse tree
     */
    void exitClassTail(YJSParser.ClassTailContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#classElement}.
     * 
     * @param ctx the parse tree
     */
    void enterClassElement(YJSParser.ClassElementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#classElement}.
     * 
     * @param ctx the parse tree
     */
    void exitClassElement(YJSParser.ClassElementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#methodDefinition}.
     * 
     * @param ctx the parse tree
     */
    void enterMethodDefinition(YJSParser.MethodDefinitionContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#methodDefinition}.
     * 
     * @param ctx the parse tree
     */
    void exitMethodDefinition(YJSParser.MethodDefinitionContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#formalParameterList}.
     * 
     * @param ctx the parse tree
     */
    void enterFormalParameterList(YJSParser.FormalParameterListContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#formalParameterList}.
     * 
     * @param ctx the parse tree
     */
    void exitFormalParameterList(YJSParser.FormalParameterListContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#formalParameterArg}.
     * 
     * @param ctx the parse tree
     */
    void enterFormalParameterArg(YJSParser.FormalParameterArgContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#formalParameterArg}.
     * 
     * @param ctx the parse tree
     */
    void exitFormalParameterArg(YJSParser.FormalParameterArgContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#lastFormalParameterArg}.
     * 
     * @param ctx the parse tree
     */
    void enterLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#lastFormalParameterArg}.
     * 
     * @param ctx the parse tree
     */
    void exitLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#functionBody}.
     * 
     * @param ctx the parse tree
     */
    void enterFunctionBody(YJSParser.FunctionBodyContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#functionBody}.
     * 
     * @param ctx the parse tree
     */
    void exitFunctionBody(YJSParser.FunctionBodyContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#sourceElements}.
     * 
     * @param ctx the parse tree
     */
    void enterSourceElements(YJSParser.SourceElementsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#sourceElements}.
     * 
     * @param ctx the parse tree
     */
    void exitSourceElements(YJSParser.SourceElementsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#arrayLiteral}.
     * 
     * @param ctx the parse tree
     */
    void enterArrayLiteral(YJSParser.ArrayLiteralContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#arrayLiteral}.
     * 
     * @param ctx the parse tree
     */
    void exitArrayLiteral(YJSParser.ArrayLiteralContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#elementList}.
     * 
     * @param ctx the parse tree
     */
    void enterElementList(YJSParser.ElementListContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#elementList}.
     * 
     * @param ctx the parse tree
     */
    void exitElementList(YJSParser.ElementListContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#lastElement}.
     * 
     * @param ctx the parse tree
     */
    void enterLastElement(YJSParser.LastElementContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#lastElement}.
     * 
     * @param ctx the parse tree
     */
    void exitLastElement(YJSParser.LastElementContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#objectLiteral}.
     * 
     * @param ctx the parse tree
     */
    void enterObjectLiteral(YJSParser.ObjectLiteralContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#objectLiteral}.
     * 
     * @param ctx the parse tree
     */
    void exitObjectLiteral(YJSParser.ObjectLiteralContext ctx);

    /**
     * Enter a parse tree produced by the {@code PropertyExpressionAssignment} labeled alternative
     * in {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void enterPropertyExpressionAssignment(YJSParser.PropertyExpressionAssignmentContext ctx);

    /**
     * Exit a parse tree produced by the {@code PropertyExpressionAssignment} labeled alternative in
     * {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void exitPropertyExpressionAssignment(YJSParser.PropertyExpressionAssignmentContext ctx);

    /**
     * Enter a parse tree produced by the {@code ComputedPropertyExpressionAssignment} labeled
     * alternative in {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void enterComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx);

    /**
     * Exit a parse tree produced by the {@code ComputedPropertyExpressionAssignment} labeled
     * alternative in {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void exitComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx);

    /**
     * Enter a parse tree produced by the {@code PropertyShorthand} labeled alternative in
     * {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void enterPropertyShorthand(YJSParser.PropertyShorthandContext ctx);

    /**
     * Exit a parse tree produced by the {@code PropertyShorthand} labeled alternative in
     * {@link YJSParser#propertyAssignment}.
     * 
     * @param ctx the parse tree
     */
    void exitPropertyShorthand(YJSParser.PropertyShorthandContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#propertyName}.
     * 
     * @param ctx the parse tree
     */
    void enterPropertyName(YJSParser.PropertyNameContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#propertyName}.
     * 
     * @param ctx the parse tree
     */
    void exitPropertyName(YJSParser.PropertyNameContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#arguments}.
     * 
     * @param ctx the parse tree
     */
    void enterArguments(YJSParser.ArgumentsContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#arguments}.
     * 
     * @param ctx the parse tree
     */
    void exitArguments(YJSParser.ArgumentsContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#lastArgument}.
     * 
     * @param ctx the parse tree
     */
    void enterLastArgument(YJSParser.LastArgumentContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#lastArgument}.
     * 
     * @param ctx the parse tree
     */
    void exitLastArgument(YJSParser.LastArgumentContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#expressionSequence}.
     * 
     * @param ctx the parse tree
     */
    void enterExpressionSequence(YJSParser.ExpressionSequenceContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#expressionSequence}.
     * 
     * @param ctx the parse tree
     */
    void exitExpressionSequence(YJSParser.ExpressionSequenceContext ctx);

    /**
     * Enter a parse tree produced by the {@code TemplateStringExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code TemplateStringExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code TernaryExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterTernaryExpression(YJSParser.TernaryExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code TernaryExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitTernaryExpression(YJSParser.TernaryExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code LogicalAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code LogicalAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code PreIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code PreIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ObjectLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ObjectLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code InExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterInExpression(YJSParser.InExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code InExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitInExpression(YJSParser.InExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code LogicalOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code LogicalOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code NotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterNotExpression(YJSParser.NotExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code NotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitNotExpression(YJSParser.NotExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code PreDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code PreDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ArgumentsExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ArgumentsExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ThisExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterThisExpression(YJSParser.ThisExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ThisExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitThisExpression(YJSParser.ThisExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code UnaryMinusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code UnaryMinusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code AssignmentExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterAssignmentExpression(YJSParser.AssignmentExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code AssignmentExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitAssignmentExpression(YJSParser.AssignmentExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code PostDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code PostDecreaseExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code TypeofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterTypeofExpression(YJSParser.TypeofExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code TypeofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitTypeofExpression(YJSParser.TypeofExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code InstanceofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterInstanceofExpression(YJSParser.InstanceofExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code InstanceofExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitInstanceofExpression(YJSParser.InstanceofExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code UnaryPlusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code UnaryPlusExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ArrowFunctionExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ArrowFunctionExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code EqualityExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterEqualityExpression(YJSParser.EqualityExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code EqualityExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitEqualityExpression(YJSParser.EqualityExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code BitXOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterBitXOrExpression(YJSParser.BitXOrExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code BitXOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitBitXOrExpression(YJSParser.BitXOrExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code SuperExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterSuperExpression(YJSParser.SuperExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code SuperExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitSuperExpression(YJSParser.SuperExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code MultiplicativeExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code MultiplicativeExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code BitShiftExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterBitShiftExpression(YJSParser.BitShiftExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code BitShiftExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitBitShiftExpression(YJSParser.BitShiftExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ParenthesizedExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ParenthesizedExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code AdditiveExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterAdditiveExpression(YJSParser.AdditiveExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code AdditiveExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitAdditiveExpression(YJSParser.AdditiveExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code RelationalExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterRelationalExpression(YJSParser.RelationalExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code RelationalExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitRelationalExpression(YJSParser.RelationalExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code PostIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code PostIncrementExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code BitNotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterBitNotExpression(YJSParser.BitNotExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code BitNotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitBitNotExpression(YJSParser.BitNotExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code NewExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterNewExpression(YJSParser.NewExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code NewExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitNewExpression(YJSParser.NewExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code LiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterLiteralExpression(YJSParser.LiteralExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code LiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitLiteralExpression(YJSParser.LiteralExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code ArrayLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code ArrayLiteralExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code MemberDotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterMemberDotExpression(YJSParser.MemberDotExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code MemberDotExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitMemberDotExpression(YJSParser.MemberDotExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code MemberIndexExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code MemberIndexExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code IdentifierExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterIdentifierExpression(YJSParser.IdentifierExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code IdentifierExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitIdentifierExpression(YJSParser.IdentifierExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code BitAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterBitAndExpression(YJSParser.BitAndExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code BitAndExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitBitAndExpression(YJSParser.BitAndExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code BitOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterBitOrExpression(YJSParser.BitOrExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code BitOrExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitBitOrExpression(YJSParser.BitOrExpressionContext ctx);

    /**
     * Enter a parse tree produced by the {@code AssignmentOperatorExpression} labeled alternative
     * in {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void enterAssignmentOperatorExpression(YJSParser.AssignmentOperatorExpressionContext ctx);

    /**
     * Exit a parse tree produced by the {@code AssignmentOperatorExpression} labeled alternative in
     * {@link YJSParser#singleExpression}.
     * 
     * @param ctx the parse tree
     */
    void exitAssignmentOperatorExpression(YJSParser.AssignmentOperatorExpressionContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#arrowFunctionParameters}.
     * 
     * @param ctx the parse tree
     */
    void enterArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#arrowFunctionParameters}.
     * 
     * @param ctx the parse tree
     */
    void exitArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#arrowFunctionBody}.
     * 
     * @param ctx the parse tree
     */
    void enterArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#arrowFunctionBody}.
     * 
     * @param ctx the parse tree
     */
    void exitArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#assignmentOperator}.
     * 
     * @param ctx the parse tree
     */
    void enterAssignmentOperator(YJSParser.AssignmentOperatorContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#assignmentOperator}.
     * 
     * @param ctx the parse tree
     */
    void exitAssignmentOperator(YJSParser.AssignmentOperatorContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#literal}.
     * 
     * @param ctx the parse tree
     */
    void enterLiteral(YJSParser.LiteralContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#literal}.
     * 
     * @param ctx the parse tree
     */
    void exitLiteral(YJSParser.LiteralContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#numericLiteral}.
     * 
     * @param ctx the parse tree
     */
    void enterNumericLiteral(YJSParser.NumericLiteralContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#numericLiteral}.
     * 
     * @param ctx the parse tree
     */
    void exitNumericLiteral(YJSParser.NumericLiteralContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#identifierName}.
     * 
     * @param ctx the parse tree
     */
    void enterIdentifierName(YJSParser.IdentifierNameContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#identifierName}.
     * 
     * @param ctx the parse tree
     */
    void exitIdentifierName(YJSParser.IdentifierNameContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#reservedWord}.
     * 
     * @param ctx the parse tree
     */
    void enterReservedWord(YJSParser.ReservedWordContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#reservedWord}.
     * 
     * @param ctx the parse tree
     */
    void exitReservedWord(YJSParser.ReservedWordContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#keyword}.
     * 
     * @param ctx the parse tree
     */
    void enterKeyword(YJSParser.KeywordContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#keyword}.
     * 
     * @param ctx the parse tree
     */
    void exitKeyword(YJSParser.KeywordContext ctx);

    /**
     * Enter a parse tree produced by {@link YJSParser#eos}.
     * 
     * @param ctx the parse tree
     */
    void enterEos(YJSParser.EosContext ctx);

    /**
     * Exit a parse tree produced by {@link YJSParser#eos}.
     * 
     * @param ctx the parse tree
     */
    void exitEos(YJSParser.EosContext ctx);
}
