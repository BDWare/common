// Generated from YJSParser.g4 by ANTLR 4.9.2
package org.bdware.sc.parser;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link YJSParserListener}, which can be extended
 * to create a listener which only needs to handle a subset of the available methods.
 */
public class YJSParserBaseListener implements YJSParserListener {
    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterProgram(YJSParser.ProgramContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitProgram(YJSParser.ProgramContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterContractDeclar(YJSParser.ContractDeclarContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitContractDeclar(YJSParser.ContractDeclarContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAnnotations(YJSParser.AnnotationsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAnnotations(YJSParser.AnnotationsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAnnotation(YJSParser.AnnotationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAnnotation(YJSParser.AnnotationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAnnotationArgs(YJSParser.AnnotationArgsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAnnotationArgs(YJSParser.AnnotationArgsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAnnotationLiteral(YJSParser.AnnotationLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitClzOrFunctionDeclaration(YJSParser.ClzOrFunctionDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitInterfaceDeclaration(YJSParser.InterfaceDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEventDeclaration(YJSParser.EventDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEventDeclaration(YJSParser.EventDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEventGlobalOrLocal(YJSParser.EventGlobalOrLocalContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEventSemantics(YJSParser.EventSemanticsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEventSemantics(YJSParser.EventSemanticsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSourceElement(YJSParser.SourceElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSourceElement(YJSParser.SourceElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterImportStmts(YJSParser.ImportStmtsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitImportStmts(YJSParser.ImportStmtsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterImportStmt(YJSParser.ImportStmtContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitImportStmt(YJSParser.ImportStmtContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterExportStmt(YJSParser.ExportStmtContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitExportStmt(YJSParser.ExportStmtContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVersionName(YJSParser.VersionNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVersionName(YJSParser.VersionNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterStatement(YJSParser.StatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitStatement(YJSParser.StatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBlock(YJSParser.BlockContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBlock(YJSParser.BlockContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterStatementList(YJSParser.StatementListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitStatementList(YJSParser.StatementListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSharableDeclaration(YJSParser.SharableDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSharableDeclaration(YJSParser.SharableDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSharableStatement(YJSParser.SharableStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSharableStatement(YJSParser.SharableStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSharableModifier(YJSParser.SharableModifierContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSharableModifier(YJSParser.SharableModifierContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVariableStatement(YJSParser.VariableStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVariableStatement(YJSParser.VariableStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVariableDeclaration(YJSParser.VariableDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVariableDeclaration(YJSParser.VariableDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEmptyStatement(YJSParser.EmptyStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEmptyStatement(YJSParser.EmptyStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterExpressionStatement(YJSParser.ExpressionStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitExpressionStatement(YJSParser.ExpressionStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterIfStatement(YJSParser.IfStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitIfStatement(YJSParser.IfStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterDoStatement(YJSParser.DoStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitDoStatement(YJSParser.DoStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterWhileStatement(YJSParser.WhileStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitWhileStatement(YJSParser.WhileStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterForStatement(YJSParser.ForStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitForStatement(YJSParser.ForStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterForVarStatement(YJSParser.ForVarStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitForVarStatement(YJSParser.ForVarStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterForInStatement(YJSParser.ForInStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitForInStatement(YJSParser.ForInStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterForVarInStatement(YJSParser.ForVarInStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitForVarInStatement(YJSParser.ForVarInStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVarModifier(YJSParser.VarModifierContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVarModifier(YJSParser.VarModifierContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterContinueStatement(YJSParser.ContinueStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitContinueStatement(YJSParser.ContinueStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBreakStatement(YJSParser.BreakStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBreakStatement(YJSParser.BreakStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterReturnStatement(YJSParser.ReturnStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitReturnStatement(YJSParser.ReturnStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterWithStatement(YJSParser.WithStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitWithStatement(YJSParser.WithStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSwitchStatement(YJSParser.SwitchStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSwitchStatement(YJSParser.SwitchStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterCaseBlock(YJSParser.CaseBlockContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitCaseBlock(YJSParser.CaseBlockContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterCaseClauses(YJSParser.CaseClausesContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitCaseClauses(YJSParser.CaseClausesContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterCaseClause(YJSParser.CaseClauseContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitCaseClause(YJSParser.CaseClauseContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterDefaultClause(YJSParser.DefaultClauseContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitDefaultClause(YJSParser.DefaultClauseContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterThrowStatement(YJSParser.ThrowStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitThrowStatement(YJSParser.ThrowStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterTryStatement(YJSParser.TryStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitTryStatement(YJSParser.TryStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterCatchProduction(YJSParser.CatchProductionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitCatchProduction(YJSParser.CatchProductionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterFinallyProduction(YJSParser.FinallyProductionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitFinallyProduction(YJSParser.FinallyProductionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterDebuggerStatement(YJSParser.DebuggerStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitDebuggerStatement(YJSParser.DebuggerStatementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitFunctionDeclaration(YJSParser.FunctionDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterClassDeclaration(YJSParser.ClassDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitClassDeclaration(YJSParser.ClassDeclarationContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterClassTail(YJSParser.ClassTailContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitClassTail(YJSParser.ClassTailContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterClassElement(YJSParser.ClassElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitClassElement(YJSParser.ClassElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterMethodDefinition(YJSParser.MethodDefinitionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitMethodDefinition(YJSParser.MethodDefinitionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterFormalParameterList(YJSParser.FormalParameterListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitFormalParameterList(YJSParser.FormalParameterListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterFormalParameterArg(YJSParser.FormalParameterArgContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitFormalParameterArg(YJSParser.FormalParameterArgContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLastFormalParameterArg(YJSParser.LastFormalParameterArgContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterFunctionBody(YJSParser.FunctionBodyContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitFunctionBody(YJSParser.FunctionBodyContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSourceElements(YJSParser.SourceElementsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSourceElements(YJSParser.SourceElementsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArrayLiteral(YJSParser.ArrayLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArrayLiteral(YJSParser.ArrayLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterElementList(YJSParser.ElementListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitElementList(YJSParser.ElementListContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLastElement(YJSParser.LastElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLastElement(YJSParser.LastElementContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterObjectLiteral(YJSParser.ObjectLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitObjectLiteral(YJSParser.ObjectLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPropertyExpressionAssignment(
            YJSParser.PropertyExpressionAssignmentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPropertyExpressionAssignment(
            YJSParser.PropertyExpressionAssignmentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitComputedPropertyExpressionAssignment(
            YJSParser.ComputedPropertyExpressionAssignmentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPropertyShorthand(YJSParser.PropertyShorthandContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPropertyShorthand(YJSParser.PropertyShorthandContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPropertyName(YJSParser.PropertyNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPropertyName(YJSParser.PropertyNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArguments(YJSParser.ArgumentsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArguments(YJSParser.ArgumentsContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLastArgument(YJSParser.LastArgumentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLastArgument(YJSParser.LastArgumentContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterExpressionSequence(YJSParser.ExpressionSequenceContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitExpressionSequence(YJSParser.ExpressionSequenceContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitTemplateStringExpression(YJSParser.TemplateStringExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterTernaryExpression(YJSParser.TernaryExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitTernaryExpression(YJSParser.TernaryExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLogicalAndExpression(YJSParser.LogicalAndExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPreIncrementExpression(YJSParser.PreIncrementExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitObjectLiteralExpression(YJSParser.ObjectLiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterInExpression(YJSParser.InExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitInExpression(YJSParser.InExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLogicalOrExpression(YJSParser.LogicalOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterNotExpression(YJSParser.NotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitNotExpression(YJSParser.NotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPreDecreaseExpression(YJSParser.PreDecreaseExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterThisExpression(YJSParser.ThisExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitThisExpression(YJSParser.ThisExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitUnaryMinusExpression(YJSParser.UnaryMinusExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAssignmentExpression(YJSParser.AssignmentExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAssignmentExpression(YJSParser.AssignmentExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPostDecreaseExpression(YJSParser.PostDecreaseExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterTypeofExpression(YJSParser.TypeofExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitTypeofExpression(YJSParser.TypeofExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterInstanceofExpression(YJSParser.InstanceofExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitInstanceofExpression(YJSParser.InstanceofExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitUnaryPlusExpression(YJSParser.UnaryPlusExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArrowFunctionExpression(YJSParser.ArrowFunctionExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEqualityExpression(YJSParser.EqualityExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEqualityExpression(YJSParser.EqualityExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBitXOrExpression(YJSParser.BitXOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBitXOrExpression(YJSParser.BitXOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSuperExpression(YJSParser.SuperExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSuperExpression(YJSParser.SuperExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitMultiplicativeExpression(YJSParser.MultiplicativeExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBitShiftExpression(YJSParser.BitShiftExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBitShiftExpression(YJSParser.BitShiftExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitParenthesizedExpression(YJSParser.ParenthesizedExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAdditiveExpression(YJSParser.AdditiveExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAdditiveExpression(YJSParser.AdditiveExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterRelationalExpression(YJSParser.RelationalExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitRelationalExpression(YJSParser.RelationalExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPostIncrementExpression(YJSParser.PostIncrementExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBitNotExpression(YJSParser.BitNotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBitNotExpression(YJSParser.BitNotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterNewExpression(YJSParser.NewExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitNewExpression(YJSParser.NewExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLiteralExpression(YJSParser.LiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLiteralExpression(YJSParser.LiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArrayLiteralExpression(YJSParser.ArrayLiteralExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterMemberDotExpression(YJSParser.MemberDotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitMemberDotExpression(YJSParser.MemberDotExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitMemberIndexExpression(YJSParser.MemberIndexExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterIdentifierExpression(YJSParser.IdentifierExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitIdentifierExpression(YJSParser.IdentifierExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBitAndExpression(YJSParser.BitAndExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBitAndExpression(YJSParser.BitAndExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBitOrExpression(YJSParser.BitOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBitOrExpression(YJSParser.BitOrExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAssignmentOperatorExpression(
            YJSParser.AssignmentOperatorExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAssignmentOperatorExpression(
            YJSParser.AssignmentOperatorExpressionContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArrowFunctionParameters(YJSParser.ArrowFunctionParametersContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitArrowFunctionBody(YJSParser.ArrowFunctionBodyContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAssignmentOperator(YJSParser.AssignmentOperatorContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAssignmentOperator(YJSParser.AssignmentOperatorContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterLiteral(YJSParser.LiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitLiteral(YJSParser.LiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterNumericLiteral(YJSParser.NumericLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitNumericLiteral(YJSParser.NumericLiteralContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterIdentifierName(YJSParser.IdentifierNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitIdentifierName(YJSParser.IdentifierNameContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterReservedWord(YJSParser.ReservedWordContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitReservedWord(YJSParser.ReservedWordContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterKeyword(YJSParser.KeywordContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitKeyword(YJSParser.KeywordContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEos(YJSParser.EosContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEos(YJSParser.EosContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterEveryRule(ParserRuleContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitEveryRule(ParserRuleContext ctx) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void visitTerminal(TerminalNode node) {}

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void visitErrorNode(ErrorNode node) {}
}
