// Generated from YJSParser.g4 by ANTLR 4.9.2
package org.bdware.sc.parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class YJSParser extends JavaScriptBaseParser {
    static {
        RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    public static final int MultiLineComment = 1, SingleLineComment = 2,
            RegularExpressionLiteral = 3, OpenBracket = 4, CloseBracket = 5, OpenParen = 6,
            CloseParen = 7, OpenBrace = 8, CloseBrace = 9, SemiColon = 10, Comma = 11, Assign = 12,
            QuestionMark = 13, Colon = 14, Ellipsis = 15, Dot = 16, PlusPlus = 17, MinusMinus = 18,
            Plus = 19, Minus = 20, BitNot = 21, Not = 22, Multiply = 23, Divide = 24, Modulus = 25,
            RightShiftArithmetic = 26, LeftShiftArithmetic = 27, RightShiftLogical = 28,
            LessThan = 29, MoreThan = 30, LessThanEquals = 31, GreaterThanEquals = 32, Equals_ = 33,
            NotEquals = 34, IdentityEquals = 35, IdentityNotEquals = 36, BitAnd = 37, BitXOr = 38,
            BitOr = 39, And = 40, Or = 41, MultiplyAssign = 42, DivideAssign = 43,
            ModulusAssign = 44, PlusAssign = 45, MinusAssign = 46, LeftShiftArithmeticAssign = 47,
            RightShiftArithmeticAssign = 48, RightShiftLogicalAssign = 49, BitAndAssign = 50,
            BitXorAssign = 51, BitOrAssign = 52, ARROW = 53, NullLiteral = 54, BooleanLiteral = 55,
            DecimalLiteral = 56, HexIntegerLiteral = 57, OctalIntegerLiteral = 58,
            OctalIntegerLiteral2 = 59, BinaryIntegerLiteral = 60, Break = 61, Do = 62,
            Instanceof = 63, Typeof = 64, Case = 65, Else = 66, New = 67, Var = 68, Catch = 69,
            Finally = 70, Return = 71, Void = 72, Continue = 73, For = 74, Switch = 75, While = 76,
            Debugger = 77, Function = 78, This = 79, With = 80, Default = 81, If = 82, Throw = 83,
            Delete = 84, In = 85, Try = 86, Event = 87, AtToken = 88, Sharable = 89,
            AtLeastOnce = 90, AtMostOnce = 91, OnlyOnce = 92, Global = 93, Local = 94, View = 95,
            Class = 96, Enum = 97, Extends = 98, Super = 99, Const = 100, Export = 101,
            Import = 102, Contract = 103, Module = 104, Oracle = 105, DoipModule = 106,
            Implements = 107, Let = 108, Private = 109, Public = 110, Interface = 111,
            Package = 112, Protected = 113, Static = 114, Yield = 115, Identifier = 116,
            StringLiteral = 117, TemplateStringLiteral = 118, WhiteSpaces = 119,
            LineTerminator = 120, HtmlComment = 121, CDataComment = 122, UnexpectedCharacter = 123;
    public static final int RULE_program = 0, RULE_contractDeclar = 1, RULE_annotations = 2,
            RULE_annotation = 3, RULE_annotationArgs = 4, RULE_annotationLiteral = 5,
            RULE_clzOrFunctionDeclaration = 6, RULE_interfaceDeclaration = 7,
            RULE_eventDeclaration = 8, RULE_eventGlobalOrLocal = 9, RULE_eventSemantics = 10,
            RULE_sourceElement = 11, RULE_importStmts = 12, RULE_importStmt = 13,
            RULE_exportStmt = 14, RULE_versionName = 15, RULE_statement = 16, RULE_block = 17,
            RULE_statementList = 18, RULE_sharableDeclaration = 19, RULE_sharableStatement = 20,
            RULE_sharableModifier = 21, RULE_variableStatement = 22,
            RULE_variableDeclarationList = 23, RULE_variableDeclaration = 24,
            RULE_emptyStatement = 25, RULE_expressionStatement = 26, RULE_ifStatement = 27,
            RULE_iterationStatement = 28, RULE_varModifier = 29, RULE_continueStatement = 30,
            RULE_breakStatement = 31, RULE_returnStatement = 32, RULE_withStatement = 33,
            RULE_switchStatement = 34, RULE_caseBlock = 35, RULE_caseClauses = 36,
            RULE_caseClause = 37, RULE_defaultClause = 38, RULE_throwStatement = 39,
            RULE_tryStatement = 40, RULE_catchProduction = 41, RULE_finallyProduction = 42,
            RULE_debuggerStatement = 43, RULE_functionDeclaration = 44, RULE_classDeclaration = 45,
            RULE_classTail = 46, RULE_classElement = 47, RULE_methodDefinition = 48,
            RULE_formalParameterList = 49, RULE_formalParameterArg = 50,
            RULE_lastFormalParameterArg = 51, RULE_functionBody = 52, RULE_sourceElements = 53,
            RULE_arrayLiteral = 54, RULE_elementList = 55, RULE_lastElement = 56,
            RULE_objectLiteral = 57, RULE_propertyAssignment = 58, RULE_propertyName = 59,
            RULE_arguments = 60, RULE_lastArgument = 61, RULE_expressionSequence = 62,
            RULE_singleExpression = 63, RULE_arrowFunctionParameters = 64,
            RULE_arrowFunctionBody = 65, RULE_assignmentOperator = 66, RULE_literal = 67,
            RULE_numericLiteral = 68, RULE_identifierName = 69, RULE_reservedWord = 70,
            RULE_keyword = 71, RULE_eos = 72;

    private static String[] makeRuleNames() {
        return new String[] {"program", "contractDeclar", "annotations", "annotation",
                "annotationArgs", "annotationLiteral", "clzOrFunctionDeclaration",
                "interfaceDeclaration", "eventDeclaration", "eventGlobalOrLocal", "eventSemantics",
                "sourceElement", "importStmts", "importStmt", "exportStmt", "versionName",
                "statement", "block", "statementList", "sharableDeclaration", "sharableStatement",
                "sharableModifier", "variableStatement", "variableDeclarationList",
                "variableDeclaration", "emptyStatement", "expressionStatement", "ifStatement",
                "iterationStatement", "varModifier", "continueStatement", "breakStatement",
                "returnStatement", "withStatement", "switchStatement", "caseBlock", "caseClauses",
                "caseClause", "defaultClause", "throwStatement", "tryStatement", "catchProduction",
                "finallyProduction", "debuggerStatement", "functionDeclaration", "classDeclaration",
                "classTail", "classElement", "methodDefinition", "formalParameterList",
                "formalParameterArg", "lastFormalParameterArg", "functionBody", "sourceElements",
                "arrayLiteral", "elementList", "lastElement", "objectLiteral", "propertyAssignment",
                "propertyName", "arguments", "lastArgument", "expressionSequence",
                "singleExpression", "arrowFunctionParameters", "arrowFunctionBody",
                "assignmentOperator", "literal", "numericLiteral", "identifierName", "reservedWord",
                "keyword", "eos"};
    }

    public static final String[] ruleNames = makeRuleNames();

    private static String[] makeLiteralNames() {
        return new String[] {null, null, null, null, "'['", "']'", "'('", "')'", "'{'", "'}'",
                "';'", "','", "'='", "'?'", "':'", "'...'", "'.'", "'++'", "'--'", "'+'", "'-'",
                "'~'", "'!'", "'*'", "'/'", "'%'", "'>>'", "'<<'", "'>>>'", "'<'", "'>'", "'<='",
                "'>='", "'=='", "'!='", "'==='", "'!=='", "'&'", "'^'", "'|'", "'&&'", "'||'",
                "'*='", "'/='", "'%='", "'+='", "'-='", "'<<='", "'>>='", "'>>>='", "'&='", "'^='",
                "'|='", "'=>'", "'null'", null, null, null, null, null, null, "'break'", "'do'",
                "'instanceof'", "'typeof'", "'case'", "'else'", "'new'", "'var'", "'catch'",
                "'finally'", "'return'", "'void'", "'continue'", "'for'", "'switch'", "'while'",
                "'debugger'", "'function'", "'this'", "'with'", "'default'", "'if'", "'throw'",
                "'delete'", "'in'", "'try'", "'event'", "'@'", "'sharable'", "'AT_LEAST_ONCE'",
                "'AT_MOST_ONCE'", "'ONLY_ONCE'", "'global'", "'local'", "'view'", "'class'",
                "'enum'", "'extends'", "'super'", "'const'", "'export'", "'import'", "'contract'",
                "'module'", "'oracle'", "'doipmodule'", "'implements'", "'let'", "'private'",
                "'public'", "'interface'", "'package'", "'protected'", "'static'", "'yield'"};
    }

    private static final String[] _LITERAL_NAMES = makeLiteralNames();

    private static String[] makeSymbolicNames() {
        return new String[] {null, "MultiLineComment", "SingleLineComment",
                "RegularExpressionLiteral", "OpenBracket", "CloseBracket", "OpenParen",
                "CloseParen", "OpenBrace", "CloseBrace", "SemiColon", "Comma", "Assign",
                "QuestionMark", "Colon", "Ellipsis", "Dot", "PlusPlus", "MinusMinus", "Plus",
                "Minus", "BitNot", "Not", "Multiply", "Divide", "Modulus", "RightShiftArithmetic",
                "LeftShiftArithmetic", "RightShiftLogical", "LessThan", "MoreThan",
                "LessThanEquals", "GreaterThanEquals", "Equals_", "NotEquals", "IdentityEquals",
                "IdentityNotEquals", "BitAnd", "BitXOr", "BitOr", "And", "Or", "MultiplyAssign",
                "DivideAssign", "ModulusAssign", "PlusAssign", "MinusAssign",
                "LeftShiftArithmeticAssign", "RightShiftArithmeticAssign",
                "RightShiftLogicalAssign", "BitAndAssign", "BitXorAssign", "BitOrAssign", "ARROW",
                "NullLiteral", "BooleanLiteral", "DecimalLiteral", "HexIntegerLiteral",
                "OctalIntegerLiteral", "OctalIntegerLiteral2", "BinaryIntegerLiteral", "Break",
                "Do", "Instanceof", "Typeof", "Case", "Else", "New", "Var", "Catch", "Finally",
                "Return", "Void", "Continue", "For", "Switch", "While", "Debugger", "Function",
                "This", "With", "Default", "If", "Throw", "Delete", "In", "Try", "Event", "AtToken",
                "Sharable", "AtLeastOnce", "AtMostOnce", "OnlyOnce", "Global", "Local", "View",
                "Class", "Enum", "Extends", "Super", "Const", "Export", "Import", "Contract",
                "Module", "Oracle", "DoipModule", "Implements", "Let", "Private", "Public",
                "Interface", "Package", "Protected", "Static", "Yield", "Identifier",
                "StringLiteral", "TemplateStringLiteral", "WhiteSpaces", "LineTerminator",
                "HtmlComment", "CDataComment", "UnexpectedCharacter"};
    }

    private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;
    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "YJSParser.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public YJSParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class ProgramContext extends ParserRuleContext {
        public ContractDeclarContext contractDeclar() {
            return getRuleContext(ContractDeclarContext.class, 0);
        }

        public ImportStmtsContext importStmts() {
            return getRuleContext(ImportStmtsContext.class, 0);
        }

        public ProgramContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_program;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterProgram(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitProgram(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitProgram(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ProgramContext program() throws RecognitionException {
        ProgramContext _localctx = new ProgramContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_program);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(147);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Import) {
                    {
                        setState(146);
                        importStmts();
                    }
                }

                setState(149);
                contractDeclar();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ContractDeclarContext extends ParserRuleContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public TerminalNode Contract() {
            return getToken(YJSParser.Contract, 0);
        }

        public TerminalNode Module() {
            return getToken(YJSParser.Module, 0);
        }

        public TerminalNode Oracle() {
            return getToken(YJSParser.Oracle, 0);
        }

        public TerminalNode DoipModule() {
            return getToken(YJSParser.DoipModule, 0);
        }

        public AnnotationsContext annotations() {
            return getRuleContext(AnnotationsContext.class, 0);
        }

        public List<ClzOrFunctionDeclarationContext> clzOrFunctionDeclaration() {
            return getRuleContexts(ClzOrFunctionDeclarationContext.class);
        }

        public ClzOrFunctionDeclarationContext clzOrFunctionDeclaration(int i) {
            return getRuleContext(ClzOrFunctionDeclarationContext.class, i);
        }

        public ContractDeclarContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_contractDeclar;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterContractDeclar(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitContractDeclar(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitContractDeclar(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ContractDeclarContext contractDeclar() throws RecognitionException {
        ContractDeclarContext _localctx = new ContractDeclarContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_contractDeclar);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(152);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == AtToken) {
                    {
                        setState(151);
                        annotations();
                    }
                }

                setState(154);
                _la = _input.LA(1);
                if (!(((((_la - 103)) & ~0x3f) == 0
                        && ((1L << (_la - 103)) & ((1L << (Contract - 103)) | (1L << (Module - 103))
                                | (1L << (Oracle - 103)) | (1L << (DoipModule - 103)))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
                setState(155);
                match(Identifier);
                setState(156);
                match(OpenBrace);
                setState(158);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(157);
                            clzOrFunctionDeclaration();
                        }
                    }
                    setState(160);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (((((_la - 78)) & ~0x3f) == 0 && ((1L << (_la - 78))
                        & ((1L << (Function - 78)) | (1L << (Event - 78)) | (1L << (AtToken - 78))
                                | (1L << (Sharable - 78)) | (1L << (Class - 78))
                                | (1L << (Export - 78)) | (1L << (Interface - 78)))) != 0));
                setState(162);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AnnotationsContext extends ParserRuleContext {
        public List<AnnotationContext> annotation() {
            return getRuleContexts(AnnotationContext.class);
        }

        public AnnotationContext annotation(int i) {
            return getRuleContext(AnnotationContext.class, i);
        }

        public AnnotationsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_annotations;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAnnotations(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAnnotations(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAnnotations(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AnnotationsContext annotations() throws RecognitionException {
        AnnotationsContext _localctx = new AnnotationsContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_annotations);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(165);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(164);
                            annotation();
                        }
                    }
                    setState(167);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == AtToken);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AnnotationContext extends ParserRuleContext {
        public TerminalNode AtToken() {
            return getToken(YJSParser.AtToken, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public AnnotationArgsContext annotationArgs() {
            return getRuleContext(AnnotationArgsContext.class, 0);
        }

        public AnnotationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_annotation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAnnotation(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAnnotation(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAnnotation(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AnnotationContext annotation() throws RecognitionException {
        AnnotationContext _localctx = new AnnotationContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_annotation);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(169);
                match(AtToken);
                setState(170);
                match(Identifier);
                setState(171);
                match(OpenParen);
                setState(173);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << OpenBrace) | (1L << DecimalLiteral) | (1L << HexIntegerLiteral)
                                | (1L << OctalIntegerLiteral) | (1L << OctalIntegerLiteral2)
                                | (1L << BinaryIntegerLiteral))) != 0)
                        || _la == StringLiteral) {
                    {
                        setState(172);
                        annotationArgs();
                    }
                }

                setState(175);
                match(CloseParen);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AnnotationArgsContext extends ParserRuleContext {
        public List<AnnotationLiteralContext> annotationLiteral() {
            return getRuleContexts(AnnotationLiteralContext.class);
        }

        public AnnotationLiteralContext annotationLiteral(int i) {
            return getRuleContext(AnnotationLiteralContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public AnnotationArgsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_annotationArgs;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAnnotationArgs(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAnnotationArgs(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAnnotationArgs(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AnnotationArgsContext annotationArgs() throws RecognitionException {
        AnnotationArgsContext _localctx = new AnnotationArgsContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_annotationArgs);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(177);
                annotationLiteral();
                setState(182);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(178);
                            match(Comma);
                            setState(179);
                            annotationLiteral();
                        }
                    }
                    setState(184);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AnnotationLiteralContext extends ParserRuleContext {
        public NumericLiteralContext numericLiteral() {
            return getRuleContext(NumericLiteralContext.class, 0);
        }

        public TerminalNode StringLiteral() {
            return getToken(YJSParser.StringLiteral, 0);
        }

        public ObjectLiteralContext objectLiteral() {
            return getRuleContext(ObjectLiteralContext.class, 0);
        }

        public AnnotationLiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_annotationLiteral;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAnnotationLiteral(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAnnotationLiteral(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAnnotationLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AnnotationLiteralContext annotationLiteral() throws RecognitionException {
        AnnotationLiteralContext _localctx = new AnnotationLiteralContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_annotationLiteral);
        try {
            setState(188);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case DecimalLiteral:
                case HexIntegerLiteral:
                case OctalIntegerLiteral:
                case OctalIntegerLiteral2:
                case BinaryIntegerLiteral:
                    enterOuterAlt(_localctx, 1); {
                    setState(185);
                    numericLiteral();
                }
                    break;
                case StringLiteral:
                    enterOuterAlt(_localctx, 2); {
                    setState(186);
                    match(StringLiteral);
                }
                    break;
                case OpenBrace:
                    enterOuterAlt(_localctx, 3); {
                    setState(187);
                    objectLiteral();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ClzOrFunctionDeclarationContext extends ParserRuleContext {
        public ClassDeclarationContext classDeclaration() {
            return getRuleContext(ClassDeclarationContext.class, 0);
        }

        public FunctionDeclarationContext functionDeclaration() {
            return getRuleContext(FunctionDeclarationContext.class, 0);
        }

        public InterfaceDeclarationContext interfaceDeclaration() {
            return getRuleContext(InterfaceDeclarationContext.class, 0);
        }

        public EventDeclarationContext eventDeclaration() {
            return getRuleContext(EventDeclarationContext.class, 0);
        }

        public SharableDeclarationContext sharableDeclaration() {
            return getRuleContext(SharableDeclarationContext.class, 0);
        }

        public ClzOrFunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_clzOrFunctionDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterClzOrFunctionDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitClzOrFunctionDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitClzOrFunctionDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ClzOrFunctionDeclarationContext clzOrFunctionDeclaration()
            throws RecognitionException {
        ClzOrFunctionDeclarationContext _localctx =
                new ClzOrFunctionDeclarationContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_clzOrFunctionDeclaration);
        try {
            setState(195);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 7, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1); {
                    setState(190);
                    classDeclaration();
                }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2); {
                    setState(191);
                    functionDeclaration();
                }
                    break;
                case 3:
                    enterOuterAlt(_localctx, 3); {
                    setState(192);
                    interfaceDeclaration();
                }
                    break;
                case 4:
                    enterOuterAlt(_localctx, 4); {
                    setState(193);
                    eventDeclaration();
                }
                    break;
                case 5:
                    enterOuterAlt(_localctx, 5); {
                    setState(194);
                    sharableDeclaration();
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class InterfaceDeclarationContext extends ParserRuleContext {
        public TerminalNode Interface() {
            return getToken(YJSParser.Interface, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public AnnotationsContext annotations() {
            return getRuleContext(AnnotationsContext.class, 0);
        }

        public FormalParameterListContext formalParameterList() {
            return getRuleContext(FormalParameterListContext.class, 0);
        }

        public InterfaceDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_interfaceDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterInterfaceDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitInterfaceDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitInterfaceDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final InterfaceDeclarationContext interfaceDeclaration() throws RecognitionException {
        InterfaceDeclarationContext _localctx = new InterfaceDeclarationContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_interfaceDeclaration);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(198);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == AtToken) {
                    {
                        setState(197);
                        annotations();
                    }
                }

                setState(200);
                match(Interface);
                setState(201);
                match(Identifier);
                setState(202);
                match(OpenParen);
                setState(204);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << OpenBracket) | (1L << OpenBrace) | (1L << Ellipsis))) != 0)
                        || _la == Identifier) {
                    {
                        setState(203);
                        formalParameterList();
                    }
                }

                setState(206);
                match(CloseParen);
                setState(207);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EventDeclarationContext extends ParserRuleContext {
        public TerminalNode Event() {
            return getToken(YJSParser.Event, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode SemiColon() {
            return getToken(YJSParser.SemiColon, 0);
        }

        public EventGlobalOrLocalContext eventGlobalOrLocal() {
            return getRuleContext(EventGlobalOrLocalContext.class, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public EventSemanticsContext eventSemantics() {
            return getRuleContext(EventSemanticsContext.class, 0);
        }

        public EventDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_eventDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEventDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEventDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEventDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final EventDeclarationContext eventDeclaration() throws RecognitionException {
        EventDeclarationContext _localctx = new EventDeclarationContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_eventDeclaration);
        int _la;
        try {
            setState(226);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 13, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1); {
                    setState(209);
                    match(Event);
                    setState(211);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if (_la == Global || _la == Local) {
                        {
                            setState(210);
                            eventGlobalOrLocal();
                        }
                    }

                    setState(213);
                    match(Identifier);
                    setState(214);
                    match(SemiColon);
                }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2); {
                    setState(215);
                    match(Event);
                    setState(217);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if (_la == Global || _la == Local) {
                        {
                            setState(216);
                            eventGlobalOrLocal();
                        }
                    }

                    setState(219);
                    match(Identifier);
                    setState(220);
                    match(OpenParen);
                    setState(222);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if (((((_la - 90)) & ~0x3f) == 0
                            && ((1L << (_la - 90)) & ((1L << (AtLeastOnce - 90))
                                    | (1L << (AtMostOnce - 90)) | (1L << (OnlyOnce - 90)))) != 0)) {
                        {
                            setState(221);
                            eventSemantics();
                        }
                    }

                    setState(224);
                    match(CloseParen);
                    setState(225);
                    match(SemiColon);
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EventGlobalOrLocalContext extends ParserRuleContext {
        public TerminalNode Global() {
            return getToken(YJSParser.Global, 0);
        }

        public TerminalNode Local() {
            return getToken(YJSParser.Local, 0);
        }

        public EventGlobalOrLocalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_eventGlobalOrLocal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEventGlobalOrLocal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEventGlobalOrLocal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEventGlobalOrLocal(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final EventGlobalOrLocalContext eventGlobalOrLocal() throws RecognitionException {
        EventGlobalOrLocalContext _localctx = new EventGlobalOrLocalContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_eventGlobalOrLocal);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(228);
                _la = _input.LA(1);
                if (!(_la == Global || _la == Local)) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EventSemanticsContext extends ParserRuleContext {
        public TerminalNode AtLeastOnce() {
            return getToken(YJSParser.AtLeastOnce, 0);
        }

        public TerminalNode AtMostOnce() {
            return getToken(YJSParser.AtMostOnce, 0);
        }

        public TerminalNode OnlyOnce() {
            return getToken(YJSParser.OnlyOnce, 0);
        }

        public EventSemanticsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_eventSemantics;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEventSemantics(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEventSemantics(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEventSemantics(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final EventSemanticsContext eventSemantics() throws RecognitionException {
        EventSemanticsContext _localctx = new EventSemanticsContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_eventSemantics);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(230);
                _la = _input.LA(1);
                if (!(((((_la - 90)) & ~0x3f) == 0
                        && ((1L << (_la - 90)) & ((1L << (AtLeastOnce - 90))
                                | (1L << (AtMostOnce - 90)) | (1L << (OnlyOnce - 90)))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SourceElementContext extends ParserRuleContext {
        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public SourceElementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_sourceElement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSourceElement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSourceElement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSourceElement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SourceElementContext sourceElement() throws RecognitionException {
        SourceElementContext _localctx = new SourceElementContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_sourceElement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(232);
                statement();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ImportStmtsContext extends ParserRuleContext {
        public List<ImportStmtContext> importStmt() {
            return getRuleContexts(ImportStmtContext.class);
        }

        public ImportStmtContext importStmt(int i) {
            return getRuleContext(ImportStmtContext.class, i);
        }

        public ImportStmtsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_importStmts;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterImportStmts(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitImportStmts(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitImportStmts(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ImportStmtsContext importStmts() throws RecognitionException {
        ImportStmtsContext _localctx = new ImportStmtsContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_importStmts);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(235);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(234);
                            importStmt();
                        }
                    }
                    setState(237);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == Import);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ImportStmtContext extends ParserRuleContext {
        public TerminalNode Import() {
            return getToken(YJSParser.Import, 0);
        }

        public TerminalNode StringLiteral() {
            return getToken(YJSParser.StringLiteral, 0);
        }

        public TerminalNode SemiColon() {
            return getToken(YJSParser.SemiColon, 0);
        }

        public ImportStmtContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_importStmt;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterImportStmt(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitImportStmt(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitImportStmt(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ImportStmtContext importStmt() throws RecognitionException {
        ImportStmtContext _localctx = new ImportStmtContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_importStmt);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(239);
                match(Import);
                setState(240);
                match(StringLiteral);
                setState(241);
                match(SemiColon);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExportStmtContext extends ParserRuleContext {
        public TerminalNode Export() {
            return getToken(YJSParser.Export, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public VersionNameContext versionName() {
            return getRuleContext(VersionNameContext.class, 0);
        }

        public TerminalNode SemiColon() {
            return getToken(YJSParser.SemiColon, 0);
        }

        public ExportStmtContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_exportStmt;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterExportStmt(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitExportStmt(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitExportStmt(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ExportStmtContext exportStmt() throws RecognitionException {
        ExportStmtContext _localctx = new ExportStmtContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_exportStmt);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(243);
                match(Export);
                setState(244);
                match(Identifier);
                setState(245);
                versionName();
                setState(246);
                match(SemiColon);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VersionNameContext extends ParserRuleContext {
        public List<TerminalNode> DecimalLiteral() {
            return getTokens(YJSParser.DecimalLiteral);
        }

        public TerminalNode DecimalLiteral(int i) {
            return getToken(YJSParser.DecimalLiteral, i);
        }

        public List<TerminalNode> Dot() {
            return getTokens(YJSParser.Dot);
        }

        public TerminalNode Dot(int i) {
            return getToken(YJSParser.Dot, i);
        }

        public VersionNameContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_versionName;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterVersionName(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitVersionName(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitVersionName(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final VersionNameContext versionName() throws RecognitionException {
        VersionNameContext _localctx = new VersionNameContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_versionName);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(248);
                match(DecimalLiteral);
                setState(253);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Dot) {
                    {
                        {
                            setState(249);
                            match(Dot);
                            setState(250);
                            match(DecimalLiteral);
                        }
                    }
                    setState(255);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementContext extends ParserRuleContext {
        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public VariableStatementContext variableStatement() {
            return getRuleContext(VariableStatementContext.class, 0);
        }

        public EmptyStatementContext emptyStatement() {
            return getRuleContext(EmptyStatementContext.class, 0);
        }

        public ExpressionStatementContext expressionStatement() {
            return getRuleContext(ExpressionStatementContext.class, 0);
        }

        public IfStatementContext ifStatement() {
            return getRuleContext(IfStatementContext.class, 0);
        }

        public IterationStatementContext iterationStatement() {
            return getRuleContext(IterationStatementContext.class, 0);
        }

        public ContinueStatementContext continueStatement() {
            return getRuleContext(ContinueStatementContext.class, 0);
        }

        public BreakStatementContext breakStatement() {
            return getRuleContext(BreakStatementContext.class, 0);
        }

        public ReturnStatementContext returnStatement() {
            return getRuleContext(ReturnStatementContext.class, 0);
        }

        public SwitchStatementContext switchStatement() {
            return getRuleContext(SwitchStatementContext.class, 0);
        }

        public StatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final StatementContext statement() throws RecognitionException {
        StatementContext _localctx = new StatementContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_statement);
        try {
            setState(266);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 16, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1); {
                    setState(256);
                    block();
                }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2); {
                    setState(257);
                    variableStatement();
                }
                    break;
                case 3:
                    enterOuterAlt(_localctx, 3); {
                    setState(258);
                    emptyStatement();
                }
                    break;
                case 4:
                    enterOuterAlt(_localctx, 4); {
                    setState(259);
                    expressionStatement();
                }
                    break;
                case 5:
                    enterOuterAlt(_localctx, 5); {
                    setState(260);
                    ifStatement();
                }
                    break;
                case 6:
                    enterOuterAlt(_localctx, 6); {
                    setState(261);
                    iterationStatement();
                }
                    break;
                case 7:
                    enterOuterAlt(_localctx, 7); {
                    setState(262);
                    continueStatement();
                }
                    break;
                case 8:
                    enterOuterAlt(_localctx, 8); {
                    setState(263);
                    breakStatement();
                }
                    break;
                case 9:
                    enterOuterAlt(_localctx, 9); {
                    setState(264);
                    returnStatement();
                }
                    break;
                case 10:
                    enterOuterAlt(_localctx, 10); {
                    setState(265);
                    switchStatement();
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BlockContext extends ParserRuleContext {
        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public StatementListContext statementList() {
            return getRuleContext(StatementListContext.class, 0);
        }

        public BlockContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_block;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBlock(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBlock(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBlock(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final BlockContext block() throws RecognitionException {
        BlockContext _localctx = new BlockContext(_ctx, getState());
        enterRule(_localctx, 34, RULE_block);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(268);
                match(OpenBrace);
                setState(270);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 17, _ctx)) {
                    case 1: {
                        setState(269);
                        statementList();
                    }
                        break;
                }
                setState(272);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class StatementListContext extends ParserRuleContext {
        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public StatementListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statementList;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterStatementList(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitStatementList(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitStatementList(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final StatementListContext statementList() throws RecognitionException {
        StatementListContext _localctx = new StatementListContext(_ctx, getState());
        enterRule(_localctx, 36, RULE_statementList);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(275);
                _errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1: {
                            {
                                setState(274);
                                statement();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(277);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 18, _ctx);
                } while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SharableDeclarationContext extends ParserRuleContext {
        public SharableStatementContext sharableStatement() {
            return getRuleContext(SharableStatementContext.class, 0);
        }

        public SharableDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_sharableDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSharableDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSharableDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSharableDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SharableDeclarationContext sharableDeclaration() throws RecognitionException {
        SharableDeclarationContext _localctx = new SharableDeclarationContext(_ctx, getState());
        enterRule(_localctx, 38, RULE_sharableDeclaration);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(279);
                sharableStatement();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SharableStatementContext extends ParserRuleContext {
        public SharableModifierContext sharableModifier() {
            return getRuleContext(SharableModifierContext.class, 0);
        }

        public VariableDeclarationListContext variableDeclarationList() {
            return getRuleContext(VariableDeclarationListContext.class, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public SharableStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_sharableStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSharableStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSharableStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSharableStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SharableStatementContext sharableStatement() throws RecognitionException {
        SharableStatementContext _localctx = new SharableStatementContext(_ctx, getState());
        enterRule(_localctx, 40, RULE_sharableStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(281);
                sharableModifier();
                setState(282);
                variableDeclarationList();
                setState(283);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SharableModifierContext extends ParserRuleContext {
        public TerminalNode Sharable() {
            return getToken(YJSParser.Sharable, 0);
        }

        public SharableModifierContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_sharableModifier;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSharableModifier(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSharableModifier(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSharableModifier(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SharableModifierContext sharableModifier() throws RecognitionException {
        SharableModifierContext _localctx = new SharableModifierContext(_ctx, getState());
        enterRule(_localctx, 42, RULE_sharableModifier);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(285);
                match(Sharable);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VariableStatementContext extends ParserRuleContext {
        public VarModifierContext varModifier() {
            return getRuleContext(VarModifierContext.class, 0);
        }

        public VariableDeclarationListContext variableDeclarationList() {
            return getRuleContext(VariableDeclarationListContext.class, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public VariableStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_variableStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterVariableStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitVariableStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitVariableStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final VariableStatementContext variableStatement() throws RecognitionException {
        VariableStatementContext _localctx = new VariableStatementContext(_ctx, getState());
        enterRule(_localctx, 44, RULE_variableStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(287);
                varModifier();
                setState(288);
                variableDeclarationList();
                setState(289);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VariableDeclarationListContext extends ParserRuleContext {
        public List<VariableDeclarationContext> variableDeclaration() {
            return getRuleContexts(VariableDeclarationContext.class);
        }

        public VariableDeclarationContext variableDeclaration(int i) {
            return getRuleContext(VariableDeclarationContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public VariableDeclarationListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_variableDeclarationList;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterVariableDeclarationList(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitVariableDeclarationList(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitVariableDeclarationList(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final VariableDeclarationListContext variableDeclarationList()
            throws RecognitionException {
        VariableDeclarationListContext _localctx =
                new VariableDeclarationListContext(_ctx, getState());
        enterRule(_localctx, 46, RULE_variableDeclarationList);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(291);
                variableDeclaration();
                setState(296);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 19, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(292);
                                match(Comma);
                                setState(293);
                                variableDeclaration();
                            }
                        }
                    }
                    setState(298);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 19, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VariableDeclarationContext extends ParserRuleContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode Assign() {
            return getToken(YJSParser.Assign, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_variableDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterVariableDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitVariableDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitVariableDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
        VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
        enterRule(_localctx, 48, RULE_variableDeclaration);
        try {
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(299);
                    match(Identifier);
                }
                setState(302);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 20, _ctx)) {
                    case 1: {
                        setState(300);
                        match(Assign);
                        setState(301);
                        singleExpression(0);
                    }
                        break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EmptyStatementContext extends ParserRuleContext {
        public TerminalNode SemiColon() {
            return getToken(YJSParser.SemiColon, 0);
        }

        public EmptyStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_emptyStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEmptyStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEmptyStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEmptyStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final EmptyStatementContext emptyStatement() throws RecognitionException {
        EmptyStatementContext _localctx = new EmptyStatementContext(_ctx, getState());
        enterRule(_localctx, 50, RULE_emptyStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(304);
                match(SemiColon);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExpressionStatementContext extends ParserRuleContext {
        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public ExpressionStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expressionStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterExpressionStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitExpressionStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitExpressionStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ExpressionStatementContext expressionStatement() throws RecognitionException {
        ExpressionStatementContext _localctx = new ExpressionStatementContext(_ctx, getState());
        enterRule(_localctx, 52, RULE_expressionStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(306);
                if (!(notOpenBraceAndNotFunction()))
                    throw new FailedPredicateException(this, "notOpenBraceAndNotFunction()");
                setState(307);
                expressionSequence();
                setState(308);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class IfStatementContext extends ParserRuleContext {
        public TerminalNode If() {
            return getToken(YJSParser.If, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public TerminalNode Else() {
            return getToken(YJSParser.Else, 0);
        }

        public IfStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_ifStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterIfStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitIfStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitIfStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final IfStatementContext ifStatement() throws RecognitionException {
        IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
        enterRule(_localctx, 54, RULE_ifStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(310);
                match(If);
                setState(311);
                match(OpenParen);
                setState(312);
                expressionSequence();
                setState(313);
                match(CloseParen);
                setState(314);
                statement();
                setState(317);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 21, _ctx)) {
                    case 1: {
                        setState(315);
                        match(Else);
                        setState(316);
                        statement();
                    }
                        break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class IterationStatementContext extends ParserRuleContext {
        public IterationStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_iterationStatement;
        }

        public IterationStatementContext() {}

        public void copyFrom(IterationStatementContext ctx) {
            super.copyFrom(ctx);
        }
    }
    public static class DoStatementContext extends IterationStatementContext {
        public TerminalNode Do() {
            return getToken(YJSParser.Do, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public TerminalNode While() {
            return getToken(YJSParser.While, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public DoStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterDoStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitDoStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitDoStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ForVarStatementContext extends IterationStatementContext {
        public TerminalNode For() {
            return getToken(YJSParser.For, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public VarModifierContext varModifier() {
            return getRuleContext(VarModifierContext.class, 0);
        }

        public VariableDeclarationListContext variableDeclarationList() {
            return getRuleContext(VariableDeclarationListContext.class, 0);
        }

        public List<TerminalNode> SemiColon() {
            return getTokens(YJSParser.SemiColon);
        }

        public TerminalNode SemiColon(int i) {
            return getToken(YJSParser.SemiColon, i);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public List<ExpressionSequenceContext> expressionSequence() {
            return getRuleContexts(ExpressionSequenceContext.class);
        }

        public ExpressionSequenceContext expressionSequence(int i) {
            return getRuleContext(ExpressionSequenceContext.class, i);
        }

        public ForVarStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterForVarStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitForVarStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitForVarStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ForVarInStatementContext extends IterationStatementContext {
        public TerminalNode For() {
            return getToken(YJSParser.For, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public VarModifierContext varModifier() {
            return getRuleContext(VarModifierContext.class, 0);
        }

        public VariableDeclarationContext variableDeclaration() {
            return getRuleContext(VariableDeclarationContext.class, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public TerminalNode In() {
            return getToken(YJSParser.In, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public ForVarInStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterForVarInStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitForVarInStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitForVarInStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class WhileStatementContext extends IterationStatementContext {
        public TerminalNode While() {
            return getToken(YJSParser.While, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public WhileStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterWhileStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitWhileStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitWhileStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ForStatementContext extends IterationStatementContext {
        public TerminalNode For() {
            return getToken(YJSParser.For, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public List<TerminalNode> SemiColon() {
            return getTokens(YJSParser.SemiColon);
        }

        public TerminalNode SemiColon(int i) {
            return getToken(YJSParser.SemiColon, i);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public List<ExpressionSequenceContext> expressionSequence() {
            return getRuleContexts(ExpressionSequenceContext.class);
        }

        public ExpressionSequenceContext expressionSequence(int i) {
            return getRuleContext(ExpressionSequenceContext.class, i);
        }

        public ForStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterForStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitForStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitForStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ForInStatementContext extends IterationStatementContext {
        public TerminalNode For() {
            return getToken(YJSParser.For, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public TerminalNode In() {
            return getToken(YJSParser.In, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public ForInStatementContext(IterationStatementContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterForInStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitForInStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitForInStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final IterationStatementContext iterationStatement() throws RecognitionException {
        IterationStatementContext _localctx = new IterationStatementContext(_ctx, getState());
        enterRule(_localctx, 56, RULE_iterationStatement);
        int _la;
        try {
            setState(388);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 29, _ctx)) {
                case 1:
                    _localctx = new DoStatementContext(_localctx);
                    enterOuterAlt(_localctx, 1); {
                    setState(319);
                    match(Do);
                    setState(320);
                    statement();
                    setState(321);
                    match(While);
                    setState(322);
                    match(OpenParen);
                    setState(323);
                    expressionSequence();
                    setState(324);
                    match(CloseParen);
                    setState(325);
                    eos();
                }
                    break;
                case 2:
                    _localctx = new WhileStatementContext(_localctx);
                    enterOuterAlt(_localctx, 2); {
                    setState(327);
                    match(While);
                    setState(328);
                    match(OpenParen);
                    setState(329);
                    expressionSequence();
                    setState(330);
                    match(CloseParen);
                    setState(331);
                    statement();
                }
                    break;
                case 3:
                    _localctx = new ForStatementContext(_localctx);
                    enterOuterAlt(_localctx, 3); {
                    setState(333);
                    match(For);
                    setState(334);
                    match(OpenParen);
                    setState(336);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                            | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                            | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus) | (1L << Minus)
                            | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                            | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                            | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                            | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                            || ((((_la - 64)) & ~0x3f) == 0
                                    && ((1L << (_la - 64)) & ((1L << (Typeof - 64))
                                            | (1L << (New - 64)) | (1L << (This - 64))
                                            | (1L << (Super - 64)) | (1L << (Identifier - 64))
                                            | (1L << (StringLiteral - 64))
                                            | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                        {
                            setState(335);
                            expressionSequence();
                        }
                    }

                    setState(338);
                    match(SemiColon);
                    setState(340);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                            | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                            | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus) | (1L << Minus)
                            | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                            | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                            | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                            | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                            || ((((_la - 64)) & ~0x3f) == 0
                                    && ((1L << (_la - 64)) & ((1L << (Typeof - 64))
                                            | (1L << (New - 64)) | (1L << (This - 64))
                                            | (1L << (Super - 64)) | (1L << (Identifier - 64))
                                            | (1L << (StringLiteral - 64))
                                            | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                        {
                            setState(339);
                            expressionSequence();
                        }
                    }

                    setState(342);
                    match(SemiColon);
                    setState(344);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                            | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                            | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus) | (1L << Minus)
                            | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                            | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                            | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                            | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                            || ((((_la - 64)) & ~0x3f) == 0
                                    && ((1L << (_la - 64)) & ((1L << (Typeof - 64))
                                            | (1L << (New - 64)) | (1L << (This - 64))
                                            | (1L << (Super - 64)) | (1L << (Identifier - 64))
                                            | (1L << (StringLiteral - 64))
                                            | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                        {
                            setState(343);
                            expressionSequence();
                        }
                    }

                    setState(346);
                    match(CloseParen);
                    setState(347);
                    statement();
                }
                    break;
                case 4:
                    _localctx = new ForVarStatementContext(_localctx);
                    enterOuterAlt(_localctx, 4); {
                    setState(348);
                    match(For);
                    setState(349);
                    match(OpenParen);
                    setState(350);
                    varModifier();
                    setState(351);
                    variableDeclarationList();
                    setState(352);
                    match(SemiColon);
                    setState(354);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                            | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                            | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus) | (1L << Minus)
                            | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                            | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                            | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                            | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                            || ((((_la - 64)) & ~0x3f) == 0
                                    && ((1L << (_la - 64)) & ((1L << (Typeof - 64))
                                            | (1L << (New - 64)) | (1L << (This - 64))
                                            | (1L << (Super - 64)) | (1L << (Identifier - 64))
                                            | (1L << (StringLiteral - 64))
                                            | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                        {
                            setState(353);
                            expressionSequence();
                        }
                    }

                    setState(356);
                    match(SemiColon);
                    setState(358);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                            | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                            | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus) | (1L << Minus)
                            | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                            | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                            | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                            | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                            || ((((_la - 64)) & ~0x3f) == 0
                                    && ((1L << (_la - 64)) & ((1L << (Typeof - 64))
                                            | (1L << (New - 64)) | (1L << (This - 64))
                                            | (1L << (Super - 64)) | (1L << (Identifier - 64))
                                            | (1L << (StringLiteral - 64))
                                            | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                        {
                            setState(357);
                            expressionSequence();
                        }
                    }

                    setState(360);
                    match(CloseParen);
                    setState(361);
                    statement();
                }
                    break;
                case 5:
                    _localctx = new ForInStatementContext(_localctx);
                    enterOuterAlt(_localctx, 5); {
                    setState(363);
                    match(For);
                    setState(364);
                    match(OpenParen);
                    setState(365);
                    singleExpression(0);
                    setState(369);
                    _errHandler.sync(this);
                    switch (_input.LA(1)) {
                        case In: {
                            setState(366);
                            match(In);
                        }
                            break;
                        case Identifier: {
                            setState(367);
                            match(Identifier);
                            setState(368);
                            if (!(p("of")))
                                throw new FailedPredicateException(this, "p(\"of\")");
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(371);
                    expressionSequence();
                    setState(372);
                    match(CloseParen);
                    setState(373);
                    statement();
                }
                    break;
                case 6:
                    _localctx = new ForVarInStatementContext(_localctx);
                    enterOuterAlt(_localctx, 6); {
                    setState(375);
                    match(For);
                    setState(376);
                    match(OpenParen);
                    setState(377);
                    varModifier();
                    setState(378);
                    variableDeclaration();
                    setState(382);
                    _errHandler.sync(this);
                    switch (_input.LA(1)) {
                        case In: {
                            setState(379);
                            match(In);
                        }
                            break;
                        case Identifier: {
                            setState(380);
                            match(Identifier);
                            setState(381);
                            if (!(p("of")))
                                throw new FailedPredicateException(this, "p(\"of\")");
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(384);
                    expressionSequence();
                    setState(385);
                    match(CloseParen);
                    setState(386);
                    statement();
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class VarModifierContext extends ParserRuleContext {
        public TerminalNode Var() {
            return getToken(YJSParser.Var, 0);
        }

        public VarModifierContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_varModifier;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterVarModifier(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitVarModifier(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitVarModifier(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final VarModifierContext varModifier() throws RecognitionException {
        VarModifierContext _localctx = new VarModifierContext(_ctx, getState());
        enterRule(_localctx, 58, RULE_varModifier);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(390);
                match(Var);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ContinueStatementContext extends ParserRuleContext {
        public TerminalNode Continue() {
            return getToken(YJSParser.Continue, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public ContinueStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_continueStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterContinueStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitContinueStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitContinueStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ContinueStatementContext continueStatement() throws RecognitionException {
        ContinueStatementContext _localctx = new ContinueStatementContext(_ctx, getState());
        enterRule(_localctx, 60, RULE_continueStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(392);
                match(Continue);
                setState(395);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 30, _ctx)) {
                    case 1: {
                        setState(393);
                        if (!(notLineTerminator()))
                            throw new FailedPredicateException(this, "notLineTerminator()");
                        setState(394);
                        match(Identifier);
                    }
                        break;
                }
                setState(397);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BreakStatementContext extends ParserRuleContext {
        public TerminalNode Break() {
            return getToken(YJSParser.Break, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public BreakStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_breakStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBreakStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBreakStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBreakStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final BreakStatementContext breakStatement() throws RecognitionException {
        BreakStatementContext _localctx = new BreakStatementContext(_ctx, getState());
        enterRule(_localctx, 62, RULE_breakStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(399);
                match(Break);
                setState(402);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 31, _ctx)) {
                    case 1: {
                        setState(400);
                        if (!(notLineTerminator()))
                            throw new FailedPredicateException(this, "notLineTerminator()");
                        setState(401);
                        match(Identifier);
                    }
                        break;
                }
                setState(404);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ReturnStatementContext extends ParserRuleContext {
        public TerminalNode Return() {
            return getToken(YJSParser.Return, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_returnStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterReturnStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitReturnStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitReturnStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ReturnStatementContext returnStatement() throws RecognitionException {
        ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
        enterRule(_localctx, 64, RULE_returnStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(406);
                match(Return);
                setState(409);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 32, _ctx)) {
                    case 1: {
                        setState(407);
                        if (!(notLineTerminator()))
                            throw new FailedPredicateException(this, "notLineTerminator()");
                        setState(408);
                        expressionSequence();
                    }
                        break;
                }
                setState(411);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class WithStatementContext extends ParserRuleContext {
        public TerminalNode With() {
            return getToken(YJSParser.With, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public WithStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_withStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterWithStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitWithStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitWithStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final WithStatementContext withStatement() throws RecognitionException {
        WithStatementContext _localctx = new WithStatementContext(_ctx, getState());
        enterRule(_localctx, 66, RULE_withStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(413);
                match(With);
                setState(414);
                match(OpenParen);
                setState(415);
                expressionSequence();
                setState(416);
                match(CloseParen);
                setState(417);
                statement();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SwitchStatementContext extends ParserRuleContext {
        public TerminalNode Switch() {
            return getToken(YJSParser.Switch, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public CaseBlockContext caseBlock() {
            return getRuleContext(CaseBlockContext.class, 0);
        }

        public SwitchStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_switchStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSwitchStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSwitchStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSwitchStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SwitchStatementContext switchStatement() throws RecognitionException {
        SwitchStatementContext _localctx = new SwitchStatementContext(_ctx, getState());
        enterRule(_localctx, 68, RULE_switchStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(419);
                match(Switch);
                setState(420);
                match(OpenParen);
                setState(421);
                expressionSequence();
                setState(422);
                match(CloseParen);
                setState(423);
                caseBlock();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CaseBlockContext extends ParserRuleContext {
        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public List<CaseClausesContext> caseClauses() {
            return getRuleContexts(CaseClausesContext.class);
        }

        public CaseClausesContext caseClauses(int i) {
            return getRuleContext(CaseClausesContext.class, i);
        }

        public DefaultClauseContext defaultClause() {
            return getRuleContext(DefaultClauseContext.class, 0);
        }

        public CaseBlockContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_caseBlock;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterCaseBlock(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitCaseBlock(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitCaseBlock(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final CaseBlockContext caseBlock() throws RecognitionException {
        CaseBlockContext _localctx = new CaseBlockContext(_ctx, getState());
        enterRule(_localctx, 70, RULE_caseBlock);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(425);
                match(OpenBrace);
                setState(427);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Case) {
                    {
                        setState(426);
                        caseClauses();
                    }
                }

                setState(433);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Default) {
                    {
                        setState(429);
                        defaultClause();
                        setState(431);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == Case) {
                            {
                                setState(430);
                                caseClauses();
                            }
                        }

                    }
                }

                setState(435);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CaseClausesContext extends ParserRuleContext {
        public List<CaseClauseContext> caseClause() {
            return getRuleContexts(CaseClauseContext.class);
        }

        public CaseClauseContext caseClause(int i) {
            return getRuleContext(CaseClauseContext.class, i);
        }

        public CaseClausesContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_caseClauses;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterCaseClauses(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitCaseClauses(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitCaseClauses(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final CaseClausesContext caseClauses() throws RecognitionException {
        CaseClausesContext _localctx = new CaseClausesContext(_ctx, getState());
        enterRule(_localctx, 72, RULE_caseClauses);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(438);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(437);
                            caseClause();
                        }
                    }
                    setState(440);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == Case);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CaseClauseContext extends ParserRuleContext {
        public TerminalNode Case() {
            return getToken(YJSParser.Case, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode Colon() {
            return getToken(YJSParser.Colon, 0);
        }

        public StatementListContext statementList() {
            return getRuleContext(StatementListContext.class, 0);
        }

        public CaseClauseContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_caseClause;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterCaseClause(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitCaseClause(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitCaseClause(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final CaseClauseContext caseClause() throws RecognitionException {
        CaseClauseContext _localctx = new CaseClauseContext(_ctx, getState());
        enterRule(_localctx, 74, RULE_caseClause);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(442);
                match(Case);
                setState(443);
                expressionSequence();
                setState(444);
                match(Colon);
                setState(446);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 37, _ctx)) {
                    case 1: {
                        setState(445);
                        statementList();
                    }
                        break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class DefaultClauseContext extends ParserRuleContext {
        public TerminalNode Default() {
            return getToken(YJSParser.Default, 0);
        }

        public TerminalNode Colon() {
            return getToken(YJSParser.Colon, 0);
        }

        public StatementListContext statementList() {
            return getRuleContext(StatementListContext.class, 0);
        }

        public DefaultClauseContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_defaultClause;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterDefaultClause(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitDefaultClause(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitDefaultClause(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final DefaultClauseContext defaultClause() throws RecognitionException {
        DefaultClauseContext _localctx = new DefaultClauseContext(_ctx, getState());
        enterRule(_localctx, 76, RULE_defaultClause);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(448);
                match(Default);
                setState(449);
                match(Colon);
                setState(451);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 38, _ctx)) {
                    case 1: {
                        setState(450);
                        statementList();
                    }
                        break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ThrowStatementContext extends ParserRuleContext {
        public TerminalNode Throw() {
            return getToken(YJSParser.Throw, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public ThrowStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_throwStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterThrowStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitThrowStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitThrowStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ThrowStatementContext throwStatement() throws RecognitionException {
        ThrowStatementContext _localctx = new ThrowStatementContext(_ctx, getState());
        enterRule(_localctx, 78, RULE_throwStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(453);
                match(Throw);
                setState(454);
                if (!(notLineTerminator()))
                    throw new FailedPredicateException(this, "notLineTerminator()");
                setState(455);
                expressionSequence();
                setState(456);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TryStatementContext extends ParserRuleContext {
        public TerminalNode Try() {
            return getToken(YJSParser.Try, 0);
        }

        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public CatchProductionContext catchProduction() {
            return getRuleContext(CatchProductionContext.class, 0);
        }

        public FinallyProductionContext finallyProduction() {
            return getRuleContext(FinallyProductionContext.class, 0);
        }

        public TryStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_tryStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterTryStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitTryStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitTryStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final TryStatementContext tryStatement() throws RecognitionException {
        TryStatementContext _localctx = new TryStatementContext(_ctx, getState());
        enterRule(_localctx, 80, RULE_tryStatement);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(458);
                match(Try);
                setState(459);
                block();
                setState(465);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case Catch: {
                        setState(460);
                        catchProduction();
                        setState(462);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == Finally) {
                            {
                                setState(461);
                                finallyProduction();
                            }
                        }

                    }
                        break;
                    case Finally: {
                        setState(464);
                        finallyProduction();
                    }
                        break;
                    default:
                        throw new NoViableAltException(this);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class CatchProductionContext extends ParserRuleContext {
        public TerminalNode Catch() {
            return getToken(YJSParser.Catch, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public CatchProductionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_catchProduction;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterCatchProduction(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitCatchProduction(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitCatchProduction(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final CatchProductionContext catchProduction() throws RecognitionException {
        CatchProductionContext _localctx = new CatchProductionContext(_ctx, getState());
        enterRule(_localctx, 82, RULE_catchProduction);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(467);
                match(Catch);
                setState(468);
                match(OpenParen);
                setState(469);
                match(Identifier);
                setState(470);
                match(CloseParen);
                setState(471);
                block();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FinallyProductionContext extends ParserRuleContext {
        public TerminalNode Finally() {
            return getToken(YJSParser.Finally, 0);
        }

        public BlockContext block() {
            return getRuleContext(BlockContext.class, 0);
        }

        public FinallyProductionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_finallyProduction;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterFinallyProduction(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitFinallyProduction(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitFinallyProduction(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final FinallyProductionContext finallyProduction() throws RecognitionException {
        FinallyProductionContext _localctx = new FinallyProductionContext(_ctx, getState());
        enterRule(_localctx, 84, RULE_finallyProduction);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(473);
                match(Finally);
                setState(474);
                block();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class DebuggerStatementContext extends ParserRuleContext {
        public TerminalNode Debugger() {
            return getToken(YJSParser.Debugger, 0);
        }

        public EosContext eos() {
            return getRuleContext(EosContext.class, 0);
        }

        public DebuggerStatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_debuggerStatement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterDebuggerStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitDebuggerStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitDebuggerStatement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final DebuggerStatementContext debuggerStatement() throws RecognitionException {
        DebuggerStatementContext _localctx = new DebuggerStatementContext(_ctx, getState());
        enterRule(_localctx, 86, RULE_debuggerStatement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(476);
                match(Debugger);
                setState(477);
                eos();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FunctionDeclarationContext extends ParserRuleContext {
        public TerminalNode Function() {
            return getToken(YJSParser.Function, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public FunctionBodyContext functionBody() {
            return getRuleContext(FunctionBodyContext.class, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public AnnotationsContext annotations() {
            return getRuleContext(AnnotationsContext.class, 0);
        }

        public TerminalNode Export() {
            return getToken(YJSParser.Export, 0);
        }

        public FormalParameterListContext formalParameterList() {
            return getRuleContext(FormalParameterListContext.class, 0);
        }

        public TerminalNode View() {
            return getToken(YJSParser.View, 0);
        }

        public FunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_functionDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterFunctionDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitFunctionDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitFunctionDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final FunctionDeclarationContext functionDeclaration() throws RecognitionException {
        FunctionDeclarationContext _localctx = new FunctionDeclarationContext(_ctx, getState());
        enterRule(_localctx, 88, RULE_functionDeclaration);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(480);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == AtToken) {
                    {
                        setState(479);
                        annotations();
                    }
                }

                setState(483);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Export) {
                    {
                        setState(482);
                        match(Export);
                    }
                }

                setState(485);
                match(Function);
                setState(486);
                match(Identifier);
                setState(487);
                match(OpenParen);
                setState(489);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << OpenBracket) | (1L << OpenBrace) | (1L << Ellipsis))) != 0)
                        || _la == Identifier) {
                    {
                        setState(488);
                        formalParameterList();
                    }
                }

                setState(491);
                match(CloseParen);
                setState(493);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == View) {
                    {
                        setState(492);
                        match(View);
                    }
                }

                setState(495);
                match(OpenBrace);
                setState(496);
                functionBody();
                setState(497);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ClassDeclarationContext extends ParserRuleContext {
        public TerminalNode Class() {
            return getToken(YJSParser.Class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public ClassTailContext classTail() {
            return getRuleContext(ClassTailContext.class, 0);
        }

        public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_classDeclaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterClassDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitClassDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitClassDeclaration(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ClassDeclarationContext classDeclaration() throws RecognitionException {
        ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
        enterRule(_localctx, 90, RULE_classDeclaration);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(499);
                match(Class);
                setState(500);
                match(Identifier);
                setState(501);
                classTail();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ClassTailContext extends ParserRuleContext {
        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public TerminalNode Extends() {
            return getToken(YJSParser.Extends, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public List<ClassElementContext> classElement() {
            return getRuleContexts(ClassElementContext.class);
        }

        public ClassElementContext classElement(int i) {
            return getRuleContext(ClassElementContext.class, i);
        }

        public ClassTailContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_classTail;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterClassTail(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitClassTail(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitClassTail(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ClassTailContext classTail() throws RecognitionException {
        ClassTailContext _localctx = new ClassTailContext(_ctx, getState());
        enterRule(_localctx, 92, RULE_classTail);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(505);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Extends) {
                    {
                        setState(503);
                        match(Extends);
                        setState(504);
                        singleExpression(0);
                    }
                }

                setState(507);
                match(OpenBrace);
                setState(511);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la - 54)) & ~0x3f) == 0 && ((1L << (_la - 54))
                        & ((1L << (NullLiteral - 54)) | (1L << (BooleanLiteral - 54))
                                | (1L << (DecimalLiteral - 54)) | (1L << (HexIntegerLiteral - 54))
                                | (1L << (OctalIntegerLiteral - 54))
                                | (1L << (OctalIntegerLiteral2 - 54))
                                | (1L << (BinaryIntegerLiteral - 54)) | (1L << (Break - 54))
                                | (1L << (Do - 54)) | (1L << (Instanceof - 54))
                                | (1L << (Typeof - 54)) | (1L << (Case - 54)) | (1L << (Else - 54))
                                | (1L << (New - 54)) | (1L << (Var - 54)) | (1L << (Catch - 54))
                                | (1L << (Finally - 54)) | (1L << (Return - 54))
                                | (1L << (Void - 54)) | (1L << (Continue - 54)) | (1L << (For - 54))
                                | (1L << (Switch - 54)) | (1L << (While - 54))
                                | (1L << (Debugger - 54)) | (1L << (Function - 54))
                                | (1L << (This - 54)) | (1L << (With - 54)) | (1L << (Default - 54))
                                | (1L << (If - 54)) | (1L << (Throw - 54)) | (1L << (Delete - 54))
                                | (1L << (In - 54)) | (1L << (Try - 54)) | (1L << (Sharable - 54))
                                | (1L << (Class - 54)) | (1L << (Enum - 54))
                                | (1L << (Extends - 54)) | (1L << (Super - 54))
                                | (1L << (Const - 54)) | (1L << (Export - 54))
                                | (1L << (Import - 54)) | (1L << (Implements - 54))
                                | (1L << (Let - 54)) | (1L << (Private - 54))
                                | (1L << (Public - 54)) | (1L << (Interface - 54))
                                | (1L << (Package - 54)) | (1L << (Protected - 54))
                                | (1L << (Static - 54)) | (1L << (Yield - 54))
                                | (1L << (Identifier - 54))
                                | (1L << (StringLiteral - 54)))) != 0)) {
                    {
                        {
                            setState(508);
                            classElement();
                        }
                    }
                    setState(513);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(514);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ClassElementContext extends ParserRuleContext {
        public MethodDefinitionContext methodDefinition() {
            return getRuleContext(MethodDefinitionContext.class, 0);
        }

        public TerminalNode Static() {
            return getToken(YJSParser.Static, 0);
        }

        public ClassElementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_classElement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterClassElement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitClassElement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitClassElement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ClassElementContext classElement() throws RecognitionException {
        ClassElementContext _localctx = new ClassElementContext(_ctx, getState());
        enterRule(_localctx, 94, RULE_classElement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(517);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 47, _ctx)) {
                    case 1: {
                        setState(516);
                        match(Static);
                    }
                        break;
                }
                setState(519);
                methodDefinition();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class MethodDefinitionContext extends ParserRuleContext {
        public PropertyNameContext propertyName() {
            return getRuleContext(PropertyNameContext.class, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public FunctionBodyContext functionBody() {
            return getRuleContext(FunctionBodyContext.class, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public FormalParameterListContext formalParameterList() {
            return getRuleContext(FormalParameterListContext.class, 0);
        }

        public MethodDefinitionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_methodDefinition;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterMethodDefinition(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitMethodDefinition(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitMethodDefinition(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final MethodDefinitionContext methodDefinition() throws RecognitionException {
        MethodDefinitionContext _localctx = new MethodDefinitionContext(_ctx, getState());
        enterRule(_localctx, 96, RULE_methodDefinition);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(521);
                propertyName();
                setState(522);
                match(OpenParen);
                setState(524);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                        & ((1L << OpenBracket) | (1L << OpenBrace) | (1L << Ellipsis))) != 0)
                        || _la == Identifier) {
                    {
                        setState(523);
                        formalParameterList();
                    }
                }

                setState(526);
                match(CloseParen);
                setState(527);
                match(OpenBrace);
                setState(528);
                functionBody();
                setState(529);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FormalParameterListContext extends ParserRuleContext {
        public List<FormalParameterArgContext> formalParameterArg() {
            return getRuleContexts(FormalParameterArgContext.class);
        }

        public FormalParameterArgContext formalParameterArg(int i) {
            return getRuleContext(FormalParameterArgContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public LastFormalParameterArgContext lastFormalParameterArg() {
            return getRuleContext(LastFormalParameterArgContext.class, 0);
        }

        public ArrayLiteralContext arrayLiteral() {
            return getRuleContext(ArrayLiteralContext.class, 0);
        }

        public ObjectLiteralContext objectLiteral() {
            return getRuleContext(ObjectLiteralContext.class, 0);
        }

        public FormalParameterListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_formalParameterList;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterFormalParameterList(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitFormalParameterList(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitFormalParameterList(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final FormalParameterListContext formalParameterList() throws RecognitionException {
        FormalParameterListContext _localctx = new FormalParameterListContext(_ctx, getState());
        enterRule(_localctx, 98, RULE_formalParameterList);
        int _la;
        try {
            int _alt;
            setState(546);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case Identifier:
                    enterOuterAlt(_localctx, 1); {
                    setState(531);
                    formalParameterArg();
                    setState(536);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 49, _ctx);
                    while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                        if (_alt == 1) {
                            {
                                {
                                    setState(532);
                                    match(Comma);
                                    setState(533);
                                    formalParameterArg();
                                }
                            }
                        }
                        setState(538);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 49, _ctx);
                    }
                    setState(541);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if (_la == Comma) {
                        {
                            setState(539);
                            match(Comma);
                            setState(540);
                            lastFormalParameterArg();
                        }
                    }

                }
                    break;
                case Ellipsis:
                    enterOuterAlt(_localctx, 2); {
                    setState(543);
                    lastFormalParameterArg();
                }
                    break;
                case OpenBracket:
                    enterOuterAlt(_localctx, 3); {
                    setState(544);
                    arrayLiteral();
                }
                    break;
                case OpenBrace:
                    enterOuterAlt(_localctx, 4); {
                    setState(545);
                    objectLiteral();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FormalParameterArgContext extends ParserRuleContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode Assign() {
            return getToken(YJSParser.Assign, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public FormalParameterArgContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_formalParameterArg;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterFormalParameterArg(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitFormalParameterArg(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitFormalParameterArg(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final FormalParameterArgContext formalParameterArg() throws RecognitionException {
        FormalParameterArgContext _localctx = new FormalParameterArgContext(_ctx, getState());
        enterRule(_localctx, 100, RULE_formalParameterArg);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(548);
                match(Identifier);
                setState(551);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Assign) {
                    {
                        setState(549);
                        match(Assign);
                        setState(550);
                        singleExpression(0);
                    }
                }

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LastFormalParameterArgContext extends ParserRuleContext {
        public TerminalNode Ellipsis() {
            return getToken(YJSParser.Ellipsis, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public LastFormalParameterArgContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lastFormalParameterArg;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLastFormalParameterArg(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLastFormalParameterArg(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLastFormalParameterArg(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final LastFormalParameterArgContext lastFormalParameterArg()
            throws RecognitionException {
        LastFormalParameterArgContext _localctx =
                new LastFormalParameterArgContext(_ctx, getState());
        enterRule(_localctx, 102, RULE_lastFormalParameterArg);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(553);
                match(Ellipsis);
                setState(554);
                match(Identifier);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FunctionBodyContext extends ParserRuleContext {
        public SourceElementsContext sourceElements() {
            return getRuleContext(SourceElementsContext.class, 0);
        }

        public FunctionBodyContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_functionBody;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterFunctionBody(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitFunctionBody(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitFunctionBody(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final FunctionBodyContext functionBody() throws RecognitionException {
        FunctionBodyContext _localctx = new FunctionBodyContext(_ctx, getState());
        enterRule(_localctx, 104, RULE_functionBody);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(557);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 53, _ctx)) {
                    case 1: {
                        setState(556);
                        sourceElements();
                    }
                        break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SourceElementsContext extends ParserRuleContext {
        public List<SourceElementContext> sourceElement() {
            return getRuleContexts(SourceElementContext.class);
        }

        public SourceElementContext sourceElement(int i) {
            return getRuleContext(SourceElementContext.class, i);
        }

        public SourceElementsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_sourceElements;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSourceElements(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSourceElements(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSourceElements(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SourceElementsContext sourceElements() throws RecognitionException {
        SourceElementsContext _localctx = new SourceElementsContext(_ctx, getState());
        enterRule(_localctx, 106, RULE_sourceElements);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(560);
                _errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1: {
                            {
                                setState(559);
                                sourceElement();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(562);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 54, _ctx);
                } while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArrayLiteralContext extends ParserRuleContext {
        public TerminalNode OpenBracket() {
            return getToken(YJSParser.OpenBracket, 0);
        }

        public TerminalNode CloseBracket() {
            return getToken(YJSParser.CloseBracket, 0);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public ElementListContext elementList() {
            return getRuleContext(ElementListContext.class, 0);
        }

        public ArrayLiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arrayLiteral;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArrayLiteral(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArrayLiteral(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArrayLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArrayLiteralContext arrayLiteral() throws RecognitionException {
        ArrayLiteralContext _localctx = new ArrayLiteralContext(_ctx, getState());
        enterRule(_localctx, 108, RULE_arrayLiteral);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(564);
                match(OpenBracket);
                setState(568);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 55, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(565);
                                match(Comma);
                            }
                        }
                    }
                    setState(570);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 55, _ctx);
                }
                setState(572);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RegularExpressionLiteral)
                        | (1L << OpenBracket) | (1L << OpenParen) | (1L << OpenBrace)
                        | (1L << Ellipsis) | (1L << PlusPlus) | (1L << MinusMinus) | (1L << Plus)
                        | (1L << Minus) | (1L << BitNot) | (1L << Not) | (1L << NullLiteral)
                        | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                        | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                        | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0)
                        || ((((_la - 64)) & ~0x3f) == 0
                                && ((1L << (_la - 64)) & ((1L << (Typeof - 64)) | (1L << (New - 64))
                                        | (1L << (This - 64)) | (1L << (Super - 64))
                                        | (1L << (Identifier - 64)) | (1L << (StringLiteral - 64))
                                        | (1L << (TemplateStringLiteral - 64)))) != 0)) {
                    {
                        setState(571);
                        elementList();
                    }
                }

                setState(577);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(574);
                            match(Comma);
                        }
                    }
                    setState(579);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(580);
                match(CloseBracket);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ElementListContext extends ParserRuleContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public LastElementContext lastElement() {
            return getRuleContext(LastElementContext.class, 0);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public ElementListContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_elementList;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterElementList(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitElementList(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitElementList(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ElementListContext elementList() throws RecognitionException {
        ElementListContext _localctx = new ElementListContext(_ctx, getState());
        enterRule(_localctx, 110, RULE_elementList);
        int _la;
        try {
            int _alt;
            setState(603);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case RegularExpressionLiteral:
                case OpenBracket:
                case OpenParen:
                case OpenBrace:
                case PlusPlus:
                case MinusMinus:
                case Plus:
                case Minus:
                case BitNot:
                case Not:
                case NullLiteral:
                case BooleanLiteral:
                case DecimalLiteral:
                case HexIntegerLiteral:
                case OctalIntegerLiteral:
                case OctalIntegerLiteral2:
                case BinaryIntegerLiteral:
                case Typeof:
                case New:
                case This:
                case Super:
                case Identifier:
                case StringLiteral:
                case TemplateStringLiteral:
                    enterOuterAlt(_localctx, 1); {
                    setState(582);
                    singleExpression(0);
                    setState(591);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 59, _ctx);
                    while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                        if (_alt == 1) {
                            {
                                {
                                    setState(584);
                                    _errHandler.sync(this);
                                    _la = _input.LA(1);
                                    do {
                                        {
                                            {
                                                setState(583);
                                                match(Comma);
                                            }
                                        }
                                        setState(586);
                                        _errHandler.sync(this);
                                        _la = _input.LA(1);
                                    } while (_la == Comma);
                                    setState(588);
                                    singleExpression(0);
                                }
                            }
                        }
                        setState(593);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 59, _ctx);
                    }
                    setState(600);
                    _errHandler.sync(this);
                    switch (getInterpreter().adaptivePredict(_input, 61, _ctx)) {
                        case 1: {
                            setState(595);
                            _errHandler.sync(this);
                            _la = _input.LA(1);
                            do {
                                {
                                    {
                                        setState(594);
                                        match(Comma);
                                    }
                                }
                                setState(597);
                                _errHandler.sync(this);
                                _la = _input.LA(1);
                            } while (_la == Comma);
                            setState(599);
                            lastElement();
                        }
                            break;
                    }
                }
                    break;
                case Ellipsis:
                    enterOuterAlt(_localctx, 2); {
                    setState(602);
                    lastElement();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LastElementContext extends ParserRuleContext {
        public TerminalNode Ellipsis() {
            return getToken(YJSParser.Ellipsis, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public LastElementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lastElement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLastElement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLastElement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLastElement(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final LastElementContext lastElement() throws RecognitionException {
        LastElementContext _localctx = new LastElementContext(_ctx, getState());
        enterRule(_localctx, 112, RULE_lastElement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(605);
                match(Ellipsis);
                setState(606);
                match(Identifier);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ObjectLiteralContext extends ParserRuleContext {
        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public List<PropertyAssignmentContext> propertyAssignment() {
            return getRuleContexts(PropertyAssignmentContext.class);
        }

        public PropertyAssignmentContext propertyAssignment(int i) {
            return getRuleContext(PropertyAssignmentContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public ObjectLiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_objectLiteral;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterObjectLiteral(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitObjectLiteral(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitObjectLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ObjectLiteralContext objectLiteral() throws RecognitionException {
        ObjectLiteralContext _localctx = new ObjectLiteralContext(_ctx, getState());
        enterRule(_localctx, 114, RULE_objectLiteral);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(608);
                match(OpenBrace);
                setState(617);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OpenBracket)
                        | (1L << NullLiteral) | (1L << BooleanLiteral) | (1L << DecimalLiteral)
                        | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                        | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral)
                        | (1L << Break) | (1L << Do) | (1L << Instanceof))) != 0)
                        || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64))
                                & ((1L << (Typeof - 64)) | (1L << (Case - 64)) | (1L << (Else - 64))
                                        | (1L << (New - 64)) | (1L << (Var - 64))
                                        | (1L << (Catch - 64)) | (1L << (Finally - 64))
                                        | (1L << (Return - 64)) | (1L << (Void - 64))
                                        | (1L << (Continue - 64)) | (1L << (For - 64))
                                        | (1L << (Switch - 64)) | (1L << (While - 64))
                                        | (1L << (Debugger - 64)) | (1L << (Function - 64))
                                        | (1L << (This - 64)) | (1L << (With - 64))
                                        | (1L << (Default - 64)) | (1L << (If - 64))
                                        | (1L << (Throw - 64)) | (1L << (Delete - 64))
                                        | (1L << (In - 64)) | (1L << (Try - 64))
                                        | (1L << (Sharable - 64)) | (1L << (Class - 64))
                                        | (1L << (Enum - 64)) | (1L << (Extends - 64))
                                        | (1L << (Super - 64)) | (1L << (Const - 64))
                                        | (1L << (Export - 64)) | (1L << (Import - 64))
                                        | (1L << (Implements - 64)) | (1L << (Let - 64))
                                        | (1L << (Private - 64)) | (1L << (Public - 64))
                                        | (1L << (Interface - 64)) | (1L << (Package - 64))
                                        | (1L << (Protected - 64)) | (1L << (Static - 64))
                                        | (1L << (Yield - 64)) | (1L << (Identifier - 64))
                                        | (1L << (StringLiteral - 64)))) != 0)) {
                    {
                        setState(609);
                        propertyAssignment();
                        setState(614);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 63, _ctx);
                        while (_alt != 2
                                && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                            if (_alt == 1) {
                                {
                                    {
                                        setState(610);
                                        match(Comma);
                                        setState(611);
                                        propertyAssignment();
                                    }
                                }
                            }
                            setState(616);
                            _errHandler.sync(this);
                            _alt = getInterpreter().adaptivePredict(_input, 63, _ctx);
                        }
                    }
                }

                setState(620);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == Comma) {
                    {
                        setState(619);
                        match(Comma);
                    }
                }

                setState(622);
                match(CloseBrace);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class PropertyAssignmentContext extends ParserRuleContext {
        public PropertyAssignmentContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_propertyAssignment;
        }

        public PropertyAssignmentContext() {}

        public void copyFrom(PropertyAssignmentContext ctx) {
            super.copyFrom(ctx);
        }
    }
    public static class PropertyExpressionAssignmentContext extends PropertyAssignmentContext {
        public PropertyNameContext propertyName() {
            return getRuleContext(PropertyNameContext.class, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode Colon() {
            return getToken(YJSParser.Colon, 0);
        }

        public TerminalNode Assign() {
            return getToken(YJSParser.Assign, 0);
        }

        public PropertyExpressionAssignmentContext(PropertyAssignmentContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPropertyExpressionAssignment(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPropertyExpressionAssignment(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitPropertyExpressionAssignment(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ComputedPropertyExpressionAssignmentContext
            extends PropertyAssignmentContext {
        public TerminalNode OpenBracket() {
            return getToken(YJSParser.OpenBracket, 0);
        }

        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode CloseBracket() {
            return getToken(YJSParser.CloseBracket, 0);
        }

        public TerminalNode Colon() {
            return getToken(YJSParser.Colon, 0);
        }

        public ComputedPropertyExpressionAssignmentContext(PropertyAssignmentContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterComputedPropertyExpressionAssignment(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitComputedPropertyExpressionAssignment(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitComputedPropertyExpressionAssignment(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class PropertyShorthandContext extends PropertyAssignmentContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public PropertyShorthandContext(PropertyAssignmentContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPropertyShorthand(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPropertyShorthand(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPropertyShorthand(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final PropertyAssignmentContext propertyAssignment() throws RecognitionException {
        PropertyAssignmentContext _localctx = new PropertyAssignmentContext(_ctx, getState());
        enterRule(_localctx, 116, RULE_propertyAssignment);
        int _la;
        try {
            setState(635);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 66, _ctx)) {
                case 1:
                    _localctx = new PropertyExpressionAssignmentContext(_localctx);
                    enterOuterAlt(_localctx, 1); {
                    setState(624);
                    propertyName();
                    setState(625);
                    _la = _input.LA(1);
                    if (!(_la == Assign || _la == Colon)) {
                        _errHandler.recoverInline(this);
                    } else {
                        if (_input.LA(1) == Token.EOF)
                            matchedEOF = true;
                        _errHandler.reportMatch(this);
                        consume();
                    }
                    setState(626);
                    singleExpression(0);
                }
                    break;
                case 2:
                    _localctx = new ComputedPropertyExpressionAssignmentContext(_localctx);
                    enterOuterAlt(_localctx, 2); {
                    setState(628);
                    match(OpenBracket);
                    setState(629);
                    singleExpression(0);
                    setState(630);
                    match(CloseBracket);
                    setState(631);
                    match(Colon);
                    setState(632);
                    singleExpression(0);
                }
                    break;
                case 3:
                    _localctx = new PropertyShorthandContext(_localctx);
                    enterOuterAlt(_localctx, 3); {
                    setState(634);
                    match(Identifier);
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class PropertyNameContext extends ParserRuleContext {
        public IdentifierNameContext identifierName() {
            return getRuleContext(IdentifierNameContext.class, 0);
        }

        public TerminalNode StringLiteral() {
            return getToken(YJSParser.StringLiteral, 0);
        }

        public NumericLiteralContext numericLiteral() {
            return getRuleContext(NumericLiteralContext.class, 0);
        }

        public PropertyNameContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_propertyName;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPropertyName(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPropertyName(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPropertyName(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final PropertyNameContext propertyName() throws RecognitionException {
        PropertyNameContext _localctx = new PropertyNameContext(_ctx, getState());
        enterRule(_localctx, 118, RULE_propertyName);
        try {
            setState(640);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case NullLiteral:
                case BooleanLiteral:
                case Break:
                case Do:
                case Instanceof:
                case Typeof:
                case Case:
                case Else:
                case New:
                case Var:
                case Catch:
                case Finally:
                case Return:
                case Void:
                case Continue:
                case For:
                case Switch:
                case While:
                case Debugger:
                case Function:
                case This:
                case With:
                case Default:
                case If:
                case Throw:
                case Delete:
                case In:
                case Try:
                case Sharable:
                case Class:
                case Enum:
                case Extends:
                case Super:
                case Const:
                case Export:
                case Import:
                case Implements:
                case Let:
                case Private:
                case Public:
                case Interface:
                case Package:
                case Protected:
                case Static:
                case Yield:
                case Identifier:
                    enterOuterAlt(_localctx, 1); {
                    setState(637);
                    identifierName();
                }
                    break;
                case StringLiteral:
                    enterOuterAlt(_localctx, 2); {
                    setState(638);
                    match(StringLiteral);
                }
                    break;
                case DecimalLiteral:
                case HexIntegerLiteral:
                case OctalIntegerLiteral:
                case OctalIntegerLiteral2:
                case BinaryIntegerLiteral:
                    enterOuterAlt(_localctx, 3); {
                    setState(639);
                    numericLiteral();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArgumentsContext extends ParserRuleContext {
        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public LastArgumentContext lastArgument() {
            return getRuleContext(LastArgumentContext.class, 0);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public ArgumentsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arguments;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArguments(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArguments(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArguments(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArgumentsContext arguments() throws RecognitionException {
        ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
        enterRule(_localctx, 120, RULE_arguments);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(642);
                match(OpenParen);
                setState(656);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                    case RegularExpressionLiteral:
                    case OpenBracket:
                    case OpenParen:
                    case OpenBrace:
                    case PlusPlus:
                    case MinusMinus:
                    case Plus:
                    case Minus:
                    case BitNot:
                    case Not:
                    case NullLiteral:
                    case BooleanLiteral:
                    case DecimalLiteral:
                    case HexIntegerLiteral:
                    case OctalIntegerLiteral:
                    case OctalIntegerLiteral2:
                    case BinaryIntegerLiteral:
                    case Typeof:
                    case New:
                    case This:
                    case Super:
                    case Identifier:
                    case StringLiteral:
                    case TemplateStringLiteral: {
                        setState(643);
                        singleExpression(0);
                        setState(648);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 68, _ctx);
                        while (_alt != 2
                                && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                            if (_alt == 1) {
                                {
                                    {
                                        setState(644);
                                        match(Comma);
                                        setState(645);
                                        singleExpression(0);
                                    }
                                }
                            }
                            setState(650);
                            _errHandler.sync(this);
                            _alt = getInterpreter().adaptivePredict(_input, 68, _ctx);
                        }
                        setState(653);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                        if (_la == Comma) {
                            {
                                setState(651);
                                match(Comma);
                                setState(652);
                                lastArgument();
                            }
                        }

                    }
                        break;
                    case Ellipsis: {
                        setState(655);
                        lastArgument();
                    }
                        break;
                    case CloseParen:
                        break;
                    default:
                        break;
                }
                setState(658);
                match(CloseParen);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LastArgumentContext extends ParserRuleContext {
        public TerminalNode Ellipsis() {
            return getToken(YJSParser.Ellipsis, 0);
        }

        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public LastArgumentContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lastArgument;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLastArgument(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLastArgument(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLastArgument(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final LastArgumentContext lastArgument() throws RecognitionException {
        LastArgumentContext _localctx = new LastArgumentContext(_ctx, getState());
        enterRule(_localctx, 122, RULE_lastArgument);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(660);
                match(Ellipsis);
                setState(661);
                match(Identifier);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExpressionSequenceContext extends ParserRuleContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(YJSParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(YJSParser.Comma, i);
        }

        public ExpressionSequenceContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expressionSequence;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterExpressionSequence(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitExpressionSequence(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitExpressionSequence(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ExpressionSequenceContext expressionSequence() throws RecognitionException {
        ExpressionSequenceContext _localctx = new ExpressionSequenceContext(_ctx, getState());
        enterRule(_localctx, 124, RULE_expressionSequence);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(663);
                singleExpression(0);
                setState(668);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 71, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(664);
                                match(Comma);
                                setState(665);
                                singleExpression(0);
                            }
                        }
                    }
                    setState(670);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 71, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class SingleExpressionContext extends ParserRuleContext {
        public SingleExpressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_singleExpression;
        }

        public SingleExpressionContext() {}

        public void copyFrom(SingleExpressionContext ctx) {
            super.copyFrom(ctx);
        }
    }
    public static class TemplateStringExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode TemplateStringLiteral() {
            return getToken(YJSParser.TemplateStringLiteral, 0);
        }

        public TemplateStringExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterTemplateStringExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitTemplateStringExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitTemplateStringExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class TernaryExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode QuestionMark() {
            return getToken(YJSParser.QuestionMark, 0);
        }

        public TerminalNode Colon() {
            return getToken(YJSParser.Colon, 0);
        }

        public TernaryExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterTernaryExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitTernaryExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitTernaryExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class LogicalAndExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode And() {
            return getToken(YJSParser.And, 0);
        }

        public LogicalAndExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLogicalAndExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLogicalAndExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLogicalAndExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class PreIncrementExpressionContext extends SingleExpressionContext {
        public TerminalNode PlusPlus() {
            return getToken(YJSParser.PlusPlus, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public PreIncrementExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPreIncrementExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPreIncrementExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPreIncrementExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ObjectLiteralExpressionContext extends SingleExpressionContext {
        public ObjectLiteralContext objectLiteral() {
            return getRuleContext(ObjectLiteralContext.class, 0);
        }

        public ObjectLiteralExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterObjectLiteralExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitObjectLiteralExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitObjectLiteralExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class InExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode In() {
            return getToken(YJSParser.In, 0);
        }

        public InExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterInExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitInExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitInExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class LogicalOrExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Or() {
            return getToken(YJSParser.Or, 0);
        }

        public LogicalOrExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLogicalOrExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLogicalOrExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLogicalOrExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class NotExpressionContext extends SingleExpressionContext {
        public TerminalNode Not() {
            return getToken(YJSParser.Not, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public NotExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterNotExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitNotExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitNotExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class PreDecreaseExpressionContext extends SingleExpressionContext {
        public TerminalNode MinusMinus() {
            return getToken(YJSParser.MinusMinus, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public PreDecreaseExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPreDecreaseExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPreDecreaseExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPreDecreaseExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ArgumentsExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public ArgumentsContext arguments() {
            return getRuleContext(ArgumentsContext.class, 0);
        }

        public ArgumentsExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArgumentsExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArgumentsExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArgumentsExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ThisExpressionContext extends SingleExpressionContext {
        public TerminalNode This() {
            return getToken(YJSParser.This, 0);
        }

        public ThisExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterThisExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitThisExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitThisExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class UnaryMinusExpressionContext extends SingleExpressionContext {
        public TerminalNode Minus() {
            return getToken(YJSParser.Minus, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public UnaryMinusExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterUnaryMinusExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitUnaryMinusExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitUnaryMinusExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class AssignmentExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Assign() {
            return getToken(YJSParser.Assign, 0);
        }

        public AssignmentExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAssignmentExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAssignmentExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAssignmentExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class PostDecreaseExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode MinusMinus() {
            return getToken(YJSParser.MinusMinus, 0);
        }

        public PostDecreaseExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPostDecreaseExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPostDecreaseExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPostDecreaseExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class TypeofExpressionContext extends SingleExpressionContext {
        public TerminalNode Typeof() {
            return getToken(YJSParser.Typeof, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TypeofExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterTypeofExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitTypeofExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitTypeofExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class InstanceofExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Instanceof() {
            return getToken(YJSParser.Instanceof, 0);
        }

        public InstanceofExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterInstanceofExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitInstanceofExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitInstanceofExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class UnaryPlusExpressionContext extends SingleExpressionContext {
        public TerminalNode Plus() {
            return getToken(YJSParser.Plus, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public UnaryPlusExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterUnaryPlusExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitUnaryPlusExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitUnaryPlusExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ArrowFunctionExpressionContext extends SingleExpressionContext {
        public ArrowFunctionParametersContext arrowFunctionParameters() {
            return getRuleContext(ArrowFunctionParametersContext.class, 0);
        }

        public TerminalNode ARROW() {
            return getToken(YJSParser.ARROW, 0);
        }

        public ArrowFunctionBodyContext arrowFunctionBody() {
            return getRuleContext(ArrowFunctionBodyContext.class, 0);
        }

        public ArrowFunctionExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArrowFunctionExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArrowFunctionExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArrowFunctionExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class EqualityExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Equals_() {
            return getToken(YJSParser.Equals_, 0);
        }

        public TerminalNode NotEquals() {
            return getToken(YJSParser.NotEquals, 0);
        }

        public TerminalNode IdentityEquals() {
            return getToken(YJSParser.IdentityEquals, 0);
        }

        public TerminalNode IdentityNotEquals() {
            return getToken(YJSParser.IdentityNotEquals, 0);
        }

        public EqualityExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEqualityExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEqualityExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEqualityExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class BitXOrExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode BitXOr() {
            return getToken(YJSParser.BitXOr, 0);
        }

        public BitXOrExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBitXOrExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBitXOrExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBitXOrExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class SuperExpressionContext extends SingleExpressionContext {
        public TerminalNode Super() {
            return getToken(YJSParser.Super, 0);
        }

        public SuperExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterSuperExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitSuperExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitSuperExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class MultiplicativeExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Multiply() {
            return getToken(YJSParser.Multiply, 0);
        }

        public TerminalNode Divide() {
            return getToken(YJSParser.Divide, 0);
        }

        public TerminalNode Modulus() {
            return getToken(YJSParser.Modulus, 0);
        }

        public MultiplicativeExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterMultiplicativeExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitMultiplicativeExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitMultiplicativeExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class BitShiftExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode LeftShiftArithmetic() {
            return getToken(YJSParser.LeftShiftArithmetic, 0);
        }

        public TerminalNode RightShiftArithmetic() {
            return getToken(YJSParser.RightShiftArithmetic, 0);
        }

        public TerminalNode RightShiftLogical() {
            return getToken(YJSParser.RightShiftLogical, 0);
        }

        public BitShiftExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBitShiftExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBitShiftExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBitShiftExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ParenthesizedExpressionContext extends SingleExpressionContext {
        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public ParenthesizedExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterParenthesizedExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitParenthesizedExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitParenthesizedExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class AdditiveExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode Plus() {
            return getToken(YJSParser.Plus, 0);
        }

        public TerminalNode Minus() {
            return getToken(YJSParser.Minus, 0);
        }

        public AdditiveExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAdditiveExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAdditiveExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAdditiveExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class RelationalExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode LessThan() {
            return getToken(YJSParser.LessThan, 0);
        }

        public TerminalNode MoreThan() {
            return getToken(YJSParser.MoreThan, 0);
        }

        public TerminalNode LessThanEquals() {
            return getToken(YJSParser.LessThanEquals, 0);
        }

        public TerminalNode GreaterThanEquals() {
            return getToken(YJSParser.GreaterThanEquals, 0);
        }

        public RelationalExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterRelationalExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitRelationalExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitRelationalExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class PostIncrementExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode PlusPlus() {
            return getToken(YJSParser.PlusPlus, 0);
        }

        public PostIncrementExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterPostIncrementExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitPostIncrementExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitPostIncrementExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class BitNotExpressionContext extends SingleExpressionContext {
        public TerminalNode BitNot() {
            return getToken(YJSParser.BitNot, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public BitNotExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBitNotExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBitNotExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBitNotExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class NewExpressionContext extends SingleExpressionContext {
        public TerminalNode New() {
            return getToken(YJSParser.New, 0);
        }

        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public ArgumentsContext arguments() {
            return getRuleContext(ArgumentsContext.class, 0);
        }

        public NewExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterNewExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitNewExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitNewExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class LiteralExpressionContext extends SingleExpressionContext {
        public LiteralContext literal() {
            return getRuleContext(LiteralContext.class, 0);
        }

        public LiteralExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLiteralExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLiteralExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLiteralExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class ArrayLiteralExpressionContext extends SingleExpressionContext {
        public ArrayLiteralContext arrayLiteral() {
            return getRuleContext(ArrayLiteralContext.class, 0);
        }

        public ArrayLiteralExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArrayLiteralExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArrayLiteralExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArrayLiteralExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class MemberDotExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode Dot() {
            return getToken(YJSParser.Dot, 0);
        }

        public IdentifierNameContext identifierName() {
            return getRuleContext(IdentifierNameContext.class, 0);
        }

        public MemberDotExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterMemberDotExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitMemberDotExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitMemberDotExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class MemberIndexExpressionContext extends SingleExpressionContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode OpenBracket() {
            return getToken(YJSParser.OpenBracket, 0);
        }

        public ExpressionSequenceContext expressionSequence() {
            return getRuleContext(ExpressionSequenceContext.class, 0);
        }

        public TerminalNode CloseBracket() {
            return getToken(YJSParser.CloseBracket, 0);
        }

        public MemberIndexExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterMemberIndexExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitMemberIndexExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitMemberIndexExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class IdentifierExpressionContext extends SingleExpressionContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public IdentifierExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterIdentifierExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitIdentifierExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitIdentifierExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class BitAndExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode BitAnd() {
            return getToken(YJSParser.BitAnd, 0);
        }

        public BitAndExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBitAndExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBitAndExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBitAndExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class BitOrExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public TerminalNode BitOr() {
            return getToken(YJSParser.BitOr, 0);
        }

        public BitOrExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterBitOrExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitBitOrExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitBitOrExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }
    public static class AssignmentOperatorExpressionContext extends SingleExpressionContext {
        public List<SingleExpressionContext> singleExpression() {
            return getRuleContexts(SingleExpressionContext.class);
        }

        public SingleExpressionContext singleExpression(int i) {
            return getRuleContext(SingleExpressionContext.class, i);
        }

        public AssignmentOperatorContext assignmentOperator() {
            return getRuleContext(AssignmentOperatorContext.class, 0);
        }

        public AssignmentOperatorExpressionContext(SingleExpressionContext ctx) {
            copyFrom(ctx);
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAssignmentOperatorExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAssignmentOperatorExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor)
                        .visitAssignmentOperatorExpression(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final SingleExpressionContext singleExpression() throws RecognitionException {
        return singleExpression(0);
    }

    private SingleExpressionContext singleExpression(int _p) throws RecognitionException {
        ParserRuleContext _parentctx = _ctx;
        int _parentState = getState();
        SingleExpressionContext _localctx = new SingleExpressionContext(_ctx, _parentState);
        SingleExpressionContext _prevctx = _localctx;
        int _startState = 126;
        enterRecursionRule(_localctx, 126, RULE_singleExpression, _p);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(705);
                _errHandler.sync(this);
                switch (getInterpreter().adaptivePredict(_input, 73, _ctx)) {
                    case 1: {
                        _localctx = new NewExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;

                        setState(672);
                        match(New);
                        setState(673);
                        singleExpression(0);
                        setState(675);
                        _errHandler.sync(this);
                        switch (getInterpreter().adaptivePredict(_input, 72, _ctx)) {
                            case 1: {
                                setState(674);
                                arguments();
                            }
                                break;
                        }
                    }
                        break;
                    case 2: {
                        _localctx = new TypeofExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(677);
                        match(Typeof);
                        setState(678);
                        singleExpression(31);
                    }
                        break;
                    case 3: {
                        _localctx = new PreIncrementExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(679);
                        match(PlusPlus);
                        setState(680);
                        singleExpression(30);
                    }
                        break;
                    case 4: {
                        _localctx = new PreDecreaseExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(681);
                        match(MinusMinus);
                        setState(682);
                        singleExpression(29);
                    }
                        break;
                    case 5: {
                        _localctx = new UnaryPlusExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(683);
                        match(Plus);
                        setState(684);
                        singleExpression(28);
                    }
                        break;
                    case 6: {
                        _localctx = new UnaryMinusExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(685);
                        match(Minus);
                        setState(686);
                        singleExpression(27);
                    }
                        break;
                    case 7: {
                        _localctx = new BitNotExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(687);
                        match(BitNot);
                        setState(688);
                        singleExpression(26);
                    }
                        break;
                    case 8: {
                        _localctx = new NotExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(689);
                        match(Not);
                        setState(690);
                        singleExpression(25);
                    }
                        break;
                    case 9: {
                        _localctx = new ThisExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(691);
                        match(This);
                    }
                        break;
                    case 10: {
                        _localctx = new IdentifierExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(692);
                        match(Identifier);
                    }
                        break;
                    case 11: {
                        _localctx = new SuperExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(693);
                        match(Super);
                    }
                        break;
                    case 12: {
                        _localctx = new LiteralExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(694);
                        literal();
                    }
                        break;
                    case 13: {
                        _localctx = new ArrayLiteralExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(695);
                        arrayLiteral();
                    }
                        break;
                    case 14: {
                        _localctx = new ObjectLiteralExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(696);
                        objectLiteral();
                    }
                        break;
                    case 15: {
                        _localctx = new ParenthesizedExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(697);
                        match(OpenParen);
                        setState(698);
                        expressionSequence();
                        setState(699);
                        match(CloseParen);
                    }
                        break;
                    case 16: {
                        _localctx = new ArrowFunctionExpressionContext(_localctx);
                        _ctx = _localctx;
                        _prevctx = _localctx;
                        setState(701);
                        arrowFunctionParameters();
                        setState(702);
                        match(ARROW);
                        setState(703);
                        arrowFunctionBody();
                    }
                        break;
                }
                _ctx.stop = _input.LT(-1);
                setState(776);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 75, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        if (_parseListeners != null)
                            triggerExitRuleEvent();
                        _prevctx = _localctx;
                        {
                            setState(774);
                            _errHandler.sync(this);
                            switch (getInterpreter().adaptivePredict(_input, 74, _ctx)) {
                                case 1: {
                                    _localctx = new MultiplicativeExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(707);
                                    if (!(precpred(_ctx, 24)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 24)");
                                    setState(708);
                                    _la = _input.LA(1);
                                    if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Multiply)
                                            | (1L << Divide) | (1L << Modulus))) != 0))) {
                                        _errHandler.recoverInline(this);
                                    } else {
                                        if (_input.LA(1) == Token.EOF)
                                            matchedEOF = true;
                                        _errHandler.reportMatch(this);
                                        consume();
                                    }
                                    setState(709);
                                    singleExpression(25);
                                }
                                    break;
                                case 2: {
                                    _localctx = new AdditiveExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(710);
                                    if (!(precpred(_ctx, 23)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 23)");
                                    setState(711);
                                    _la = _input.LA(1);
                                    if (!(_la == Plus || _la == Minus)) {
                                        _errHandler.recoverInline(this);
                                    } else {
                                        if (_input.LA(1) == Token.EOF)
                                            matchedEOF = true;
                                        _errHandler.reportMatch(this);
                                        consume();
                                    }
                                    setState(712);
                                    singleExpression(24);
                                }
                                    break;
                                case 3: {
                                    _localctx = new BitShiftExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(713);
                                    if (!(precpred(_ctx, 22)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 22)");
                                    setState(714);
                                    _la = _input.LA(1);
                                    if (!((((_la) & ~0x3f) == 0
                                            && ((1L << _la) & ((1L << RightShiftArithmetic)
                                                    | (1L << LeftShiftArithmetic)
                                                    | (1L << RightShiftLogical))) != 0))) {
                                        _errHandler.recoverInline(this);
                                    } else {
                                        if (_input.LA(1) == Token.EOF)
                                            matchedEOF = true;
                                        _errHandler.reportMatch(this);
                                        consume();
                                    }
                                    setState(715);
                                    singleExpression(23);
                                }
                                    break;
                                case 4: {
                                    _localctx = new RelationalExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(716);
                                    if (!(precpred(_ctx, 21)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 21)");
                                    setState(717);
                                    _la = _input.LA(1);
                                    if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LessThan)
                                            | (1L << MoreThan) | (1L << LessThanEquals)
                                            | (1L << GreaterThanEquals))) != 0))) {
                                        _errHandler.recoverInline(this);
                                    } else {
                                        if (_input.LA(1) == Token.EOF)
                                            matchedEOF = true;
                                        _errHandler.reportMatch(this);
                                        consume();
                                    }
                                    setState(718);
                                    singleExpression(22);
                                }
                                    break;
                                case 5: {
                                    _localctx = new InstanceofExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(719);
                                    if (!(precpred(_ctx, 20)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 20)");
                                    setState(720);
                                    match(Instanceof);
                                    setState(721);
                                    singleExpression(21);
                                }
                                    break;
                                case 6: {
                                    _localctx = new InExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(722);
                                    if (!(precpred(_ctx, 19)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 19)");
                                    setState(723);
                                    match(In);
                                    setState(724);
                                    singleExpression(20);
                                }
                                    break;
                                case 7: {
                                    _localctx = new EqualityExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(725);
                                    if (!(precpred(_ctx, 18)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 18)");
                                    setState(726);
                                    _la = _input.LA(1);
                                    if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Equals_)
                                            | (1L << NotEquals) | (1L << IdentityEquals)
                                            | (1L << IdentityNotEquals))) != 0))) {
                                        _errHandler.recoverInline(this);
                                    } else {
                                        if (_input.LA(1) == Token.EOF)
                                            matchedEOF = true;
                                        _errHandler.reportMatch(this);
                                        consume();
                                    }
                                    setState(727);
                                    singleExpression(19);
                                }
                                    break;
                                case 8: {
                                    _localctx = new BitAndExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(728);
                                    if (!(precpred(_ctx, 17)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 17)");
                                    setState(729);
                                    match(BitAnd);
                                    setState(730);
                                    singleExpression(18);
                                }
                                    break;
                                case 9: {
                                    _localctx = new BitXOrExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(731);
                                    if (!(precpred(_ctx, 16)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 16)");
                                    setState(732);
                                    match(BitXOr);
                                    setState(733);
                                    singleExpression(17);
                                }
                                    break;
                                case 10: {
                                    _localctx = new BitOrExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(734);
                                    if (!(precpred(_ctx, 15)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 15)");
                                    setState(735);
                                    match(BitOr);
                                    setState(736);
                                    singleExpression(16);
                                }
                                    break;
                                case 11: {
                                    _localctx = new LogicalAndExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(737);
                                    if (!(precpred(_ctx, 14)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 14)");
                                    setState(738);
                                    match(And);
                                    setState(739);
                                    singleExpression(15);
                                }
                                    break;
                                case 12: {
                                    _localctx = new LogicalOrExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(740);
                                    if (!(precpred(_ctx, 13)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 13)");
                                    setState(741);
                                    match(Or);
                                    setState(742);
                                    singleExpression(14);
                                }
                                    break;
                                case 13: {
                                    _localctx = new TernaryExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(743);
                                    if (!(precpred(_ctx, 12)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 12)");
                                    setState(744);
                                    match(QuestionMark);
                                    setState(745);
                                    singleExpression(0);
                                    setState(746);
                                    match(Colon);
                                    setState(747);
                                    singleExpression(13);
                                }
                                    break;
                                case 14: {
                                    _localctx = new AssignmentExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(749);
                                    if (!(precpred(_ctx, 11)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 11)");
                                    setState(750);
                                    match(Assign);
                                    setState(751);
                                    singleExpression(12);
                                }
                                    break;
                                case 15: {
                                    _localctx = new AssignmentOperatorExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(752);
                                    if (!(precpred(_ctx, 10)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 10)");
                                    setState(753);
                                    assignmentOperator();
                                    setState(754);
                                    singleExpression(11);
                                }
                                    break;
                                case 16: {
                                    _localctx = new MemberIndexExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(756);
                                    if (!(precpred(_ctx, 37)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 37)");
                                    setState(757);
                                    match(OpenBracket);
                                    setState(758);
                                    expressionSequence();
                                    setState(759);
                                    match(CloseBracket);
                                }
                                    break;
                                case 17: {
                                    _localctx = new MemberDotExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(761);
                                    if (!(precpred(_ctx, 36)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 36)");
                                    setState(762);
                                    match(Dot);
                                    setState(763);
                                    identifierName();
                                }
                                    break;
                                case 18: {
                                    _localctx = new ArgumentsExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(764);
                                    if (!(precpred(_ctx, 35)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 35)");
                                    setState(765);
                                    arguments();
                                }
                                    break;
                                case 19: {
                                    _localctx = new PostIncrementExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(766);
                                    if (!(precpred(_ctx, 33)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 33)");
                                    setState(767);
                                    if (!(notLineTerminator()))
                                        throw new FailedPredicateException(this,
                                                "notLineTerminator()");
                                    setState(768);
                                    match(PlusPlus);
                                }
                                    break;
                                case 20: {
                                    _localctx = new PostDecreaseExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(769);
                                    if (!(precpred(_ctx, 32)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 32)");
                                    setState(770);
                                    if (!(notLineTerminator()))
                                        throw new FailedPredicateException(this,
                                                "notLineTerminator()");
                                    setState(771);
                                    match(MinusMinus);
                                }
                                    break;
                                case 21: {
                                    _localctx = new TemplateStringExpressionContext(
                                            new SingleExpressionContext(_parentctx, _parentState));
                                    pushNewRecursionContext(_localctx, _startState,
                                            RULE_singleExpression);
                                    setState(772);
                                    if (!(precpred(_ctx, 9)))
                                        throw new FailedPredicateException(this,
                                                "precpred(_ctx, 9)");
                                    setState(773);
                                    match(TemplateStringLiteral);
                                }
                                    break;
                            }
                        }
                    }
                    setState(778);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 75, _ctx);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            unrollRecursionContexts(_parentctx);
        }
        return _localctx;
    }

    public static class ArrowFunctionParametersContext extends ParserRuleContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public TerminalNode OpenParen() {
            return getToken(YJSParser.OpenParen, 0);
        }

        public TerminalNode CloseParen() {
            return getToken(YJSParser.CloseParen, 0);
        }

        public FormalParameterListContext formalParameterList() {
            return getRuleContext(FormalParameterListContext.class, 0);
        }

        public ArrowFunctionParametersContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arrowFunctionParameters;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArrowFunctionParameters(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArrowFunctionParameters(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArrowFunctionParameters(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArrowFunctionParametersContext arrowFunctionParameters()
            throws RecognitionException {
        ArrowFunctionParametersContext _localctx =
                new ArrowFunctionParametersContext(_ctx, getState());
        enterRule(_localctx, 128, RULE_arrowFunctionParameters);
        int _la;
        try {
            setState(785);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case Identifier:
                    enterOuterAlt(_localctx, 1); {
                    setState(779);
                    match(Identifier);
                }
                    break;
                case OpenParen:
                    enterOuterAlt(_localctx, 2); {
                    setState(780);
                    match(OpenParen);
                    setState(782);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la)
                            & ((1L << OpenBracket) | (1L << OpenBrace) | (1L << Ellipsis))) != 0)
                            || _la == Identifier) {
                        {
                            setState(781);
                            formalParameterList();
                        }
                    }

                    setState(784);
                    match(CloseParen);
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArrowFunctionBodyContext extends ParserRuleContext {
        public SingleExpressionContext singleExpression() {
            return getRuleContext(SingleExpressionContext.class, 0);
        }

        public TerminalNode OpenBrace() {
            return getToken(YJSParser.OpenBrace, 0);
        }

        public FunctionBodyContext functionBody() {
            return getRuleContext(FunctionBodyContext.class, 0);
        }

        public TerminalNode CloseBrace() {
            return getToken(YJSParser.CloseBrace, 0);
        }

        public ArrowFunctionBodyContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arrowFunctionBody;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterArrowFunctionBody(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitArrowFunctionBody(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitArrowFunctionBody(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ArrowFunctionBodyContext arrowFunctionBody() throws RecognitionException {
        ArrowFunctionBodyContext _localctx = new ArrowFunctionBodyContext(_ctx, getState());
        enterRule(_localctx, 130, RULE_arrowFunctionBody);
        try {
            setState(792);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 78, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1); {
                    setState(787);
                    singleExpression(0);
                }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2); {
                    setState(788);
                    match(OpenBrace);
                    setState(789);
                    functionBody();
                    setState(790);
                    match(CloseBrace);
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AssignmentOperatorContext extends ParserRuleContext {
        public TerminalNode MultiplyAssign() {
            return getToken(YJSParser.MultiplyAssign, 0);
        }

        public TerminalNode DivideAssign() {
            return getToken(YJSParser.DivideAssign, 0);
        }

        public TerminalNode ModulusAssign() {
            return getToken(YJSParser.ModulusAssign, 0);
        }

        public TerminalNode PlusAssign() {
            return getToken(YJSParser.PlusAssign, 0);
        }

        public TerminalNode MinusAssign() {
            return getToken(YJSParser.MinusAssign, 0);
        }

        public TerminalNode LeftShiftArithmeticAssign() {
            return getToken(YJSParser.LeftShiftArithmeticAssign, 0);
        }

        public TerminalNode RightShiftArithmeticAssign() {
            return getToken(YJSParser.RightShiftArithmeticAssign, 0);
        }

        public TerminalNode RightShiftLogicalAssign() {
            return getToken(YJSParser.RightShiftLogicalAssign, 0);
        }

        public TerminalNode BitAndAssign() {
            return getToken(YJSParser.BitAndAssign, 0);
        }

        public TerminalNode BitXorAssign() {
            return getToken(YJSParser.BitXorAssign, 0);
        }

        public TerminalNode BitOrAssign() {
            return getToken(YJSParser.BitOrAssign, 0);
        }

        public AssignmentOperatorContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignmentOperator;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterAssignmentOperator(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitAssignmentOperator(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitAssignmentOperator(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final AssignmentOperatorContext assignmentOperator() throws RecognitionException {
        AssignmentOperatorContext _localctx = new AssignmentOperatorContext(_ctx, getState());
        enterRule(_localctx, 132, RULE_assignmentOperator);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(794);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MultiplyAssign)
                        | (1L << DivideAssign) | (1L << ModulusAssign) | (1L << PlusAssign)
                        | (1L << MinusAssign) | (1L << LeftShiftArithmeticAssign)
                        | (1L << RightShiftArithmeticAssign) | (1L << RightShiftLogicalAssign)
                        | (1L << BitAndAssign) | (1L << BitXorAssign)
                        | (1L << BitOrAssign))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class LiteralContext extends ParserRuleContext {
        public TerminalNode NullLiteral() {
            return getToken(YJSParser.NullLiteral, 0);
        }

        public TerminalNode BooleanLiteral() {
            return getToken(YJSParser.BooleanLiteral, 0);
        }

        public TerminalNode StringLiteral() {
            return getToken(YJSParser.StringLiteral, 0);
        }

        public TerminalNode TemplateStringLiteral() {
            return getToken(YJSParser.TemplateStringLiteral, 0);
        }

        public TerminalNode RegularExpressionLiteral() {
            return getToken(YJSParser.RegularExpressionLiteral, 0);
        }

        public NumericLiteralContext numericLiteral() {
            return getRuleContext(NumericLiteralContext.class, 0);
        }

        public LiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_literal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterLiteral(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitLiteral(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final LiteralContext literal() throws RecognitionException {
        LiteralContext _localctx = new LiteralContext(_ctx, getState());
        enterRule(_localctx, 134, RULE_literal);
        try {
            setState(802);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case NullLiteral:
                    enterOuterAlt(_localctx, 1); {
                    setState(796);
                    match(NullLiteral);
                }
                    break;
                case BooleanLiteral:
                    enterOuterAlt(_localctx, 2); {
                    setState(797);
                    match(BooleanLiteral);
                }
                    break;
                case StringLiteral:
                    enterOuterAlt(_localctx, 3); {
                    setState(798);
                    match(StringLiteral);
                }
                    break;
                case TemplateStringLiteral:
                    enterOuterAlt(_localctx, 4); {
                    setState(799);
                    match(TemplateStringLiteral);
                }
                    break;
                case RegularExpressionLiteral:
                    enterOuterAlt(_localctx, 5); {
                    setState(800);
                    match(RegularExpressionLiteral);
                }
                    break;
                case DecimalLiteral:
                case HexIntegerLiteral:
                case OctalIntegerLiteral:
                case OctalIntegerLiteral2:
                case BinaryIntegerLiteral:
                    enterOuterAlt(_localctx, 6); {
                    setState(801);
                    numericLiteral();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class NumericLiteralContext extends ParserRuleContext {
        public TerminalNode DecimalLiteral() {
            return getToken(YJSParser.DecimalLiteral, 0);
        }

        public TerminalNode HexIntegerLiteral() {
            return getToken(YJSParser.HexIntegerLiteral, 0);
        }

        public TerminalNode OctalIntegerLiteral() {
            return getToken(YJSParser.OctalIntegerLiteral, 0);
        }

        public TerminalNode OctalIntegerLiteral2() {
            return getToken(YJSParser.OctalIntegerLiteral2, 0);
        }

        public TerminalNode BinaryIntegerLiteral() {
            return getToken(YJSParser.BinaryIntegerLiteral, 0);
        }

        public NumericLiteralContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_numericLiteral;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterNumericLiteral(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitNumericLiteral(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitNumericLiteral(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final NumericLiteralContext numericLiteral() throws RecognitionException {
        NumericLiteralContext _localctx = new NumericLiteralContext(_ctx, getState());
        enterRule(_localctx, 136, RULE_numericLiteral);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(804);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DecimalLiteral)
                        | (1L << HexIntegerLiteral) | (1L << OctalIntegerLiteral)
                        | (1L << OctalIntegerLiteral2) | (1L << BinaryIntegerLiteral))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class IdentifierNameContext extends ParserRuleContext {
        public TerminalNode Identifier() {
            return getToken(YJSParser.Identifier, 0);
        }

        public ReservedWordContext reservedWord() {
            return getRuleContext(ReservedWordContext.class, 0);
        }

        public IdentifierNameContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_identifierName;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterIdentifierName(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitIdentifierName(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitIdentifierName(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final IdentifierNameContext identifierName() throws RecognitionException {
        IdentifierNameContext _localctx = new IdentifierNameContext(_ctx, getState());
        enterRule(_localctx, 138, RULE_identifierName);
        try {
            setState(808);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case Identifier:
                    enterOuterAlt(_localctx, 1); {
                    setState(806);
                    match(Identifier);
                }
                    break;
                case NullLiteral:
                case BooleanLiteral:
                case Break:
                case Do:
                case Instanceof:
                case Typeof:
                case Case:
                case Else:
                case New:
                case Var:
                case Catch:
                case Finally:
                case Return:
                case Void:
                case Continue:
                case For:
                case Switch:
                case While:
                case Debugger:
                case Function:
                case This:
                case With:
                case Default:
                case If:
                case Throw:
                case Delete:
                case In:
                case Try:
                case Sharable:
                case Class:
                case Enum:
                case Extends:
                case Super:
                case Const:
                case Export:
                case Import:
                case Implements:
                case Let:
                case Private:
                case Public:
                case Interface:
                case Package:
                case Protected:
                case Static:
                case Yield:
                    enterOuterAlt(_localctx, 2); {
                    setState(807);
                    reservedWord();
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ReservedWordContext extends ParserRuleContext {
        public KeywordContext keyword() {
            return getRuleContext(KeywordContext.class, 0);
        }

        public TerminalNode NullLiteral() {
            return getToken(YJSParser.NullLiteral, 0);
        }

        public TerminalNode BooleanLiteral() {
            return getToken(YJSParser.BooleanLiteral, 0);
        }

        public ReservedWordContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_reservedWord;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterReservedWord(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitReservedWord(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitReservedWord(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final ReservedWordContext reservedWord() throws RecognitionException {
        ReservedWordContext _localctx = new ReservedWordContext(_ctx, getState());
        enterRule(_localctx, 140, RULE_reservedWord);
        try {
            setState(813);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
                case Break:
                case Do:
                case Instanceof:
                case Typeof:
                case Case:
                case Else:
                case New:
                case Var:
                case Catch:
                case Finally:
                case Return:
                case Void:
                case Continue:
                case For:
                case Switch:
                case While:
                case Debugger:
                case Function:
                case This:
                case With:
                case Default:
                case If:
                case Throw:
                case Delete:
                case In:
                case Try:
                case Sharable:
                case Class:
                case Enum:
                case Extends:
                case Super:
                case Const:
                case Export:
                case Import:
                case Implements:
                case Let:
                case Private:
                case Public:
                case Interface:
                case Package:
                case Protected:
                case Static:
                case Yield:
                    enterOuterAlt(_localctx, 1); {
                    setState(810);
                    keyword();
                }
                    break;
                case NullLiteral:
                    enterOuterAlt(_localctx, 2); {
                    setState(811);
                    match(NullLiteral);
                }
                    break;
                case BooleanLiteral:
                    enterOuterAlt(_localctx, 3); {
                    setState(812);
                    match(BooleanLiteral);
                }
                    break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class KeywordContext extends ParserRuleContext {
        public TerminalNode Break() {
            return getToken(YJSParser.Break, 0);
        }

        public TerminalNode Do() {
            return getToken(YJSParser.Do, 0);
        }

        public TerminalNode Instanceof() {
            return getToken(YJSParser.Instanceof, 0);
        }

        public TerminalNode Typeof() {
            return getToken(YJSParser.Typeof, 0);
        }

        public TerminalNode Case() {
            return getToken(YJSParser.Case, 0);
        }

        public TerminalNode Else() {
            return getToken(YJSParser.Else, 0);
        }

        public TerminalNode New() {
            return getToken(YJSParser.New, 0);
        }

        public TerminalNode Var() {
            return getToken(YJSParser.Var, 0);
        }

        public TerminalNode Catch() {
            return getToken(YJSParser.Catch, 0);
        }

        public TerminalNode Finally() {
            return getToken(YJSParser.Finally, 0);
        }

        public TerminalNode Return() {
            return getToken(YJSParser.Return, 0);
        }

        public TerminalNode Void() {
            return getToken(YJSParser.Void, 0);
        }

        public TerminalNode Continue() {
            return getToken(YJSParser.Continue, 0);
        }

        public TerminalNode For() {
            return getToken(YJSParser.For, 0);
        }

        public TerminalNode Switch() {
            return getToken(YJSParser.Switch, 0);
        }

        public TerminalNode While() {
            return getToken(YJSParser.While, 0);
        }

        public TerminalNode Debugger() {
            return getToken(YJSParser.Debugger, 0);
        }

        public TerminalNode Function() {
            return getToken(YJSParser.Function, 0);
        }

        public TerminalNode This() {
            return getToken(YJSParser.This, 0);
        }

        public TerminalNode With() {
            return getToken(YJSParser.With, 0);
        }

        public TerminalNode Default() {
            return getToken(YJSParser.Default, 0);
        }

        public TerminalNode If() {
            return getToken(YJSParser.If, 0);
        }

        public TerminalNode Throw() {
            return getToken(YJSParser.Throw, 0);
        }

        public TerminalNode Delete() {
            return getToken(YJSParser.Delete, 0);
        }

        public TerminalNode In() {
            return getToken(YJSParser.In, 0);
        }

        public TerminalNode Try() {
            return getToken(YJSParser.Try, 0);
        }

        public TerminalNode Class() {
            return getToken(YJSParser.Class, 0);
        }

        public TerminalNode Enum() {
            return getToken(YJSParser.Enum, 0);
        }

        public TerminalNode Extends() {
            return getToken(YJSParser.Extends, 0);
        }

        public TerminalNode Super() {
            return getToken(YJSParser.Super, 0);
        }

        public TerminalNode Const() {
            return getToken(YJSParser.Const, 0);
        }

        public TerminalNode Export() {
            return getToken(YJSParser.Export, 0);
        }

        public TerminalNode Import() {
            return getToken(YJSParser.Import, 0);
        }

        public TerminalNode Implements() {
            return getToken(YJSParser.Implements, 0);
        }

        public TerminalNode Let() {
            return getToken(YJSParser.Let, 0);
        }

        public TerminalNode Private() {
            return getToken(YJSParser.Private, 0);
        }

        public TerminalNode Public() {
            return getToken(YJSParser.Public, 0);
        }

        public TerminalNode Interface() {
            return getToken(YJSParser.Interface, 0);
        }

        public TerminalNode Package() {
            return getToken(YJSParser.Package, 0);
        }

        public TerminalNode Protected() {
            return getToken(YJSParser.Protected, 0);
        }

        public TerminalNode Static() {
            return getToken(YJSParser.Static, 0);
        }

        public TerminalNode Yield() {
            return getToken(YJSParser.Yield, 0);
        }

        public TerminalNode Sharable() {
            return getToken(YJSParser.Sharable, 0);
        }

        public KeywordContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_keyword;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterKeyword(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitKeyword(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitKeyword(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final KeywordContext keyword() throws RecognitionException {
        KeywordContext _localctx = new KeywordContext(_ctx, getState());
        enterRule(_localctx, 142, RULE_keyword);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(815);
                _la = _input.LA(1);
                if (!(((((_la - 61)) & ~0x3f) == 0 && ((1L << (_la - 61)) & ((1L << (Break - 61))
                        | (1L << (Do - 61)) | (1L << (Instanceof - 61)) | (1L << (Typeof - 61))
                        | (1L << (Case - 61)) | (1L << (Else - 61)) | (1L << (New - 61))
                        | (1L << (Var - 61)) | (1L << (Catch - 61)) | (1L << (Finally - 61))
                        | (1L << (Return - 61)) | (1L << (Void - 61)) | (1L << (Continue - 61))
                        | (1L << (For - 61)) | (1L << (Switch - 61)) | (1L << (While - 61))
                        | (1L << (Debugger - 61)) | (1L << (Function - 61)) | (1L << (This - 61))
                        | (1L << (With - 61)) | (1L << (Default - 61)) | (1L << (If - 61))
                        | (1L << (Throw - 61)) | (1L << (Delete - 61)) | (1L << (In - 61))
                        | (1L << (Try - 61)) | (1L << (Sharable - 61)) | (1L << (Class - 61))
                        | (1L << (Enum - 61)) | (1L << (Extends - 61)) | (1L << (Super - 61))
                        | (1L << (Const - 61)) | (1L << (Export - 61)) | (1L << (Import - 61))
                        | (1L << (Implements - 61)) | (1L << (Let - 61)) | (1L << (Private - 61))
                        | (1L << (Public - 61)) | (1L << (Interface - 61)) | (1L << (Package - 61))
                        | (1L << (Protected - 61)) | (1L << (Static - 61))
                        | (1L << (Yield - 61)))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF)
                        matchedEOF = true;
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class EosContext extends ParserRuleContext {
        public TerminalNode SemiColon() {
            return getToken(YJSParser.SemiColon, 0);
        }

        public TerminalNode EOF() {
            return getToken(YJSParser.EOF, 0);
        }

        public EosContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_eos;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).enterEos(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof YJSParserListener)
                ((YJSParserListener) listener).exitEos(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof YJSParserVisitor)
                return ((YJSParserVisitor<? extends T>) visitor).visitEos(this);
            else
                return visitor.visitChildren(this);
        }
    }

    public final EosContext eos() throws RecognitionException {
        EosContext _localctx = new EosContext(_ctx, getState());
        enterRule(_localctx, 144, RULE_eos);
        try {
            setState(821);
            _errHandler.sync(this);
            switch (getInterpreter().adaptivePredict(_input, 82, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1); {
                    setState(817);
                    match(SemiColon);
                }
                    break;
                case 2:
                    enterOuterAlt(_localctx, 2); {
                    setState(818);
                    match(EOF);
                }
                    break;
                case 3:
                    enterOuterAlt(_localctx, 3); {
                    setState(819);
                    if (!(lineTerminatorAhead()))
                        throw new FailedPredicateException(this, "lineTerminatorAhead()");
                }
                    break;
                case 4:
                    enterOuterAlt(_localctx, 4); {
                    setState(820);
                    if (!(closeBrace()))
                        throw new FailedPredicateException(this, "closeBrace()");
                }
                    break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
        switch (ruleIndex) {
            case 26:
                return expressionStatement_sempred((ExpressionStatementContext) _localctx,
                        predIndex);
            case 28:
                return iterationStatement_sempred((IterationStatementContext) _localctx, predIndex);
            case 30:
                return continueStatement_sempred((ContinueStatementContext) _localctx, predIndex);
            case 31:
                return breakStatement_sempred((BreakStatementContext) _localctx, predIndex);
            case 32:
                return returnStatement_sempred((ReturnStatementContext) _localctx, predIndex);
            case 39:
                return throwStatement_sempred((ThrowStatementContext) _localctx, predIndex);
            case 63:
                return singleExpression_sempred((SingleExpressionContext) _localctx, predIndex);
            case 72:
                return eos_sempred((EosContext) _localctx, predIndex);
        }
        return true;
    }

    private boolean expressionStatement_sempred(ExpressionStatementContext _localctx,
            int predIndex) {
        switch (predIndex) {
            case 0:
                return notOpenBraceAndNotFunction();
        }
        return true;
    }

    private boolean iterationStatement_sempred(IterationStatementContext _localctx, int predIndex) {
        switch (predIndex) {
            case 1:
                return p("of");
            case 2:
                return p("of");
        }
        return true;
    }

    private boolean continueStatement_sempred(ContinueStatementContext _localctx, int predIndex) {
        switch (predIndex) {
            case 3:
                return notLineTerminator();
        }
        return true;
    }

    private boolean breakStatement_sempred(BreakStatementContext _localctx, int predIndex) {
        switch (predIndex) {
            case 4:
                return notLineTerminator();
        }
        return true;
    }

    private boolean returnStatement_sempred(ReturnStatementContext _localctx, int predIndex) {
        switch (predIndex) {
            case 5:
                return notLineTerminator();
        }
        return true;
    }

    private boolean throwStatement_sempred(ThrowStatementContext _localctx, int predIndex) {
        switch (predIndex) {
            case 6:
                return notLineTerminator();
        }
        return true;
    }

    private boolean singleExpression_sempred(SingleExpressionContext _localctx, int predIndex) {
        switch (predIndex) {
            case 7:
                return precpred(_ctx, 24);
            case 8:
                return precpred(_ctx, 23);
            case 9:
                return precpred(_ctx, 22);
            case 10:
                return precpred(_ctx, 21);
            case 11:
                return precpred(_ctx, 20);
            case 12:
                return precpred(_ctx, 19);
            case 13:
                return precpred(_ctx, 18);
            case 14:
                return precpred(_ctx, 17);
            case 15:
                return precpred(_ctx, 16);
            case 16:
                return precpred(_ctx, 15);
            case 17:
                return precpred(_ctx, 14);
            case 18:
                return precpred(_ctx, 13);
            case 19:
                return precpred(_ctx, 12);
            case 20:
                return precpred(_ctx, 11);
            case 21:
                return precpred(_ctx, 10);
            case 22:
                return precpred(_ctx, 37);
            case 23:
                return precpred(_ctx, 36);
            case 24:
                return precpred(_ctx, 35);
            case 25:
                return precpred(_ctx, 33);
            case 26:
                return notLineTerminator();
            case 27:
                return precpred(_ctx, 32);
            case 28:
                return notLineTerminator();
            case 29:
                return precpred(_ctx, 9);
        }
        return true;
    }

    private boolean eos_sempred(EosContext _localctx, int predIndex) {
        switch (predIndex) {
            case 30:
                return lineTerminatorAhead();
            case 31:
                return closeBrace();
        }
        return true;
    }

    public static final String _serializedATN =
            "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3}\u033a\4\2\t\2\4"
                    + "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"
                    + "\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"
                    + "\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"
                    + "\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"
                    + "\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"
                    + ",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"
                    + "\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="
                    + "\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"
                    + "\tI\4J\tJ\3\2\5\2\u0096\n\2\3\2\3\2\3\3\5\3\u009b\n\3\3\3\3\3\3\3\3\3"
                    + "\6\3\u00a1\n\3\r\3\16\3\u00a2\3\3\3\3\3\4\6\4\u00a8\n\4\r\4\16\4\u00a9"
                    + "\3\5\3\5\3\5\3\5\5\5\u00b0\n\5\3\5\3\5\3\6\3\6\3\6\7\6\u00b7\n\6\f\6\16"
                    + "\6\u00ba\13\6\3\7\3\7\3\7\5\7\u00bf\n\7\3\b\3\b\3\b\3\b\3\b\5\b\u00c6"
                    + "\n\b\3\t\5\t\u00c9\n\t\3\t\3\t\3\t\3\t\5\t\u00cf\n\t\3\t\3\t\3\t\3\n\3"
                    + "\n\5\n\u00d6\n\n\3\n\3\n\3\n\3\n\5\n\u00dc\n\n\3\n\3\n\3\n\5\n\u00e1\n"
                    + "\n\3\n\3\n\5\n\u00e5\n\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\6\16\u00ee\n\16"
                    + "\r\16\16\16\u00ef\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3"
                    + "\21\3\21\7\21\u00fe\n\21\f\21\16\21\u0101\13\21\3\22\3\22\3\22\3\22\3"
                    + "\22\3\22\3\22\3\22\3\22\3\22\5\22\u010d\n\22\3\23\3\23\5\23\u0111\n\23"
                    + "\3\23\3\23\3\24\6\24\u0116\n\24\r\24\16\24\u0117\3\25\3\25\3\26\3\26\3"
                    + "\26\3\26\3\27\3\27\3\30\3\30\3\30\3\30\3\31\3\31\3\31\7\31\u0129\n\31"
                    + "\f\31\16\31\u012c\13\31\3\32\3\32\3\32\5\32\u0131\n\32\3\33\3\33\3\34"
                    + "\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u0140\n\35\3\36"
                    + "\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36"
                    + "\3\36\3\36\5\36\u0153\n\36\3\36\3\36\5\36\u0157\n\36\3\36\3\36\5\36\u015b"
                    + "\n\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u0165\n\36\3\36\3\36"
                    + "\5\36\u0169\n\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u0174"
                    + "\n\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u0181"
                    + "\n\36\3\36\3\36\3\36\3\36\5\36\u0187\n\36\3\37\3\37\3 \3 \3 \5 \u018e"
                    + "\n \3 \3 \3!\3!\3!\5!\u0195\n!\3!\3!\3\"\3\"\3\"\5\"\u019c\n\"\3\"\3\""
                    + "\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3%\3%\5%\u01ae\n%\3%\3%\5%\u01b2"
                    + "\n%\5%\u01b4\n%\3%\3%\3&\6&\u01b9\n&\r&\16&\u01ba\3\'\3\'\3\'\3\'\5\'"
                    + "\u01c1\n\'\3(\3(\3(\5(\u01c6\n(\3)\3)\3)\3)\3)\3*\3*\3*\3*\5*\u01d1\n"
                    + "*\3*\5*\u01d4\n*\3+\3+\3+\3+\3+\3+\3,\3,\3,\3-\3-\3-\3.\5.\u01e3\n.\3"
                    + ".\5.\u01e6\n.\3.\3.\3.\3.\5.\u01ec\n.\3.\3.\5.\u01f0\n.\3.\3.\3.\3.\3"
                    + "/\3/\3/\3/\3\60\3\60\5\60\u01fc\n\60\3\60\3\60\7\60\u0200\n\60\f\60\16"
                    + "\60\u0203\13\60\3\60\3\60\3\61\5\61\u0208\n\61\3\61\3\61\3\62\3\62\3\62"
                    + "\5\62\u020f\n\62\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\7\63\u0219\n"
                    + "\63\f\63\16\63\u021c\13\63\3\63\3\63\5\63\u0220\n\63\3\63\3\63\3\63\5"
                    + "\63\u0225\n\63\3\64\3\64\3\64\5\64\u022a\n\64\3\65\3\65\3\65\3\66\5\66"
                    + "\u0230\n\66\3\67\6\67\u0233\n\67\r\67\16\67\u0234\38\38\78\u0239\n8\f"
                    + "8\168\u023c\138\38\58\u023f\n8\38\78\u0242\n8\f8\168\u0245\138\38\38\3"
                    + "9\39\69\u024b\n9\r9\169\u024c\39\79\u0250\n9\f9\169\u0253\139\39\69\u0256"
                    + "\n9\r9\169\u0257\39\59\u025b\n9\39\59\u025e\n9\3:\3:\3:\3;\3;\3;\3;\7"
                    + ";\u0267\n;\f;\16;\u026a\13;\5;\u026c\n;\3;\5;\u026f\n;\3;\3;\3<\3<\3<"
                    + "\3<\3<\3<\3<\3<\3<\3<\3<\5<\u027e\n<\3=\3=\3=\5=\u0283\n=\3>\3>\3>\3>"
                    + "\7>\u0289\n>\f>\16>\u028c\13>\3>\3>\5>\u0290\n>\3>\5>\u0293\n>\3>\3>\3"
                    + "?\3?\3?\3@\3@\3@\7@\u029d\n@\f@\16@\u02a0\13@\3A\3A\3A\3A\5A\u02a6\nA"
                    + "\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A"
                    + "\3A\3A\3A\3A\3A\5A\u02c4\nA\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A"
                    + "\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A"
                    + "\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A"
                    + "\3A\3A\3A\3A\3A\3A\3A\7A\u0309\nA\fA\16A\u030c\13A\3B\3B\3B\5B\u0311\n"
                    + "B\3B\5B\u0314\nB\3C\3C\3C\3C\3C\5C\u031b\nC\3D\3D\3E\3E\3E\3E\3E\3E\5"
                    + "E\u0325\nE\3F\3F\3G\3G\5G\u032b\nG\3H\3H\3H\5H\u0330\nH\3I\3I\3J\3J\3"
                    + "J\3J\5J\u0338\nJ\3J\2\3\u0080K\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36"
                    + " \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082"
                    + "\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\2\16\3\2il\3\2_`\3\2"
                    + "\\^\4\2\16\16\20\20\3\2\31\33\3\2\25\26\3\2\34\36\3\2\37\"\3\2#&\3\2,"
                    + "\66\3\2:>\6\2?X[[bhmu\2\u0380\2\u0095\3\2\2\2\4\u009a\3\2\2\2\6\u00a7"
                    + "\3\2\2\2\b\u00ab\3\2\2\2\n\u00b3\3\2\2\2\f\u00be\3\2\2\2\16\u00c5\3\2"
                    + "\2\2\20\u00c8\3\2\2\2\22\u00e4\3\2\2\2\24\u00e6\3\2\2\2\26\u00e8\3\2\2"
                    + "\2\30\u00ea\3\2\2\2\32\u00ed\3\2\2\2\34\u00f1\3\2\2\2\36\u00f5\3\2\2\2"
                    + " \u00fa\3\2\2\2\"\u010c\3\2\2\2$\u010e\3\2\2\2&\u0115\3\2\2\2(\u0119\3"
                    + "\2\2\2*\u011b\3\2\2\2,\u011f\3\2\2\2.\u0121\3\2\2\2\60\u0125\3\2\2\2\62"
                    + "\u012d\3\2\2\2\64\u0132\3\2\2\2\66\u0134\3\2\2\28\u0138\3\2\2\2:\u0186"
                    + "\3\2\2\2<\u0188\3\2\2\2>\u018a\3\2\2\2@\u0191\3\2\2\2B\u0198\3\2\2\2D"
                    + "\u019f\3\2\2\2F\u01a5\3\2\2\2H\u01ab\3\2\2\2J\u01b8\3\2\2\2L\u01bc\3\2"
                    + "\2\2N\u01c2\3\2\2\2P\u01c7\3\2\2\2R\u01cc\3\2\2\2T\u01d5\3\2\2\2V\u01db"
                    + "\3\2\2\2X\u01de\3\2\2\2Z\u01e2\3\2\2\2\\\u01f5\3\2\2\2^\u01fb\3\2\2\2"
                    + "`\u0207\3\2\2\2b\u020b\3\2\2\2d\u0224\3\2\2\2f\u0226\3\2\2\2h\u022b\3"
                    + "\2\2\2j\u022f\3\2\2\2l\u0232\3\2\2\2n\u0236\3\2\2\2p\u025d\3\2\2\2r\u025f"
                    + "\3\2\2\2t\u0262\3\2\2\2v\u027d\3\2\2\2x\u0282\3\2\2\2z\u0284\3\2\2\2|"
                    + "\u0296\3\2\2\2~\u0299\3\2\2\2\u0080\u02c3\3\2\2\2\u0082\u0313\3\2\2\2"
                    + "\u0084\u031a\3\2\2\2\u0086\u031c\3\2\2\2\u0088\u0324\3\2\2\2\u008a\u0326"
                    + "\3\2\2\2\u008c\u032a\3\2\2\2\u008e\u032f\3\2\2\2\u0090\u0331\3\2\2\2\u0092"
                    + "\u0337\3\2\2\2\u0094\u0096\5\32\16\2\u0095\u0094\3\2\2\2\u0095\u0096\3"
                    + "\2\2\2\u0096\u0097\3\2\2\2\u0097\u0098\5\4\3\2\u0098\3\3\2\2\2\u0099\u009b"
                    + "\5\6\4\2\u009a\u0099\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009c\3\2\2\2\u009c"
                    + "\u009d\t\2\2\2\u009d\u009e\7v\2\2\u009e\u00a0\7\n\2\2\u009f\u00a1\5\16"
                    + "\b\2\u00a0\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2"
                    + "\u00a3\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a5\7\13\2\2\u00a5\5\3\2\2"
                    + "\2\u00a6\u00a8\5\b\5\2\u00a7\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00a7"
                    + "\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\7\3\2\2\2\u00ab\u00ac\7Z\2\2\u00ac"
                    + "\u00ad\7v\2\2\u00ad\u00af\7\b\2\2\u00ae\u00b0\5\n\6\2\u00af\u00ae\3\2"
                    + "\2\2\u00af\u00b0\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b2\7\t\2\2\u00b2"
                    + "\t\3\2\2\2\u00b3\u00b8\5\f\7\2\u00b4\u00b5\7\r\2\2\u00b5\u00b7\5\f\7\2"
                    + "\u00b6\u00b4\3\2\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9"
                    + "\3\2\2\2\u00b9\13\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb\u00bf\5\u008aF\2\u00bc"
                    + "\u00bf\7w\2\2\u00bd\u00bf\5t;\2\u00be\u00bb\3\2\2\2\u00be\u00bc\3\2\2"
                    + "\2\u00be\u00bd\3\2\2\2\u00bf\r\3\2\2\2\u00c0\u00c6\5\\/\2\u00c1\u00c6"
                    + "\5Z.\2\u00c2\u00c6\5\20\t\2\u00c3\u00c6\5\22\n\2\u00c4\u00c6\5(\25\2\u00c5"
                    + "\u00c0\3\2\2\2\u00c5\u00c1\3\2\2\2\u00c5\u00c2\3\2\2\2\u00c5\u00c3\3\2"
                    + "\2\2\u00c5\u00c4\3\2\2\2\u00c6\17\3\2\2\2\u00c7\u00c9\5\6\4\2\u00c8\u00c7"
                    + "\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cb\7q\2\2\u00cb"
                    + "\u00cc\7v\2\2\u00cc\u00ce\7\b\2\2\u00cd\u00cf\5d\63\2\u00ce\u00cd\3\2"
                    + "\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d1\7\t\2\2\u00d1"
                    + "\u00d2\5\u0092J\2\u00d2\21\3\2\2\2\u00d3\u00d5\7Y\2\2\u00d4\u00d6\5\24"
                    + "\13\2\u00d5\u00d4\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7"
                    + "\u00d8\7v\2\2\u00d8\u00e5\7\f\2\2\u00d9\u00db\7Y\2\2\u00da\u00dc\5\24"
                    + "\13\2\u00db\u00da\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd"
                    + "\u00de\7v\2\2\u00de\u00e0\7\b\2\2\u00df\u00e1\5\26\f\2\u00e0\u00df\3\2"
                    + "\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e3\7\t\2\2\u00e3"
                    + "\u00e5\7\f\2\2\u00e4\u00d3\3\2\2\2\u00e4\u00d9\3\2\2\2\u00e5\23\3\2\2"
                    + "\2\u00e6\u00e7\t\3\2\2\u00e7\25\3\2\2\2\u00e8\u00e9\t\4\2\2\u00e9\27\3"
                    + "\2\2\2\u00ea\u00eb\5\"\22\2\u00eb\31\3\2\2\2\u00ec\u00ee\5\34\17\2\u00ed"
                    + "\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef\u00f0\3\2"
                    + "\2\2\u00f0\33\3\2\2\2\u00f1\u00f2\7h\2\2\u00f2\u00f3\7w\2\2\u00f3\u00f4"
                    + "\7\f\2\2\u00f4\35\3\2\2\2\u00f5\u00f6\7g\2\2\u00f6\u00f7\7v\2\2\u00f7"
                    + "\u00f8\5 \21\2\u00f8\u00f9\7\f\2\2\u00f9\37\3\2\2\2\u00fa\u00ff\7:\2\2"
                    + "\u00fb\u00fc\7\22\2\2\u00fc\u00fe\7:\2\2\u00fd\u00fb\3\2\2\2\u00fe\u0101"
                    + "\3\2\2\2\u00ff\u00fd\3\2\2\2\u00ff\u0100\3\2\2\2\u0100!\3\2\2\2\u0101"
                    + "\u00ff\3\2\2\2\u0102\u010d\5$\23\2\u0103\u010d\5.\30\2\u0104\u010d\5\64"
                    + "\33\2\u0105\u010d\5\66\34\2\u0106\u010d\58\35\2\u0107\u010d\5:\36\2\u0108"
                    + "\u010d\5> \2\u0109\u010d\5@!\2\u010a\u010d\5B\"\2\u010b\u010d\5F$\2\u010c"
                    + "\u0102\3\2\2\2\u010c\u0103\3\2\2\2\u010c\u0104\3\2\2\2\u010c\u0105\3\2"
                    + "\2\2\u010c\u0106\3\2\2\2\u010c\u0107\3\2\2\2\u010c\u0108\3\2\2\2\u010c"
                    + "\u0109\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010b\3\2\2\2\u010d#\3\2\2\2"
                    + "\u010e\u0110\7\n\2\2\u010f\u0111\5&\24\2\u0110\u010f\3\2\2\2\u0110\u0111"
                    + "\3\2\2\2\u0111\u0112\3\2\2\2\u0112\u0113\7\13\2\2\u0113%\3\2\2\2\u0114"
                    + "\u0116\5\"\22\2\u0115\u0114\3\2\2\2\u0116\u0117\3\2\2\2\u0117\u0115\3"
                    + "\2\2\2\u0117\u0118\3\2\2\2\u0118\'\3\2\2\2\u0119\u011a\5*\26\2\u011a)"
                    + "\3\2\2\2\u011b\u011c\5,\27\2\u011c\u011d\5\60\31\2\u011d\u011e\5\u0092"
                    + "J\2\u011e+\3\2\2\2\u011f\u0120\7[\2\2\u0120-\3\2\2\2\u0121\u0122\5<\37"
                    + "\2\u0122\u0123\5\60\31\2\u0123\u0124\5\u0092J\2\u0124/\3\2\2\2\u0125\u012a"
                    + "\5\62\32\2\u0126\u0127\7\r\2\2\u0127\u0129\5\62\32\2\u0128\u0126\3\2\2"
                    + "\2\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b\61"
                    + "\3\2\2\2\u012c\u012a\3\2\2\2\u012d\u0130\7v\2\2\u012e\u012f\7\16\2\2\u012f"
                    + "\u0131\5\u0080A\2\u0130\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131\63\3\2"
                    + "\2\2\u0132\u0133\7\f\2\2\u0133\65\3\2\2\2\u0134\u0135\6\34\2\2\u0135\u0136"
                    + "\5~@\2\u0136\u0137\5\u0092J\2\u0137\67\3\2\2\2\u0138\u0139\7T\2\2\u0139"
                    + "\u013a\7\b\2\2\u013a\u013b\5~@\2\u013b\u013c\7\t\2\2\u013c\u013f\5\"\22"
                    + "\2\u013d\u013e\7D\2\2\u013e\u0140\5\"\22\2\u013f\u013d\3\2\2\2\u013f\u0140"
                    + "\3\2\2\2\u01409\3\2\2\2\u0141\u0142\7@\2\2\u0142\u0143\5\"\22\2\u0143"
                    + "\u0144\7N\2\2\u0144\u0145\7\b\2\2\u0145\u0146\5~@\2\u0146\u0147\7\t\2"
                    + "\2\u0147\u0148\5\u0092J\2\u0148\u0187\3\2\2\2\u0149\u014a\7N\2\2\u014a"
                    + "\u014b\7\b\2\2\u014b\u014c\5~@\2\u014c\u014d\7\t\2\2\u014d\u014e\5\"\22"
                    + "\2\u014e\u0187\3\2\2\2\u014f\u0150\7L\2\2\u0150\u0152\7\b\2\2\u0151\u0153"
                    + "\5~@\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154\3\2\2\2\u0154"
                    + "\u0156\7\f\2\2\u0155\u0157\5~@\2\u0156\u0155\3\2\2\2\u0156\u0157\3\2\2"
                    + "\2\u0157\u0158\3\2\2\2\u0158\u015a\7\f\2\2\u0159\u015b\5~@\2\u015a\u0159"
                    + "\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\7\t\2\2\u015d"
                    + "\u0187\5\"\22\2\u015e\u015f\7L\2\2\u015f\u0160\7\b\2\2\u0160\u0161\5<"
                    + "\37\2\u0161\u0162\5\60\31\2\u0162\u0164\7\f\2\2\u0163\u0165\5~@\2\u0164"
                    + "\u0163\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0168\7\f"
                    + "\2\2\u0167\u0169\5~@\2\u0168\u0167\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016a"
                    + "\3\2\2\2\u016a\u016b\7\t\2\2\u016b\u016c\5\"\22\2\u016c\u0187\3\2\2\2"
                    + "\u016d\u016e\7L\2\2\u016e\u016f\7\b\2\2\u016f\u0173\5\u0080A\2\u0170\u0174"
                    + "\7W\2\2\u0171\u0172\7v\2\2\u0172\u0174\6\36\3\2\u0173\u0170\3\2\2\2\u0173"
                    + "\u0171\3\2\2\2\u0174\u0175\3\2\2\2\u0175\u0176\5~@\2\u0176\u0177\7\t\2"
                    + "\2\u0177\u0178\5\"\22\2\u0178\u0187\3\2\2\2\u0179\u017a\7L\2\2\u017a\u017b"
                    + "\7\b\2\2\u017b\u017c\5<\37\2\u017c\u0180\5\62\32\2\u017d\u0181\7W\2\2"
                    + "\u017e\u017f\7v\2\2\u017f\u0181\6\36\4\2\u0180\u017d\3\2\2\2\u0180\u017e"
                    + "\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0183\5~@\2\u0183\u0184\7\t\2\2\u0184"
                    + "\u0185\5\"\22\2\u0185\u0187\3\2\2\2\u0186\u0141\3\2\2\2\u0186\u0149\3"
                    + "\2\2\2\u0186\u014f\3\2\2\2\u0186\u015e\3\2\2\2\u0186\u016d\3\2\2\2\u0186"
                    + "\u0179\3\2\2\2\u0187;\3\2\2\2\u0188\u0189\7F\2\2\u0189=\3\2\2\2\u018a"
                    + "\u018d\7K\2\2\u018b\u018c\6 \5\2\u018c\u018e\7v\2\2\u018d\u018b\3\2\2"
                    + "\2\u018d\u018e\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u0190\5\u0092J\2\u0190"
                    + "?\3\2\2\2\u0191\u0194\7?\2\2\u0192\u0193\6!\6\2\u0193\u0195\7v\2\2\u0194"
                    + "\u0192\3\2\2\2\u0194\u0195\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0197\5\u0092"
                    + "J\2\u0197A\3\2\2\2\u0198\u019b\7I\2\2\u0199\u019a\6\"\7\2\u019a\u019c"
                    + "\5~@\2\u019b\u0199\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019d\3\2\2\2\u019d"
                    + "\u019e\5\u0092J\2\u019eC\3\2\2\2\u019f\u01a0\7R\2\2\u01a0\u01a1\7\b\2"
                    + "\2\u01a1\u01a2\5~@\2\u01a2\u01a3\7\t\2\2\u01a3\u01a4\5\"\22\2\u01a4E\3"
                    + "\2\2\2\u01a5\u01a6\7M\2\2\u01a6\u01a7\7\b\2\2\u01a7\u01a8\5~@\2\u01a8"
                    + "\u01a9\7\t\2\2\u01a9\u01aa\5H%\2\u01aaG\3\2\2\2\u01ab\u01ad\7\n\2\2\u01ac"
                    + "\u01ae\5J&\2\u01ad\u01ac\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae\u01b3\3\2\2"
                    + "\2\u01af\u01b1\5N(\2\u01b0\u01b2\5J&\2\u01b1\u01b0\3\2\2\2\u01b1\u01b2"
                    + "\3\2\2\2\u01b2\u01b4\3\2\2\2\u01b3\u01af\3\2\2\2\u01b3\u01b4\3\2\2\2\u01b4"
                    + "\u01b5\3\2\2\2\u01b5\u01b6\7\13\2\2\u01b6I\3\2\2\2\u01b7\u01b9\5L\'\2"
                    + "\u01b8\u01b7\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01b8\3\2\2\2\u01ba\u01bb"
                    + "\3\2\2\2\u01bbK\3\2\2\2\u01bc\u01bd\7C\2\2\u01bd\u01be\5~@\2\u01be\u01c0"
                    + "\7\20\2\2\u01bf\u01c1\5&\24\2\u01c0\u01bf\3\2\2\2\u01c0\u01c1\3\2\2\2"
                    + "\u01c1M\3\2\2\2\u01c2\u01c3\7S\2\2\u01c3\u01c5\7\20\2\2\u01c4\u01c6\5"
                    + "&\24\2\u01c5\u01c4\3\2\2\2\u01c5\u01c6\3\2\2\2\u01c6O\3\2\2\2\u01c7\u01c8"
                    + "\7U\2\2\u01c8\u01c9\6)\b\2\u01c9\u01ca\5~@\2\u01ca\u01cb\5\u0092J\2\u01cb"
                    + "Q\3\2\2\2\u01cc\u01cd\7X\2\2\u01cd\u01d3\5$\23\2\u01ce\u01d0\5T+\2\u01cf"
                    + "\u01d1\5V,\2\u01d0\u01cf\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d4\3\2\2"
                    + "\2\u01d2\u01d4\5V,\2\u01d3\u01ce\3\2\2\2\u01d3\u01d2\3\2\2\2\u01d4S\3"
                    + "\2\2\2\u01d5\u01d6\7G\2\2\u01d6\u01d7\7\b\2\2\u01d7\u01d8\7v\2\2\u01d8"
                    + "\u01d9\7\t\2\2\u01d9\u01da\5$\23\2\u01daU\3\2\2\2\u01db\u01dc\7H\2\2\u01dc"
                    + "\u01dd\5$\23\2\u01ddW\3\2\2\2\u01de\u01df\7O\2\2\u01df\u01e0\5\u0092J"
                    + "\2\u01e0Y\3\2\2\2\u01e1\u01e3\5\6\4\2\u01e2\u01e1\3\2\2\2\u01e2\u01e3"
                    + "\3\2\2\2\u01e3\u01e5\3\2\2\2\u01e4\u01e6\7g\2\2\u01e5\u01e4\3\2\2\2\u01e5"
                    + "\u01e6\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7\u01e8\7P\2\2\u01e8\u01e9\7v\2"
                    + "\2\u01e9\u01eb\7\b\2\2\u01ea\u01ec\5d\63\2\u01eb\u01ea\3\2\2\2\u01eb\u01ec"
                    + "\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed\u01ef\7\t\2\2\u01ee\u01f0\7a\2\2\u01ef"
                    + "\u01ee\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1\u01f2\7\n"
                    + "\2\2\u01f2\u01f3\5j\66\2\u01f3\u01f4\7\13\2\2\u01f4[\3\2\2\2\u01f5\u01f6"
                    + "\7b\2\2\u01f6\u01f7\7v\2\2\u01f7\u01f8\5^\60\2\u01f8]\3\2\2\2\u01f9\u01fa"
                    + "\7d\2\2\u01fa\u01fc\5\u0080A\2\u01fb\u01f9\3\2\2\2\u01fb\u01fc\3\2\2\2"
                    + "\u01fc\u01fd\3\2\2\2\u01fd\u0201\7\n\2\2\u01fe\u0200\5`\61\2\u01ff\u01fe"
                    + "\3\2\2\2\u0200\u0203\3\2\2\2\u0201\u01ff\3\2\2\2\u0201\u0202\3\2\2\2\u0202"
                    + "\u0204\3\2\2\2\u0203\u0201\3\2\2\2\u0204\u0205\7\13\2\2\u0205_\3\2\2\2"
                    + "\u0206\u0208\7t\2\2\u0207\u0206\3\2\2\2\u0207\u0208\3\2\2\2\u0208\u0209"
                    + "\3\2\2\2\u0209\u020a\5b\62\2\u020aa\3\2\2\2\u020b\u020c\5x=\2\u020c\u020e"
                    + "\7\b\2\2\u020d\u020f\5d\63\2\u020e\u020d\3\2\2\2\u020e\u020f\3\2\2\2\u020f"
                    + "\u0210\3\2\2\2\u0210\u0211\7\t\2\2\u0211\u0212\7\n\2\2\u0212\u0213\5j"
                    + "\66\2\u0213\u0214\7\13\2\2\u0214c\3\2\2\2\u0215\u021a\5f\64\2\u0216\u0217"
                    + "\7\r\2\2\u0217\u0219\5f\64\2\u0218\u0216\3\2\2\2\u0219\u021c\3\2\2\2\u021a"
                    + "\u0218\3\2\2\2\u021a\u021b\3\2\2\2\u021b\u021f\3\2\2\2\u021c\u021a\3\2"
                    + "\2\2\u021d\u021e\7\r\2\2\u021e\u0220\5h\65\2\u021f\u021d\3\2\2\2\u021f"
                    + "\u0220\3\2\2\2\u0220\u0225\3\2\2\2\u0221\u0225\5h\65\2\u0222\u0225\5n"
                    + "8\2\u0223\u0225\5t;\2\u0224\u0215\3\2\2\2\u0224\u0221\3\2\2\2\u0224\u0222"
                    + "\3\2\2\2\u0224\u0223\3\2\2\2\u0225e\3\2\2\2\u0226\u0229\7v\2\2\u0227\u0228"
                    + "\7\16\2\2\u0228\u022a\5\u0080A\2\u0229\u0227\3\2\2\2\u0229\u022a\3\2\2"
                    + "\2\u022ag\3\2\2\2\u022b\u022c\7\21\2\2\u022c\u022d\7v\2\2\u022di\3\2\2"
                    + "\2\u022e\u0230\5l\67\2\u022f\u022e\3\2\2\2\u022f\u0230\3\2\2\2\u0230k"
                    + "\3\2\2\2\u0231\u0233\5\30\r\2\u0232\u0231\3\2\2\2\u0233\u0234\3\2\2\2"
                    + "\u0234\u0232\3\2\2\2\u0234\u0235\3\2\2\2\u0235m\3\2\2\2\u0236\u023a\7"
                    + "\6\2\2\u0237\u0239\7\r\2\2\u0238\u0237\3\2\2\2\u0239\u023c\3\2\2\2\u023a"
                    + "\u0238\3\2\2\2\u023a\u023b\3\2\2\2\u023b\u023e\3\2\2\2\u023c\u023a\3\2"
                    + "\2\2\u023d\u023f\5p9\2\u023e\u023d\3\2\2\2\u023e\u023f\3\2\2\2\u023f\u0243"
                    + "\3\2\2\2\u0240\u0242\7\r\2\2\u0241\u0240\3\2\2\2\u0242\u0245\3\2\2\2\u0243"
                    + "\u0241\3\2\2\2\u0243\u0244\3\2\2\2\u0244\u0246\3\2\2\2\u0245\u0243\3\2"
                    + "\2\2\u0246\u0247\7\7\2\2\u0247o\3\2\2\2\u0248\u0251\5\u0080A\2\u0249\u024b"
                    + "\7\r\2\2\u024a\u0249\3\2\2\2\u024b\u024c\3\2\2\2\u024c\u024a\3\2\2\2\u024c"
                    + "\u024d\3\2\2\2\u024d\u024e\3\2\2\2\u024e\u0250\5\u0080A\2\u024f\u024a"
                    + "\3\2\2\2\u0250\u0253\3\2\2\2\u0251\u024f\3\2\2\2\u0251\u0252\3\2\2\2\u0252"
                    + "\u025a\3\2\2\2\u0253\u0251\3\2\2\2\u0254\u0256\7\r\2\2\u0255\u0254\3\2"
                    + "\2\2\u0256\u0257\3\2\2\2\u0257\u0255\3\2\2\2\u0257\u0258\3\2\2\2\u0258"
                    + "\u0259\3\2\2\2\u0259\u025b\5r:\2\u025a\u0255\3\2\2\2\u025a\u025b\3\2\2"
                    + "\2\u025b\u025e\3\2\2\2\u025c\u025e\5r:\2\u025d\u0248\3\2\2\2\u025d\u025c"
                    + "\3\2\2\2\u025eq\3\2\2\2\u025f\u0260\7\21\2\2\u0260\u0261\7v\2\2\u0261"
                    + "s\3\2\2\2\u0262\u026b\7\n\2\2\u0263\u0268\5v<\2\u0264\u0265\7\r\2\2\u0265"
                    + "\u0267\5v<\2\u0266\u0264\3\2\2\2\u0267\u026a\3\2\2\2\u0268\u0266\3\2\2"
                    + "\2\u0268\u0269\3\2\2\2\u0269\u026c\3\2\2\2\u026a\u0268\3\2\2\2\u026b\u0263"
                    + "\3\2\2\2\u026b\u026c\3\2\2\2\u026c\u026e\3\2\2\2\u026d\u026f\7\r\2\2\u026e"
                    + "\u026d\3\2\2\2\u026e\u026f\3\2\2\2\u026f\u0270\3\2\2\2\u0270\u0271\7\13"
                    + "\2\2\u0271u\3\2\2\2\u0272\u0273\5x=\2\u0273\u0274\t\5\2\2\u0274\u0275"
                    + "\5\u0080A\2\u0275\u027e\3\2\2\2\u0276\u0277\7\6\2\2\u0277\u0278\5\u0080"
                    + "A\2\u0278\u0279\7\7\2\2\u0279\u027a\7\20\2\2\u027a\u027b\5\u0080A\2\u027b"
                    + "\u027e\3\2\2\2\u027c\u027e\7v\2\2\u027d\u0272\3\2\2\2\u027d\u0276\3\2"
                    + "\2\2\u027d\u027c\3\2\2\2\u027ew\3\2\2\2\u027f\u0283\5\u008cG\2\u0280\u0283"
                    + "\7w\2\2\u0281\u0283\5\u008aF\2\u0282\u027f\3\2\2\2\u0282\u0280\3\2\2\2"
                    + "\u0282\u0281\3\2\2\2\u0283y\3\2\2\2\u0284\u0292\7\b\2\2\u0285\u028a\5"
                    + "\u0080A\2\u0286\u0287\7\r\2\2\u0287\u0289\5\u0080A\2\u0288\u0286\3\2\2"
                    + "\2\u0289\u028c\3\2\2\2\u028a\u0288\3\2\2\2\u028a\u028b\3\2\2\2\u028b\u028f"
                    + "\3\2\2\2\u028c\u028a\3\2\2\2\u028d\u028e\7\r\2\2\u028e\u0290\5|?\2\u028f"
                    + "\u028d\3\2\2\2\u028f\u0290\3\2\2\2\u0290\u0293\3\2\2\2\u0291\u0293\5|"
                    + "?\2\u0292\u0285\3\2\2\2\u0292\u0291\3\2\2\2\u0292\u0293\3\2\2\2\u0293"
                    + "\u0294\3\2\2\2\u0294\u0295\7\t\2\2\u0295{\3\2\2\2\u0296\u0297\7\21\2\2"
                    + "\u0297\u0298\7v\2\2\u0298}\3\2\2\2\u0299\u029e\5\u0080A\2\u029a\u029b"
                    + "\7\r\2\2\u029b\u029d\5\u0080A\2\u029c\u029a\3\2\2\2\u029d\u02a0\3\2\2"
                    + "\2\u029e\u029c\3\2\2\2\u029e\u029f\3\2\2\2\u029f\177\3\2\2\2\u02a0\u029e"
                    + "\3\2\2\2\u02a1\u02a2\bA\1\2\u02a2\u02a3\7E\2\2\u02a3\u02a5\5\u0080A\2"
                    + "\u02a4\u02a6\5z>\2\u02a5\u02a4\3\2\2\2\u02a5\u02a6\3\2\2\2\u02a6\u02c4"
                    + "\3\2\2\2\u02a7\u02a8\7B\2\2\u02a8\u02c4\5\u0080A!\u02a9\u02aa\7\23\2\2"
                    + "\u02aa\u02c4\5\u0080A \u02ab\u02ac\7\24\2\2\u02ac\u02c4\5\u0080A\37\u02ad"
                    + "\u02ae\7\25\2\2\u02ae\u02c4\5\u0080A\36\u02af\u02b0\7\26\2\2\u02b0\u02c4"
                    + "\5\u0080A\35\u02b1\u02b2\7\27\2\2\u02b2\u02c4\5\u0080A\34\u02b3\u02b4"
                    + "\7\30\2\2\u02b4\u02c4\5\u0080A\33\u02b5\u02c4\7Q\2\2\u02b6\u02c4\7v\2"
                    + "\2\u02b7\u02c4\7e\2\2\u02b8\u02c4\5\u0088E\2\u02b9\u02c4\5n8\2\u02ba\u02c4"
                    + "\5t;\2\u02bb\u02bc\7\b\2\2\u02bc\u02bd\5~@\2\u02bd\u02be\7\t\2\2\u02be"
                    + "\u02c4\3\2\2\2\u02bf\u02c0\5\u0082B\2\u02c0\u02c1\7\67\2\2\u02c1\u02c2"
                    + "\5\u0084C\2\u02c2\u02c4\3\2\2\2\u02c3\u02a1\3\2\2\2\u02c3\u02a7\3\2\2"
                    + "\2\u02c3\u02a9\3\2\2\2\u02c3\u02ab\3\2\2\2\u02c3\u02ad\3\2\2\2\u02c3\u02af"
                    + "\3\2\2\2\u02c3\u02b1\3\2\2\2\u02c3\u02b3\3\2\2\2\u02c3\u02b5\3\2\2\2\u02c3"
                    + "\u02b6\3\2\2\2\u02c3\u02b7\3\2\2\2\u02c3\u02b8\3\2\2\2\u02c3\u02b9\3\2"
                    + "\2\2\u02c3\u02ba\3\2\2\2\u02c3\u02bb\3\2\2\2\u02c3\u02bf\3\2\2\2\u02c4"
                    + "\u030a\3\2\2\2\u02c5\u02c6\f\32\2\2\u02c6\u02c7\t\6\2\2\u02c7\u0309\5"
                    + "\u0080A\33\u02c8\u02c9\f\31\2\2\u02c9\u02ca\t\7\2\2\u02ca\u0309\5\u0080"
                    + "A\32\u02cb\u02cc\f\30\2\2\u02cc\u02cd\t\b\2\2\u02cd\u0309\5\u0080A\31"
                    + "\u02ce\u02cf\f\27\2\2\u02cf\u02d0\t\t\2\2\u02d0\u0309\5\u0080A\30\u02d1"
                    + "\u02d2\f\26\2\2\u02d2\u02d3\7A\2\2\u02d3\u0309\5\u0080A\27\u02d4\u02d5"
                    + "\f\25\2\2\u02d5\u02d6\7W\2\2\u02d6\u0309\5\u0080A\26\u02d7\u02d8\f\24"
                    + "\2\2\u02d8\u02d9\t\n\2\2\u02d9\u0309\5\u0080A\25\u02da\u02db\f\23\2\2"
                    + "\u02db\u02dc\7\'\2\2\u02dc\u0309\5\u0080A\24\u02dd\u02de\f\22\2\2\u02de"
                    + "\u02df\7(\2\2\u02df\u0309\5\u0080A\23\u02e0\u02e1\f\21\2\2\u02e1\u02e2"
                    + "\7)\2\2\u02e2\u0309\5\u0080A\22\u02e3\u02e4\f\20\2\2\u02e4\u02e5\7*\2"
                    + "\2\u02e5\u0309\5\u0080A\21\u02e6\u02e7\f\17\2\2\u02e7\u02e8\7+\2\2\u02e8"
                    + "\u0309\5\u0080A\20\u02e9\u02ea\f\16\2\2\u02ea\u02eb\7\17\2\2\u02eb\u02ec"
                    + "\5\u0080A\2\u02ec\u02ed\7\20\2\2\u02ed\u02ee\5\u0080A\17\u02ee\u0309\3"
                    + "\2\2\2\u02ef\u02f0\f\r\2\2\u02f0\u02f1\7\16\2\2\u02f1\u0309\5\u0080A\16"
                    + "\u02f2\u02f3\f\f\2\2\u02f3\u02f4\5\u0086D\2\u02f4\u02f5\5\u0080A\r\u02f5"
                    + "\u0309\3\2\2\2\u02f6\u02f7\f\'\2\2\u02f7\u02f8\7\6\2\2\u02f8\u02f9\5~"
                    + "@\2\u02f9\u02fa\7\7\2\2\u02fa\u0309\3\2\2\2\u02fb\u02fc\f&\2\2\u02fc\u02fd"
                    + "\7\22\2\2\u02fd\u0309\5\u008cG\2\u02fe\u02ff\f%\2\2\u02ff\u0309\5z>\2"
                    + "\u0300\u0301\f#\2\2\u0301\u0302\6A\34\2\u0302\u0309\7\23\2\2\u0303\u0304"
                    + "\f\"\2\2\u0304\u0305\6A\36\2\u0305\u0309\7\24\2\2\u0306\u0307\f\13\2\2"
                    + "\u0307\u0309\7x\2\2\u0308\u02c5\3\2\2\2\u0308\u02c8\3\2\2\2\u0308\u02cb"
                    + "\3\2\2\2\u0308\u02ce\3\2\2\2\u0308\u02d1\3\2\2\2\u0308\u02d4\3\2\2\2\u0308"
                    + "\u02d7\3\2\2\2\u0308\u02da\3\2\2\2\u0308\u02dd\3\2\2\2\u0308\u02e0\3\2"
                    + "\2\2\u0308\u02e3\3\2\2\2\u0308\u02e6\3\2\2\2\u0308\u02e9\3\2\2\2\u0308"
                    + "\u02ef\3\2\2\2\u0308\u02f2\3\2\2\2\u0308\u02f6\3\2\2\2\u0308\u02fb\3\2"
                    + "\2\2\u0308\u02fe\3\2\2\2\u0308\u0300\3\2\2\2\u0308\u0303\3\2\2\2\u0308"
                    + "\u0306\3\2\2\2\u0309\u030c\3\2\2\2\u030a\u0308\3\2\2\2\u030a\u030b\3\2"
                    + "\2\2\u030b\u0081\3\2\2\2\u030c\u030a\3\2\2\2\u030d\u0314\7v\2\2\u030e"
                    + "\u0310\7\b\2\2\u030f\u0311\5d\63\2\u0310\u030f\3\2\2\2\u0310\u0311\3\2"
                    + "\2\2\u0311\u0312\3\2\2\2\u0312\u0314\7\t\2\2\u0313\u030d\3\2\2\2\u0313"
                    + "\u030e\3\2\2\2\u0314\u0083\3\2\2\2\u0315\u031b\5\u0080A\2\u0316\u0317"
                    + "\7\n\2\2\u0317\u0318\5j\66\2\u0318\u0319\7\13\2\2\u0319\u031b\3\2\2\2"
                    + "\u031a\u0315\3\2\2\2\u031a\u0316\3\2\2\2\u031b\u0085\3\2\2\2\u031c\u031d"
                    + "\t\13\2\2\u031d\u0087\3\2\2\2\u031e\u0325\78\2\2\u031f\u0325\79\2\2\u0320"
                    + "\u0325\7w\2\2\u0321\u0325\7x\2\2\u0322\u0325\7\5\2\2\u0323\u0325\5\u008a"
                    + "F\2\u0324\u031e\3\2\2\2\u0324\u031f\3\2\2\2\u0324\u0320\3\2\2\2\u0324"
                    + "\u0321\3\2\2\2\u0324\u0322\3\2\2\2\u0324\u0323\3\2\2\2\u0325\u0089\3\2"
                    + "\2\2\u0326\u0327\t\f\2\2\u0327\u008b\3\2\2\2\u0328\u032b\7v\2\2\u0329"
                    + "\u032b\5\u008eH\2\u032a\u0328\3\2\2\2\u032a\u0329\3\2\2\2\u032b\u008d"
                    + "\3\2\2\2\u032c\u0330\5\u0090I\2\u032d\u0330\78\2\2\u032e\u0330\79\2\2"
                    + "\u032f\u032c\3\2\2\2\u032f\u032d\3\2\2\2\u032f\u032e\3\2\2\2\u0330\u008f"
                    + "\3\2\2\2\u0331\u0332\t\r\2\2\u0332\u0091\3\2\2\2\u0333\u0338\7\f\2\2\u0334"
                    + "\u0338\7\2\2\3\u0335\u0338\6J \2\u0336\u0338\6J!\2\u0337\u0333\3\2\2\2"
                    + "\u0337\u0334\3\2\2\2\u0337\u0335\3\2\2\2\u0337\u0336\3\2\2\2\u0338\u0093"
                    + "\3\2\2\2U\u0095\u009a\u00a2\u00a9\u00af\u00b8\u00be\u00c5\u00c8\u00ce"
                    + "\u00d5\u00db\u00e0\u00e4\u00ef\u00ff\u010c\u0110\u0117\u012a\u0130\u013f"
                    + "\u0152\u0156\u015a\u0164\u0168\u0173\u0180\u0186\u018d\u0194\u019b\u01ad"
                    + "\u01b1\u01b3\u01ba\u01c0\u01c5\u01d0\u01d3\u01e2\u01e5\u01eb\u01ef\u01fb"
                    + "\u0201\u0207\u020e\u021a\u021f\u0224\u0229\u022f\u0234\u023a\u023e\u0243"
                    + "\u024c\u0251\u0257\u025a\u025d\u0268\u026b\u026e\u027d\u0282\u028a\u028f"
                    + "\u0292\u029e\u02a5\u02c3\u0308\u030a\u0310\u0313\u031a\u0324\u032a\u032f"
                    + "\u0337";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}
