package org.bdware.sc.common;

/*
 * Local Log DB
 */
public class Transaction {
    String key;
    String from; // pubKey
    String to; // action
    String data;
    String requestID; // date

    public String getKey() {
        return this.key;
    }

    public String getFrom() {
        return this.from;
    }

    public String getData() {
        return this.data;
    }
}
