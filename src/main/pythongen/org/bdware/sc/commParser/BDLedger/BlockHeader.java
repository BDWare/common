package org.bdware.sc.commParser.BDLedger;

public class BlockHeader {
    public int index;
    public byte[] hash;
    public int version;
    public int timestamp;
    public byte[] prevblockID;
    public byte[] merkleroot;
    public byte[] creatorID;
}
