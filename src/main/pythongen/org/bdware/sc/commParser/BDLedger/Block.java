package org.bdware.sc.commParser.BDLedger;

import org.bdware.sc.util.HashUtil;
import org.bdware.sc.util.JsonUtil;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Block {
    public BlockHeader blockheader;
    public BlockBody blockbody;

    public Block(BlockHeader blockheader, BlockBody blockbody) {
        super();
        this.blockheader = blockheader;
        this.blockbody = blockbody;
        System.out.println(this.blockbody);
    }

    public byte[] toByteArray() {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            DataOutputStream dd = new DataOutputStream(bo);
            dd.writeInt(blockheader.index);
            dd.write(blockheader.hash);
            dd.writeInt(blockheader.timestamp);
            dd.writeInt(blockheader.version);
            dd.write(blockheader.prevblockID);
            dd.write(blockheader.merkleroot);
            dd.write(blockheader.creatorID);
            byte[] bytearr = blockbody.toByteArray();
            dd.writeInt(bytearr.length);
            dd.write(bytearr);
            return bo.toByteArray();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    // public static Block fromByteArray(byte[] block) {
    // BlockHeader header = new BlockHeader();
    // BlockBody body = new BlockBody();
    // try {
    // ByteArrayInputStream bi = new ByteArrayInputStream(block);
    // DataInputStream dd = new DataInputStream(bi);
    // header.index = dd.readInt();
    // header.hash = new byte[20];
    // dd.read(header.hash);
    // header.timestamp = dd.readInt();
    // header.version = dd.readInt();
    // header.prevblockID = new byte[20];
    // header.merkleroot = new byte[20];
    // header.creatorID = new byte[20];
    // dd.read(header.prevblockID);
    // dd.read(header.merkleroot);
    // dd.read(header.creatorID);
    // int bodysize = dd.readInt();
    // byte[] bodyarr = new byte[bodysize];
    // dd.read(bodyarr);
    // body = BlockBody.fromBytes(bodyarr);
    // Block genblock = new Block(header,body);
    // return genblock;
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // return null;
    // }
    private String[] getPrevBlockID(byte[] frombytes) {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(frombytes));
        String[] prevBlockID = new String[3];
        byte[] b1 = new byte[20];
        byte[] b2 = new byte[20];
        byte[] b3 = new byte[20];
        try {
            in.read(b1);
            in.read(b2);
            in.read(b3);
        } catch (IOException e) {
            e.printStackTrace();
        }
        prevBlockID[0] = HashUtil.byteArray2Str(b1);
        prevBlockID[1] = HashUtil.byteArray2Str(b2);
        prevBlockID[2] = HashUtil.byteArray2Str(b3);
        return prevBlockID;

    }

    public String toJson() {
        Map<String, String> ret = new HashMap<>();
        ret.put("Index", String.valueOf(blockheader.index));
        ret.put("hash", HashUtil.byteArray2Str(blockheader.hash));
        ret.put("timestamp", String.valueOf(blockheader.timestamp));
        ret.put("version", String.valueOf(blockheader.version));
        String[] prevBlockID = getPrevBlockID(blockheader.prevblockID);
        ret.put("prevblockID", prevBlockID[0] + "," + prevBlockID[1] + "," + prevBlockID[2]);
        ret.put("merkleroot", HashUtil.byteArray2Str(blockheader.merkleroot));
        ret.put("creatorID", HashUtil.byteArray2Str(blockheader.creatorID));
        return JsonUtil.toJson(ret);
    }

}
