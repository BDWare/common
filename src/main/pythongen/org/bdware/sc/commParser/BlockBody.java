package org.bdware.sc.commParser;

import org.bdware.sc.util.HashUtil;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.List;

public class BlockBody {
    public List<Transaction_bak> trans = new ArrayList<>();

    public static BlockBody fromBytes(byte[] input) {
        try {
            DataInputStream bin = new DataInputStream(new ByteArrayInputStream(input));
            int size = bin.readInt();
            size = HashUtil.toLittleEndian(size);
            BlockBody ret = new BlockBody();
            for (int i = 0; i < size; i++) {
                Transaction_bak trans = new Transaction_bak();
                ret.trans.add(trans);
                trans.srcID = new byte[20];
                trans.dstID = new byte[20];
                bin.read(trans.srcID);
                bin.read(trans.dstID);
                trans.nonce = HashUtil.toLittleEndian(bin.readLong());
                trans.type = HashUtil.toLittleEndian(bin.readInt());
                int len = HashUtil.toLittleEndian(bin.readInt());
                trans.data = new byte[len];
                bin.read(trans.data);
            }
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BlockBody();
    }
}
