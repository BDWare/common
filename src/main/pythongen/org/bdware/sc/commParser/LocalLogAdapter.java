package org.bdware.sc.commParser;

public class LocalLogAdapter {
    public Transaction trans;
    public org.bdware.sc.common.Transaction chaintran;

    public LocalLogAdapter(org.bdware.sc.common.Transaction chaintrans) {
        super();
        this.chaintran = chaintrans;
        this.trans = new Transaction();


        this.trans.Data = chaintrans.getData().getBytes();
        this.trans.SrcID = chaintrans.getKey().getBytes();
        this.trans.Txid = chaintrans.getData().getBytes(); // 交易id对应本地日志库中什么字段呢


        // this.trans.Data = chaintrans.getData().toStringUtf8().getBytes();
        // this.trans.SrcID = chaintrans.getFrom().toByteArray(); //本地库中SrcID可以是key
        // this.trans.Txid = chaintrans.getHash().toByteArray();
    }
}
