package org.bdware.sc.commParser;

public interface BlockAdapter<T> {
    Transaction convert(T t);
}
