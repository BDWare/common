package org.bdware.sc.commParser;

public class Transaction {
    public byte[] Txid;
    public byte[] SrcID;
    public byte[] DstID;
    public byte[] Data;
    public int Timestamp;
    public long Nonce;
}
