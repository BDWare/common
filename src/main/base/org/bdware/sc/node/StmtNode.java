package org.bdware.sc.node;

import org.antlr.v4.runtime.Token;

public class StmtNode {
    int line;
    int pos;

    public StmtNode setLineAndPos(Token l) {
        line = l.getLine();
        pos = l.getCharPositionInLine();
        return this;
    }

}
