package org.bdware.sc.node;

import org.bdware.sc.parser.YJSParser.ImportStmtContext;

public class ImportNode {
    String path;
    int line;

    public static ImportNode createFromCtx(ImportStmtContext importStmt) {
        ImportNode ret = new ImportNode();
        ret.path = importStmt.StringLiteral().getText().replaceAll("\"", "");
        ret.line = importStmt.start.getLine();
        return ret;
    }

    public String getPath() {
        if (path.startsWith("@"))
            return path.replaceAll("@", "/.depyjs/");
        return path;
    }
}
