package org.bdware.sc.node;

public enum LogType {
    Arg, Branch, IO, Result, Code;

    public static LogType parse(String str) {
        try {
            str = str.replaceAll("\"", "");
            return LogType.valueOf(str);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }
}
