package org.bdware.sc.node;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.Interval;

public class Script {
    int line, pos;
    String plainText;
    private Interval interval;

    public String plainText() {
        return plainText;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int charPositionInLine) {
        this.pos = charPositionInLine;
    }

    public void setInterval(Interval intraval) {
        this.interval = intraval;
    }

    public void initText(CommonTokenStream cts) {
        plainText = cts.getText(interval);
    }

    public void initTextWithCleaning(CommonTokenStream cts, boolean isExported, boolean isView) {
        String plainText = cts.getText(interval);
        if (isExported) {
            plainText = plainText.replaceFirst("exported", "");
        }
        if (isView) {
            plainText = plainText.replaceFirst("\\)\\s*view\\s*\\{", "){");
        }
        this.plainText = plainText;
    }
}
