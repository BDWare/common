package org.bdware.sc.node;

import org.bdware.sc.JSEngine;

public interface AnnotationHook {
    ArgPacks handle(JSEngine desktopEngine, ArgPacks argPacks);
}
