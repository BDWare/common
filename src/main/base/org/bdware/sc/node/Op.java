package org.bdware.sc.node;

public enum Op {
    Goto, Return, Move, STUB, Label, Ifnez
}
