package org.bdware.sc.node;

import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Map;

public class MaskConfig {
    public Map<String, JsonElement> config = new HashMap<>();
    public String version;

    public Map<String, JsonElement> getConfig() {
        return config;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String v) {
        this.version = v;
    }
}
