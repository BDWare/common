package org.bdware.sc.node;

import java.util.HashMap;
import java.util.Map;

public class ContractZipBundle {
    ContractManifest cm;
    Map<String, ContractNode> path2Node;
    private ContractNode mergedContractNode;

    public ContractZipBundle() {
        path2Node = new HashMap<>();
    }

    public boolean containsPath(String path) {
        return path2Node.containsKey(path);
    }

    public void put(String path, ContractNode cn) {
        path2Node.put(path, cn);
    }

    public ContractManifest getManifest() {
        return cm;
    }

    public void setManifest(ContractManifest cm) {
        this.cm = cm;
    }

    public ContractNode mergeContractNode() {
        return mergedContractNode;
    }

    public void setMergedContractNode() {
        ContractNode cn = new ContractNode(null);
        cn.memorySet = cm.memory;
        cn.setIsBundle(true);
        ContractNode mainNode = path2Node.get(cm.main);
        cn.contractName = mainNode.contractName;
        for (String key : path2Node.keySet()) {
            ContractNode contract = path2Node.get(key);
            if (key.equals(cm.main)) {
                cn.setYjsType(contract.getYjsType());
            }
            cn.merge(contract);
        }
        cn.mergeInterfaceAnnotationIntoFunction();
        this.mergedContractNode = cn;
    }
}
