package org.bdware.sc.node;

import com.google.gson.JsonElement;
import org.bdware.sc.bean.ContractExecType;
import org.bdware.sc.py.bean.PYPackage;

import java.util.List;

public class ContractManifest {
    public String main;
    public String memory;
    public String doi;
    public String stateful;
    public long insnLimit;
    public long buildTime;
    public String builder;
    private ContractExecType type;
    private List<PYPackage> pyDependences;
    private List<Permission> permissions;
    public boolean startAtUnpack;
    public String sourcePath;
    public JsonElement createParam;

    public List<PYPackage> getPyDependences() {
        return pyDependences;
    }

    public void setPyDependences(List<PYPackage> pyDependences) {
        this.pyDependences = pyDependences;
    }

    public ContractExecType getType() {
        return type;
    }

    public void setType(ContractExecType type) {
        this.type = type;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public long getInsnLimit() {
        return insnLimit;
    }

    public void setInsnLimit(long insnLimit) {
        this.insnLimit = insnLimit;
    }
}
