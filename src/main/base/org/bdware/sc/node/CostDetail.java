package org.bdware.sc.node;

public class CostDetail {

    private boolean countGas;
    private String extraGas;

    public CostDetail() {
        countGas = false;
        extraGas = null;
    }

    public boolean isCountGas() {
        return countGas;
    }

    public void setCountGas(boolean countGas) {
        this.countGas = countGas;
    }

    public String getExtraGas() {
        return extraGas;
    }

    public void setExtraGas(String extraGas) {
        this.extraGas = extraGas;
    }
}
