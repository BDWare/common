package org.bdware.sc.node.stmt;

import org.bdware.sc.node.Op;
import org.bdware.sc.node.StmtNode;

public class BranchStmt extends StmtNode {
    String reg;
    LabelStmt target;
    Op op = Op.Ifnez;

    // if (reg!=0)...else...
    public BranchStmt setReg(String name) {
        reg = name;
        return this;
    }

    public BranchStmt setTarget(LabelStmt end) {
        target = end;
        return this;
    }

}
