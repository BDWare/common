package org.bdware.sc.node.stmt;

import org.bdware.sc.node.Op;
import org.bdware.sc.node.StmtNode;

public class GotoStmt extends StmtNode {
    LabelStmt label;
    Op op = Op.Goto;

    public GotoStmt setTarget(LabelStmt end) {
        label = end;
        return this;
    }

}
