package org.bdware.sc.node.stmt;

import org.bdware.sc.node.Op;
import org.bdware.sc.node.StmtNode;

public class Stmt2N extends StmtNode {
    String from;
    String to;
    Op op;

    public Stmt2N(Op op) {
        this.op = op;
    }

    public void setFrom(String name) {
        from = name;
    }

    public void setTo(String name) {
        to = name;
    }

}
