package org.bdware.sc.node.stmt;

import org.bdware.sc.node.Op;
import org.bdware.sc.node.StmtNode;

public class LabelStmt extends StmtNode {
    static int globalID = 0;
    Op op = Op.Label;
    int id = globalID++;
}
