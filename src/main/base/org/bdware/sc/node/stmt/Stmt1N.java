package org.bdware.sc.node.stmt;

import org.bdware.sc.node.Op;
import org.bdware.sc.node.StmtNode;

public class Stmt1N extends StmtNode {
    Op op;
    String reg;

    public Stmt1N(Op op, String reg) {
        this.op = op;
        this.reg = reg;
    }
}
