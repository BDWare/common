package org.bdware.sc.node;

import org.bdware.sc.bean.DoipOperationInfo;
import org.bdware.sc.bean.ForkInfo;
import org.bdware.sc.bean.JoinInfo;
import org.bdware.sc.bean.RouteInfo;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class InterfaceNode extends Script {
    private final List<AnnotationHook> beforeInvoke;
    private final List<AnnotationHook> afterInvoke;
    public String functionName;
    public List<AnnotationNode> annotations;
    boolean isStatic;
    String fileName;
    EnumSet<LogType> logTypes;
    // boolean logToChain;
    LogLocation logLocation = new LogLocation();
    private RouteInfo routeInfo;
    private JoinInfo joinInfo;
    private DoipOperationInfo doipOperationInfo;
    private boolean isHandler;
    private boolean isDoipOperation;

    public InterfaceNode(String name, String fileName) {
        this.functionName = name;
        this.fileName = fileName;
        this.annotations = new ArrayList<>();
        this.logTypes = EnumSet.noneOf(LogType.class);
        beforeInvoke = new ArrayList<>();
        afterInvoke = new ArrayList<>();
    }

    public DoipOperationInfo getDoipOperationInfo() {
        return doipOperationInfo;
    }

    public void setDoipOperationInfo(DoipOperationInfo doipOperationInfo) {
        this.doipOperationInfo = doipOperationInfo;
    }

    public void setIsDoipOperation(boolean doipOperation) {
        isDoipOperation = doipOperation;
    }


    public boolean isDoipOperation() {
        return isDoipOperation;
    }


    public boolean isHandler() {
        return isHandler;
    }

    public void setHandler(boolean handler) {
        isHandler = handler;
    }


    public String getFileName() {
        return fileName;
    }

    public void addAnnotation(AnnotationNode annNode) {
        annotations.add(annNode);
    }

    public void addLogType(LogType v) {
        if (v == null) {
            return;
        }
        logTypes.add(v);
    }

    public EnumSet<LogType> getLogTypes() {
        return logTypes;
    }

    public boolean getLogToBDContract() {
        return logLocation.logToBDContract;
    }

    public void setLogToBDContract(boolean b) {
        logLocation.logToBDContract = b;
    }

    public boolean getLogToNamedLedger() {
        return logLocation.logToNamedLedger;
    }

    public void setLogToNamedLedger(boolean b) {
        logLocation.logToNamedLedger = b;
    }

    public void addLedgerName(String name) {
        if (logLocation.ledgerNames == null) {
            logLocation.ledgerNames = new ArrayList<>();
        }
        logLocation.ledgerNames.add(name);
    }

    public List<String> getLedgerNames() {
        return logLocation.ledgerNames;
    }

    public void appendAfterInvokeHandler(AnnotationHook handler) {
        afterInvoke.add(handler);
    }

    public List<AnnotationHook> afterExecutionAnnotations() {
        return afterInvoke;
    }

    public void appendBeforeInvokeHandler(AnnotationHook handler) {
        beforeInvoke.add(handler);
    }

    public List<AnnotationHook> beforeExecutionAnnotations() {
        return beforeInvoke;
    }

    public RouteInfo getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(RouteInfo routeInfo) {
        this.routeInfo = routeInfo;
    }


    public JoinInfo getJoinInfo() {
        return this.joinInfo;
    }

    public void setJoinInfo(JoinInfo joinInfo1) {
        this.joinInfo = joinInfo1;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public AnnotationNode getAnnotation(String annotationName) {
        for (AnnotationNode node : annotations)
            if (node.getType() != null && node.getType().equals(annotationName))
                return node;
        return null;
    }

    private ForkInfo forkInfo;

    public void setForkInfo(ForkInfo forkInfo) {
        this.forkInfo = forkInfo;
    }

    public ForkInfo getForkInfo() {
        return forkInfo;
    }
}
