package org.bdware.sc.node;

import java.util.ArrayList;
import java.util.List;

public enum Permission {
    Http, SQL, File, RocksDB, MongoDB, BDWareTimeSeriesDB, MultiTagIndexDB, SM2, AES, Ledger, CM, DOIP, IRP, Async, Cluster, Digest;

    public static Permission parse(String str) {
        try {
            str = str.replaceAll("\"", "");
            return Permission.valueOf(str);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> allName() {
        List<String> allPermission = new ArrayList<>();
        for (Permission per : Permission.values()) {
            allPermission.add(per.name());
        }
        return allPermission;
    }

    public String clzName() {
        return name() + "Util";
    }
}
