package org.bdware.sc.node;

public class ContractLogDetail {
    public boolean logToChain;
    public boolean logArg;
    public boolean logBranch;
    public boolean logResult;

    public ContractLogDetail() {
        this.logArg = false;
        this.logBranch = false;
        this.logResult = false;
        this.logToChain = false;
    }
}
