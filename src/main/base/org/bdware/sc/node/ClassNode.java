package org.bdware.sc.node;

import java.util.ArrayList;
import java.util.List;

public class ClassNode extends Script {
    String clzName;
    String fileName;
    List<FunctionNode> functions;

    public ClassNode(String clzName, String fileName) {
        this.clzName = clzName;
        this.fileName = fileName;
        functions = new ArrayList<>();
    }

    public void addFunction(FunctionNode function) {
        functions.add(function);
    }

    public String getFileName() {
        return fileName;
    }

}
