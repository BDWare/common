package org.bdware.sc.node;

import java.util.List;

public class SharableNode {
    private List<String> variableStatements;

    private String fileName;

    public List<String> getVariableStatements() {
        return variableStatements;
    }

    public void setVariableStatements(List<String> variableStatements) {
        this.variableStatements = variableStatements;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
