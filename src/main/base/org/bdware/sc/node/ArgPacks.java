package org.bdware.sc.node;

import org.bdware.sc.bean.ContractRequest;

public class ArgPacks {
    public Object ret;
    public ContractRequest request;
    public Object arg;

    public ArgPacks(ContractRequest request, Object arg, Object ret) {
        this.request = request;
        this.arg = arg;
        this.ret = ret;
    }
}
