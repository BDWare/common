package org.bdware.sc.node;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AnnotationNode implements Serializable {
    String type;
    List<String> args;

    public AnnotationNode(String type) {
        this.type = type;
        args = new ArrayList<>();
    }

    public void addArg(String arg) {
        args.add(arg);
    }

    public String getType() {
        return type;
    }

    public List<String> getArgs() {
        return args;
    }

}
