package org.bdware.sc.py.utils;

/**
 * Created by mengliang on 2018/9/26.
 */
public class HttpExt {

    public static String getModuleName(String classFullName) {
        String[] sArrary = classFullName.split("\\.");
        int len = sArrary.length;
        StringBuilder moduleName;
        if (len > 1) {
            moduleName = new StringBuilder(sArrary[0]);
            for (int i = 1; i < len - 1; i++) {
                moduleName.append(".").append(sArrary[i]);
            }
            return moduleName.toString();
        }
        return "";
    }

    public static String getPackageName(String moduleName) {
        String[] sArrary = moduleName.split("\\.");
        int len = sArrary.length;
        if (len > 0) {
            return sArrary[0];
        }
        return "";
    }

    public static String getClassName(String classFullName) {
        String[] sArrary = classFullName.split("\\.");
        int len = sArrary.length;
        if (len > 0) {
            return sArrary[len - 1];
        }
        return "";
    }

    public static String getMethodName(String methodFullName) {
        String[] sArrary = methodFullName.split("\\.");
        int len = sArrary.length;
        if (len > 0) {
            return sArrary[len - 1];
        }
        return "";
    }

    public static String getMethodClassFullName(String methodFullName) {
        String[] sArrary = methodFullName.split("\\.");
        int len = sArrary.length;
        StringBuilder classFullName;
        if (len > 1) {
            classFullName = new StringBuilder(sArrary[0]);
            for (int i = 1; i < len - 1; i++) {
                classFullName.append(".").append(sArrary[i]);
            }
            return classFullName.toString();
        }
        return "";
    }


}
