package org.bdware.sc.py.bean;

/**
 * Created by mengliang on 2018/9/25.
 */
public class PYYjsParams {
    String package_name;
    String module_name;
    String moduleFullName;
    String module_class_name;
    String module_func_name;
    String module_class_classmethod_name;
    String module_class_func_name;
    String module_class_staticmethod_name;
    String objectId;
    String object_method_name;
    PYMethodParams params;

    public String getObject_method_name() {
        return object_method_name;
    }

    public void setObject_method_name(String object_method_name) {
        this.object_method_name = object_method_name;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getModuleFullName() {
        return moduleFullName;
    }

    public void setModuleFullName(String moduleFullName) {
        this.moduleFullName = moduleFullName;
    }

    public String getModule_class_name() {
        return module_class_name;
    }

    public void setModule_class_name(String module_class_name) {
        this.module_class_name = module_class_name;
    }

    public String getModule_func_name() {
        return module_func_name;
    }

    public void setModule_func_name(String module_func_name) {
        this.module_func_name = module_func_name;
    }

    public String getModule_class_classmethod_name() {
        return module_class_classmethod_name;
    }

    public void setModule_class_classmethod_name(String module_class_classmethod_name) {
        this.module_class_classmethod_name = module_class_classmethod_name;
    }

    public String getModule_class_func_name() {
        return module_class_func_name;
    }

    public void setModule_class_func_name(String module_class_func_name) {
        this.module_class_func_name = module_class_func_name;
    }

    public String getModule_class_staticmethod_name() {
        return module_class_staticmethod_name;
    }

    public void setModule_class_staticmethod_name(String module_class_staticmethod_name) {
        this.module_class_staticmethod_name = module_class_staticmethod_name;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public PYMethodParams getParams() {
        return params;
    }

    public void setParams(PYMethodParams params) {
        this.params = params;
    }
}
