package org.bdware.sc.py.bean;

import com.google.gson.JsonObject;

import java.util.Map;

/**
 * Created by mengliang on 2018/9/24.
 */
public class PYYjsResponse {
    String yjsJsonRpc;
    String yjsRespnseId;
    org.bdware.sc.py.bean.PYYjsError PYYjsError;
    Map<String, JsonObject> yjsResult;

    public String getYjsJsonRpc() {
        return yjsJsonRpc;
    }

    public void setYjsJsonRpc(String yjsJsonRpc) {
        this.yjsJsonRpc = yjsJsonRpc;
    }

    public String getYjsRespnseId() {
        return yjsRespnseId;
    }

    public void setYjsRespnseId(String yjsRespnseId) {
        this.yjsRespnseId = yjsRespnseId;
    }

    public org.bdware.sc.py.bean.PYYjsError getPYYjsError() {
        return PYYjsError;
    }

    public void setPYYjsError(org.bdware.sc.py.bean.PYYjsError PYYjsError) {
        this.PYYjsError = PYYjsError;
    }

    public Map<String, JsonObject> getYjsResult() {
        return yjsResult;
    }

    public void setYjsResult(Map<String, JsonObject> yjsResult) {
        this.yjsResult = yjsResult;
    }
}
