package org.bdware.sc.py.bean;

/**
 * Created by mengliang on 2018/9/25.
 */
public class PYYjsError {
    Integer code;
    String message;
    String data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
