package org.bdware.sc.py.bean;

import java.util.List;

public class PYClass {
    String name;
    String sig;
    List<PYMethod> methods;
    List<PYMethod> funcs;

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public List<PYMethod> getFuncs() {
        return funcs;
    }

    public void setFuncs(List<PYMethod> funcs) {
        this.funcs = funcs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PYMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<PYMethod> methods) {
        this.methods = methods;
    }
}
