package org.bdware.sc.py.bean;

public class PYMethod {
    String name;
    String sig;

    boolean isClassMethod;
    boolean isClassFunction; // python 除ClassMethod修饰的函数
    boolean isModuleMethod;
    boolean isObjectMethod;

    public Boolean getClassMethod() {
        return isClassMethod;
    }

    public void setClassMethod(Boolean classMethod) {
        isClassMethod = classMethod;
    }

    public Boolean getClassFunction() {
        return isClassFunction;
    }

    public void setClassFunction(Boolean classFunction) {
        isClassFunction = classFunction;
    }

    public Boolean getModuleMethod() {
        return isModuleMethod;
    }

    public void setModuleMethod(Boolean moduleMethod) {
        isModuleMethod = moduleMethod;
    }

    public Boolean getObjectMethod() {
        return isObjectMethod;
    }

    public void setObjectMethod(Boolean objectMethod) {
        isObjectMethod = objectMethod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    // TODO 1 mengliang check
    public boolean isStatic() {
        return isClassMethod || isModuleMethod;
    }
}
