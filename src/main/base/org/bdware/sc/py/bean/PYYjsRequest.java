package org.bdware.sc.py.bean;

/**
 * Created by mengliang on 2018/9/26.
 */
public class PYYjsRequest {
    String yjsJsonRpc;
    String yjsRespnseId;
    PYYjsParams yjsParams;

    public String getYjsJsonRpc() {
        return yjsJsonRpc;
    }

    public void setYjsJsonRpc(String yjsJsonRpc) {
        this.yjsJsonRpc = yjsJsonRpc;
    }

    public String getYjsRespnseId() {
        return yjsRespnseId;
    }

    public void setYjsRespnseId(String yjsRespnseId) {
        this.yjsRespnseId = yjsRespnseId;
    }

    public PYYjsParams getYjsParams() {
        return yjsParams;
    }

    public void setYjsParams(PYYjsParams yjsParams) {
        this.yjsParams = yjsParams;
    }
}
