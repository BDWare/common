package org.bdware.sc.py.bean;

/**
 * Created by mengliang on 2018/9/25.
 */
public class PYMethodParams {
    Object params;

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }
}
