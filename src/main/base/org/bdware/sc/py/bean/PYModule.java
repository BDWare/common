package org.bdware.sc.py.bean;

import java.util.List;

public class PYModule {
    String name;
    List<PYClass> clzList;
    List<PYMethod> funcList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PYClass> getClzList() {
        return clzList;
    }

    public void setClzList(List<PYClass> clzList) {
        this.clzList = clzList;
    }

    public List<PYMethod> getFuncList() {
        return funcList;
    }

    public void setFuncList(List<PYMethod> funcList) {
        this.funcList = funcList;
    }
}
