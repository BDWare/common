package org.bdware.sc.py.bean;

import java.util.List;

public class PYPackage {
    String name;
    List<PYModule> modules;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PYModule> getModules() {
        return this.modules;
    }

    public void setModules(List<PYModule> modules) {
        this.modules = modules;
    }
}
