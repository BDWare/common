package org.bdware.sc.event;

import org.bdware.sc.event.REvent.REventSemantics;
import org.bdware.sc.util.JsonUtil;

public class Event {
    public String topic;
    public String content;
    public REventSemantics semantics;

    public Event(String topic, String content, REventSemantics semantics) {
        this.topic = topic;
        this.content = content;
        this.semantics = semantics;
        if (REventSemantics.NEED_RETRY.equals(this.semantics)) {
            this.semantics = REventSemantics.ONLY_ONCE;
        }
    }

    public String getAll() {
        return JsonUtil.toJson(this);
    }
}
