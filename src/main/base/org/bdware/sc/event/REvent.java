package org.bdware.sc.event;

import org.bdware.sc.bean.SM2Verifiable;

import java.io.Serializable;

/**
 * Events for requests
 */
public class REvent extends SM2Verifiable implements Serializable {
    private final REventType type;
    private final String topic;
    private final String content;
    private final String requestID;
    private String txHash;
    private String hash;
    private String sender;
    private REventSemantics semantics = REventSemantics.AT_LEAST_ONCE;
    private boolean forward = true;
    private String center;

    public REvent(String topic, REventType type, String content, String requestID) {
        this.topic = topic;
        this.type = type;
        this.content = content;
        this.requestID = requestID;
    }

    public String getTopic() {
        return topic;
    }

    public REventType getType() {
        return type;
    }

    public String getContent() {
        return content;
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getRequestID() {
        return requestID;
    }

    public REventSemantics getSemantics() {
        return semantics;
    }

    public void setSemantics(REventSemantics semantics) {
        this.semantics = semantics;
    }

    public boolean isForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public void doSignature(String pubKey, String privKey) {
        this.setPublicKey(pubKey);
        this.doSignature(privKey);
    }

    @Override
    public String getPublicKey() {
        return this.sender;
    }

    @Override
    public void setPublicKey(String pubkey) {
        this.sender = pubkey;
    }

    @Override
    public String getContentStr() {
        return String.format(
                "{\"topic\":\"%s\",\"content\":\"%s\",\"type\":\"%s\","
                        + "\"semantics\":\"%s\",\"requestID\":\"%s\"}",
                topic, content, type, semantics, requestID);
    }

    public enum REventType {
        SUBSCRIBE, UNSUBSCRIBE, PUBLISH, PRESUB, PREPUB
    }

    public enum REventSemantics {
        AT_LEAST_ONCE, AT_MOST_ONCE, ONLY_ONCE, NEED_RETRY
    }
}
