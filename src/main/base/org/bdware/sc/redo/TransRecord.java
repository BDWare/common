package org.bdware.sc.redo;


import com.google.gson.JsonElement;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TransRecord implements Serializable, Comparable<TransRecord> {
    private static final long serialVersionUID = 8797692289060677903L;

    boolean needSeq = false;
    int seq;
    String functionName;
    JsonElement arg;
    public Map<String, String> executes = new HashMap<String, String>(); // <innvokdeID,(flag1<seperate>flag<seperate>result)>

    public TransRecord(String s1, JsonElement s2, int sequence) {
        this.functionName = s1;
        this.arg = s2;
        seq = sequence;
        needSeq = true;
    }

    public TransRecord(String s1, JsonElement s2) {
        this.functionName = s1;
        this.arg = s2;
    }

    public String getFuncName() {
        return functionName;
    }

    public JsonElement getArg() {
        return arg;
    }

    public void addExecutes(String k, String v) {
        executes.put(k, v);
    }

    public int getSeq() {
        return seq;
    }

    public String getExecuteResult(String k) {
        return executes.get(k);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("===TransRecord===;" + needSeq + ";" + seq + ";" + functionName + ";" + arg);
        for (String k : executes.keySet()) {
            str.append("\n" + k + ";" + executes.get(k));
        }
        return str.toString();
    }

    @Override
    public int compareTo(TransRecord cr) {
        return (seq < cr.seq) ? -1 : ((seq == cr.seq) ? 0 : 1);
    }
}
