package org.bdware.sc.conn;

import java.io.*;

public class EventMsg implements Serializable {
    private static final long serialVersionUID = 5953407456061892534L;

    public String type, content, contractID;

    public EventMsg(String contractID, String type, String content) {
        this.contractID = contractID;
        this.type = type;
        this.content = content;
    }

    public static EventMsg parse(byte[] data) {
        ObjectInputStream in;
        try {
            in = new ObjectInputStream(new ByteArrayInputStream(data));
            return (EventMsg) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public byte[] getBytes() {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            oo.writeObject(this);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bo.toByteArray();
    }
}
