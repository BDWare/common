package org.bdware.sc.conn;

import com.google.gson.JsonObject;
import io.netty.channel.Channel;
import io.netty.util.Timeout;
import org.bdware.sc.util.JsonUtil;

import java.util.Map;

public abstract class ResultCallback {
    // private static final Logger LOGGER = LogManager.getLogger(ResultCallback.class);
    Timeout task;
    private Channel channel;

    public ResultCallback() {}

    public ResultCallback(Channel channel) {
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    public void onResult(Map object) {
        onResult(JsonUtil.toJson(object));
    }

    public void onResult(JsonObject jo) {
        onResult(jo.toString());
    }

    public abstract void onResult(String str);

    public void setTimeOut(Timeout task) {
        this.task = task;
    }

    public void cancelTimeOut() {
        // LOGGER.debug("[cancelTimeout]" + task.toString());
        if (null != this.task) {
            task.cancel();
        }
    }
}
