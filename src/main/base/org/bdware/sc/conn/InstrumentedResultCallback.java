package org.bdware.sc.conn;

import com.google.gson.JsonObject;

public interface InstrumentedResultCallback {
    void onResult(ResultCallback resultCallback, JsonObject result);
}
