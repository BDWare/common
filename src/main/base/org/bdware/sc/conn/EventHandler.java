package org.bdware.sc.conn;

import org.bdware.sc.event.REvent;

public interface EventHandler {
    void onEvent(REvent msg);
}
