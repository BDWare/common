package org.bdware.sc.conn;

public interface OnHashCallback {
    void publishHash(String reqID, String hashStr);
}
