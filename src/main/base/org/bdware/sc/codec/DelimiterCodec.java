package org.bdware.sc.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.CombinedChannelDuplexHandler;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

public class DelimiterCodec extends
        CombinedChannelDuplexHandler<DelimiterBasedFrameDecoder, DelimiterBasedFrameEncoder> {
    public DelimiterCodec() {
        ByteBuf buf = Unpooled.wrappedBuffer(DelimiterBasedFrameEncoder.delimiter);
        init(new DelimiterBasedFrameDecoder(50 * 1024, buf), new DelimiterBasedFrameEncoder());
    }
}
