package org.bdware.sc.codec;

import io.netty.channel.CombinedChannelDuplexHandler;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.ByteOrder;

public class LengthFieldBasedFrameCodec extends
        CombinedChannelDuplexHandler<LengthFieldBasedFrameDecoder, LengthFieldBasedFrameEncoder> {
    public LengthFieldBasedFrameCodec() {
        init(new LengthFieldBasedFrameDecoder(ByteOrder.BIG_ENDIAN, Integer.MAX_VALUE / 2, 0, 4, 0,
                4, true), new LengthFieldBasedFrameEncoder());
    }
}
