package org.bdware.sc.visitor;

import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.parser.YJSParser;
import org.bdware.sc.parser.YJSParserBaseVisitor;

public class ContractDependencyVisitor extends YJSParserBaseVisitor<FunctionNode> {

    private final ContractNode cn;
    private final FunctionNode fn;

    public ContractDependencyVisitor(ContractNode cn, FunctionNode fn) {
        this.cn = cn;
        this.fn = fn;
    }

    @Override
    public FunctionNode visitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx) {
        super.visitArgumentsExpression(ctx);
        if (ctx.singleExpression().getText().equals("executeContract")) {
            String contractName = ctx.arguments().singleExpression().get(0).getText();
            if (contractName.contains("\"")) {
                contractName = contractName.replace("\"", "");
                cn.addDependentContracts(contractName);
            }
            // ctx.arguments().singleExpression().get(1).getText();
        }
        return fn;
    }
}
