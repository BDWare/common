package org.bdware.sc.visitor;

import org.bdware.sc.node.ClassNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.parser.YJSParser;
import org.bdware.sc.parser.YJSParser.ClassElementContext;
import org.bdware.sc.parser.YJSParserBaseVisitor;

import java.util.List;

public class ClassReader extends YJSParserBaseVisitor<ClassNode> {
    ClassNode cn;
    String fileName;

    public ClassReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public ClassNode visitClassDeclaration(YJSParser.ClassDeclarationContext ctx) {
        cn = new ClassNode(ctx.Identifier().toString(), fileName);
        cn.setLine(ctx.start.getLine());
        cn.setPos(ctx.start.getCharPositionInLine());
        cn.setInterval(ctx.getSourceInterval());
        ctx.classTail().accept(this);
        return cn;
    }

    @Override
    public ClassNode visitClassTail(YJSParser.ClassTailContext ctx) {
        List<ClassElementContext> clzElements = ctx.classElement();
        if (clzElements != null)
            for (ClassElementContext clzElement : clzElements) {
                FunctionReader node = new FunctionReader(fileName);
                FunctionNode function = node.visitMethodDefinition(clzElement.methodDefinition());
                if (null != clzElement.Static()) {
                    function.setStatic(true);
                }
                cn.addFunction(function);
            }
        return cn;
    }
}
