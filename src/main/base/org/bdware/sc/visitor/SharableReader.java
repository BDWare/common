package org.bdware.sc.visitor;

import org.bdware.sc.node.SharableNode;
import org.bdware.sc.parser.YJSParser;
import org.bdware.sc.parser.YJSParserBaseVisitor;

import java.util.ArrayList;
import java.util.List;

public class SharableReader extends YJSParserBaseVisitor<SharableNode> {
    private final String fileName;

    public SharableReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public SharableNode visitVariableDeclarationList(YJSParser.VariableDeclarationListContext ctx) {
        SharableNode sharableNode = new SharableNode();
        List<String> statements = new ArrayList<>();
        for (YJSParser.VariableDeclarationContext variableDeclarationContext : ctx
                .variableDeclaration()) {
            statements.add(variableDeclarationContext.getText());
        }
        sharableNode.setVariableStatements(statements);
        sharableNode.setFileName(fileName);
        return sharableNode;
    }
}
