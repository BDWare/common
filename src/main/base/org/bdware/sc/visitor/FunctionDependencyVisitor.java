package org.bdware.sc.visitor;

import org.bdware.sc.node.ContractNode;
import org.bdware.sc.node.FunctionNode;
import org.bdware.sc.parser.YJSParser;
import org.bdware.sc.parser.YJSParserBaseVisitor;

import java.util.List;

public class FunctionDependencyVisitor extends YJSParserBaseVisitor<FunctionNode> {

    public static String APPLY = ".apply";
    public static String CALL = ".call";

    private final ContractNode cn;
    private final FunctionNode fn;

    public FunctionDependencyVisitor(ContractNode cn, FunctionNode fn) {
        this.cn = cn;
        this.fn = fn;
    }

    @Override
    public FunctionNode visitArgumentsExpression(YJSParser.ArgumentsExpressionContext ctx) {
        super.visitArgumentsExpression(ctx);
        String funText = ctx.singleExpression().getText();
        if (funText.endsWith(APPLY))
            funText = funText.replaceAll(APPLY + "$", "");
        if (funText.endsWith(CALL))
            funText = funText.replaceAll(CALL + "$", "");
        if (cn.getFunction(funText) != null)
            fn.addDependentFunctions(funText);
        if (ctx.arguments() != null) {
            List<YJSParser.SingleExpressionContext> singleExpress =
                    ctx.arguments().singleExpression();
            if (singleExpress != null && singleExpress.size() > 0) {
                for (YJSParser.SingleExpressionContext sin : singleExpress) {
                    funText = sin.getText();
                    if (cn.getFunction(funText) != null)
                        fn.addDependentFunctions(funText);
                }
            }
        }
        return fn;
    }

    @Override
    public FunctionNode visitNewExpression(YJSParser.NewExpressionContext ctx) {
        super.visitNewExpression(ctx);
        for (FunctionNode f : cn.getFunctions()) {
            if (f.functionName.equals(ctx.singleExpression().getText())) {
                fn.addDependentFunctions(f.functionName);
                break;
            }
        }
        return fn;
    }

}
