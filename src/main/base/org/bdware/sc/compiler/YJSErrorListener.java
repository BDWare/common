package org.bdware.sc.compiler;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class YJSErrorListener implements ANTLRErrorListener {
    List<String> result;

    public YJSErrorListener() {
        result = new ArrayList<>();
    }

    public List<String> getResultList() {
        return result;
    }

    @Override
    public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex,
            boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
        String content = ("SyntaxError Ambiguity, line" + recognizer.getCurrentToken().getLine()
                + " pos:" + recognizer.getCurrentToken().getCharPositionInLine());
        result.add(content);
    }

    @Override
    public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex,
            int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
        // System.out.println("YJSErrorListener AttemptingFullContext!");

    }

    @Override
    public void reportContextSensitivity(Parser arg0, DFA arg1, int arg2, int arg3, int arg4,
            ATNConfigSet arg5) {
        // System.out.println("YJSErrorListener ContextSensitivity!");

    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
            int charPos, String msg, RecognitionException e) {
        String content = "SyntaxError, line" + line + " pos:" + charPos + " msg:" + msg;
        result.add(content);
    }

}
