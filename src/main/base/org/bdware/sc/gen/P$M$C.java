package org.bdware.sc.gen;

import org.bdware.sc.py.PYEntry;
import org.bdware.sc.py.bean.PYMethodParams;
import org.bdware.sc.py.bean.PYObject;

public class P$M$C extends PYObject {

    public P$M$C(Object... params) {
        PYMethodParams paramObj = new PYMethodParams();
        paramObj.setParams(params);
        setID(PYEntry.instance.creatObject("P.M.C", paramObj));
    }

    public static String staticFunc(Object... params) {
        PYMethodParams paramObj = new PYMethodParams();
        paramObj.setParams(params);
        return PYEntry.instance.invokeClzStaticMethod("P.M.C" + "." + "staticFunc", paramObj);
    }

    public static String moduleFunc(Object... params) {
        PYMethodParams paramObj = new PYMethodParams();
        paramObj.setParams(params);
        return PYEntry.instance.invokeModuleFunc("P.M", "moduleFunc", paramObj);
    }

    public String sayHello(Object... params) {
        PYMethodParams paramObj = new PYMethodParams();
        paramObj.setParams(params);
        return PYEntry.instance.invokeObjectMethod(getID(), "sayHello", paramObj);
    }

    public String clzClassMethod(Object... params) {
        PYMethodParams paramObj = new PYMethodParams();
        paramObj.setParams(params);
        return PYEntry.instance.invokeClzClassMethod("P.M.C" + "." + "clzClassMethod", paramObj);
    }
}
