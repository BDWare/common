package org.bdware.sc;

import com.google.gson.JsonElement;
import org.bdware.sc.event.REvent;

import java.util.List;

public class ContractResult {
    public boolean needSeq = false;
    public int seq;
    public Status status;
    public JsonElement result;
    public String analysis;
    public boolean isInsnLimit;
    public String branch;
    public long totalGas = 0L;
    public long executionGas = 0L;
    public long extraGas = 0L;
    public int size = 0;
    public List<REvent> events = null;
    public boolean eventRelated = false;

    public ContractResult(Status status, JsonElement result) {
        this.status = status;
        this.result = result;
    }

    public enum Status {
        Success, Exception, Error;

        public Status merge(Status status) {
            if (status == null)
                status = Error;
            return this.ordinal() < status.ordinal() ? status : this;
        }
    }
}
