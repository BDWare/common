package org.bdware.sc.util;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.ScriptException;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileUtil {
    private static final Logger LOGGER = LogManager.getLogger(FileUtil.class);

    public static String getDir(String fullFileName) {
        return fullFileName.substring(0, fullFileName.lastIndexOf(File.separatorChar) + 1);
    }

    public static void copyFile(String src, String dst) {
        try {
            File fromFile = new File(src);
            File toFile = new File(dst);
            copyFile(fromFile, toFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(InputStream in, OutputStream out) {
        try {
            byte[] buffer = new byte[10240];
            for (int len; (len = in.read(buffer)) > 0;) {
                out.write(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(File src, File dst) {
        try {
            if (!src.exists()) {
                throw new ScriptException("src file name");
            }
            if (src.isDirectory()) {
                FileUtils.copyDirectory(src, dst);
            } else {
                FileUtils.copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileName(String string) {
        return string.substring(string.lastIndexOf("/") + 1);
    }

    public static String getFileContent(String path) {
        try {
            File file = new File(path);
            long fileLen = file.length();
            byte[] fileContent = new byte[(int) fileLen];
            FileInputStream in = new FileInputStream(file);
            LOGGER.debug("read content from " + path + ": " + in.read(fileContent));
            in.close();
            return new String(fileContent, StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.error("getContent Meets Exception: " + e.getMessage());
        }
        return null;
    }

    public static PrintStream openFileAsPrinter(String path, boolean isAppend) {
        try {
            File f = new File(path);
            if (!f.getParentFile().exists()) {
                LOGGER.trace("create directory " + path + ": " + f.getParentFile().mkdirs());
            }
            return new PrintStream(new FileOutputStream(f, isAppend));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getString(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            byte[] arr = new byte[1024];
            int l;
            while ((l = is.read(arr)) > 0) {
                bo.write(arr, 0, l);
            }
            return bo.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
