package org.bdware.sc.util;

import com.google.gson.*;

import java.io.Reader;
import java.lang.reflect.Type;

public class JsonUtil {
    public static final Gson GSON = new Gson();
    private static final Gson PRETTY_GSON = new GsonBuilder().setPrettyPrinting().create();

    public static String toJson(Object o) {
        return GSON.toJson(o);
    }

    public static <T> T fromJson(String s, Type type) {
        return GSON.fromJson(s, type);
    }

    public static <T> T fromJson(String s, Class<T> classOfT) {
        return GSON.fromJson(s, classOfT);
    }

    public static <T> T fromJson(Reader s, Type type) {
        return GSON.fromJson(s, type);
    }

    public static <T> T fromJson(Reader s, Class<T> classOfT) {
        return GSON.fromJson(s, classOfT);
    }

    public static <T> T fromJson(JsonElement je, Type type) {
        return GSON.fromJson(je, type);
    }

    public static String toPrettyJson(Object o) {
        return PRETTY_GSON.toJson(o);
    }

    public static JsonObject parseStringAsJsonObject(String json) {
        return JsonParser.parseString(json).getAsJsonObject();
    }

    public static JsonElement parseString(String json) {
        return JsonParser.parseString(json);
    }

    public static JsonObject parseReaderAsJsonObject(Reader reader) {
        return JsonParser.parseReader(reader).getAsJsonObject();
    }

    public static JsonObject parseObjectAsJsonObject(Object obj) {
        return GSON.toJsonTree(obj).getAsJsonObject();
    }

    public static JsonElement parseObject(Object obj) {
        return GSON.toJsonTree(obj);
    }
}
