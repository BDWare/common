package org.bdware.sc.util;

import java.util.LinkedList;

public class LRUList<T> {
    LinkedList<T> array = new LinkedList<T>();

    public void add(T t) {
        // TODO 似乎优化在remove这一行
        array.remove(t);
        array.addFirst(t);
    }

    public T popOldest() {
        if (array.isEmpty()) {
            return null;
        }
        return array.removeLast();
    }

    public void remove(T t) {
        if (array.size() > 0)
            array.remove(t);
    }

    public int size() {
        return array.size();
    }
}
