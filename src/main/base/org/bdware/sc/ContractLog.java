package org.bdware.sc;

public class ContractLog {
    public String date;
    public String type;
    public String value;

    public ContractLog(String k, String v) {
        String[] keys = k.split(";");
        date = keys[0];
        type = keys[1];
        value = v;
    }

}
