package org.bdware.sc.get;

import java.io.Serializable;

public class GetMessage implements Serializable {
    private static final long serialVersionUID = 1870689578034300371L;
    public String type;
    public String pkgName;
    public String method;
    public String arg;

    public GetMessage() {}

    public GetMessage(String pkgName, String method, String arg) {
        this.pkgName = pkgName;
        this.method = method;
        this.arg = arg;
    }
}
