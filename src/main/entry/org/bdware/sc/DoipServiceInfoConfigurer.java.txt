package org.bdware.sc;

import org.bdware.doip.application.DoipServiceConfig;
import org.bdware.doip.endpoint.doipServer.DoipServiceInfo;

public interface DoipServiceInfoConfigurer {
    String getServiceHandle();

    String resetServiceConfig(String configJson);

    DoipServiceConfig getDoipServiceConfig();

    DoipServiceInfo getDoipServiceInfo();
}
