package org.bdware.sc.bean;

import com.google.gson.JsonElement;
import org.bdware.sc.node.MaskConfig;
import org.bdware.sc.node.MockConfig;

public class ProjectConfig implements IDSerializable {
    String sourcePath;
    MockConfig mockConfig;
    MaskConfig maskConfig;
    // Map<String,String> mockConfig;
    // Map<String,JsonElement> maskConfig;
    String dumpPeriod;

    public ProjectConfig(String sourcePath) {
        this.sourcePath = sourcePath;
        mockConfig = null;
        maskConfig = null;
    }

    @Override
    public String getID() {
        return sourcePath;
    }

    public void setMock(String operation, JsonElement mock) {
        mockConfig.config.put(operation, mock);
    }

    public String getMock(String action) {
        if (null != mockConfig && null != mockConfig.config
                && null != mockConfig.config.get(action)) {
            return mockConfig.config.get(action).toString();
        }
        return null;
    }

    public MaskConfig getMaskConfig() {
        return this.maskConfig;
    }

    public void setMaskConfig(MaskConfig mc) {
        this.maskConfig = mc;
    }

    public MockConfig getMockConfig() {
        return this.mockConfig;
    }

    public void setMockConfig(MockConfig mc) {
        this.mockConfig = mc;
    }

    public JsonElement getMask(String action) {
        if (null != maskConfig && null != maskConfig.config) {
            return maskConfig.config.get(action);
        }
        return null;
    }

    public void setMask(String operation, JsonElement mask) {
        maskConfig.config.put(operation, mask);
    }

    public String getDumpPeriod() {
        return dumpPeriod;
    }

    public void setDumpPeriod(String dumpPeriod) {
        this.dumpPeriod = dumpPeriod;
    }
}
