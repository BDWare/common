package org.bdware.sc.bean;

import java.io.Serializable;

public interface IDSerializable extends Serializable {
    String getID();
}
