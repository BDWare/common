package org.bdware.sc.bean;

import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.util.JsonUtil;

import java.io.Serializable;

public class ForkInfo implements Serializable {
    public String funcName;

    public static ForkInfo create(AnnotationNode annotationNode, ContractNode contractNode) {
        ForkInfo info = new ForkInfo();
        if (annotationNode.getArgs().size() == 1) {
            String str = annotationNode.getArgs().get(0);
            info = JsonUtil.fromJson(str, ForkInfo.class);
        }
        System.out.println("[ForkInfo] annotationNode:" + JsonUtil.toJson(annotationNode));
        return info;
    }
}
