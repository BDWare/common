package org.bdware.sc.bean;

public class OtherNCInfo {
    public String contractID;
    public String contractName;
    public String master;

    public OtherNCInfo(String id, String name, String m) {
        contractID = id;
        contractName = name;
        master = m;
    }
}
