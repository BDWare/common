package org.bdware.sc.bean;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.Serializable;

public class SerializableJson implements Serializable {
    transient JsonElement jsonElement;
    String content;

    public SerializableJson(JsonElement jsonElement) {
        this.jsonElement = jsonElement;
        if (jsonElement != null)
            content = jsonElement.toString();
    }

    public JsonElement getJsonElement() {
        if (jsonElement != null)
            return jsonElement;
        if (content != null)
            return JsonParser.parseString(content);
        return null;
    }
}
