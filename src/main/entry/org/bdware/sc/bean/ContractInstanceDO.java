package org.bdware.sc.bean;

import java.io.Serializable;

public class ContractInstanceDO implements Serializable {
    public String id;
    public String publicKey;
    public byte[] script;

    public ContractInstanceDO(String id, String publicKey, byte[] script) {
        this.id = id;
        this.publicKey = publicKey;
        this.script = script;
    }
}
