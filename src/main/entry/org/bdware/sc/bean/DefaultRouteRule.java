package org.bdware.sc.bean;

public enum DefaultRouteRule {
    byRequester, byArgHash, byJsonPropHash, byShardingIDTree, byFunc;
}
