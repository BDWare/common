package org.bdware.sc.bean;

import org.bdware.sc.event.REvent;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.YjsType;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ContractDesp {
    public String contractID;
    public String contractName;
    public Map<String, REvent.REventSemantics> events;
    public Collection<FunctionDesp> exportedFunctions;
    public ContractExecType type;
    public YjsType yjsType;
    public List<AnnotationNode> annotations;
    public Set<String> dependentContracts;
    private volatile boolean isMaster;

    public synchronized boolean getIsMaster() {
        return isMaster;
    }

    public synchronized void setIsMaster(boolean b) {
        isMaster = b;
    }
}
