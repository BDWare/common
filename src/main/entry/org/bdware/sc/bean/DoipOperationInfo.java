package org.bdware.sc.bean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.doip.codec.operations.BasicOperations;
import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.util.JsonUtil;

public class DoipOperationInfo {
    static Logger LOGGER = LogManager.getLogger(DoipOperationInfo.class);
    public String operationType;
    public String operationName;
    public BasicOperations operation;

    // todo 对于extension进行特别处理，getArg() == 2就行？这里合理吗？参考RouteInfo貌似合理？
    public static DoipOperationInfo create(AnnotationNode annotationNode,
            ContractNode contractNode) {
        DoipOperationInfo info = new DoipOperationInfo();
        if (annotationNode.getArgs().size() == 1) {
            String str = annotationNode.getArgs().get(0);
            info = JsonUtil.fromJson(str, DoipOperationInfo.class);

            // according to operationType to find operation and fill the corresponding fields
            String opStr = info.operationType;
            for (BasicOperations op : BasicOperations.values()) {
                if (op.toString().equals(opStr)) {
                    info.operation = op;
                }
            }

            if (info.operation == null)
                info.operation = BasicOperations.Unknown;

            // If operation is not Extension, set the operationName directly from operation
            if (info.operation != BasicOperations.Extension) {
                info.operationName = info.operation.getName();
            }
        }
        LOGGER.info("[DoipOperationInfo] annotationNode:" + JsonUtil.toJson(annotationNode));
        return info;
    }
}
