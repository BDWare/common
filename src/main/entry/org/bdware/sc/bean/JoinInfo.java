package org.bdware.sc.bean;

import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.util.JsonUtil;

import java.io.Serializable;

public class JoinInfo implements Serializable {
    public DefaultJoinRule useDefault;
    public String joinFuncName;
    public int joinCount;
    public String joinCountFuncName;

    public static JoinInfo create(AnnotationNode annotationNode, ContractNode contractNode) {
        JoinInfo info = new JoinInfo();
        info.useDefault = null;
        if (annotationNode.getArgs().size() == 1) {
            String str = annotationNode.getArgs().get(0);
            info = JsonUtil.fromJson(str, JoinInfo.class);
        }
        System.out.println("[JoinInfo] annotationNode:" + JsonUtil.toJson(annotationNode));
        return info;
    }
}
