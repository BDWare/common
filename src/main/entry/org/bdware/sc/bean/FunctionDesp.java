package org.bdware.sc.bean;

import org.bdware.sc.node.AnnotationNode;

import java.io.Serializable;
import java.util.List;

public class FunctionDesp implements Serializable {
    public List<AnnotationNode> annotations;
    public String functionName;
    public RouteInfo routeInfo;
    public boolean isView;
    public JoinInfo joinInfo;


    public FunctionDesp(String name, List<AnnotationNode> annos, RouteInfo routeInfo,
            JoinInfo joinInfo, boolean isView) {
        this.functionName = name;
        this.annotations = annos;
        this.routeInfo = routeInfo;
        this.joinInfo = joinInfo;
        this.isView = isView;
    }

    public RouteInfo getRoute() {
        return routeInfo;
    }
}
