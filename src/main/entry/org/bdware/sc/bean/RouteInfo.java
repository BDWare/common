package org.bdware.sc.bean;

import org.bdware.sc.node.AnnotationNode;
import org.bdware.sc.node.ContractNode;
import org.bdware.sc.util.JsonUtil;

import java.io.Serializable;

public class RouteInfo implements Serializable {
    public DefaultRouteRule useDefault;
    // The signature of RouteFunction is
    // function myFunc(requester, sourceArg)
    public String funcName;
    public String param;

    public static RouteInfo create(AnnotationNode annotationNode, ContractNode contractNode) {
        RouteInfo info = new RouteInfo();
        info.useDefault = null;
        if (annotationNode.getArgs().size() == 1) {
            String str = annotationNode.getArgs().get(0);
            info = JsonUtil.fromJson(str, RouteInfo.class);
        }
        System.out.println("[RouteInfo] annotationNode:" + JsonUtil.toJson(annotationNode));
        return info;
    }
}
