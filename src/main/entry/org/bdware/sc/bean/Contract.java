package org.bdware.sc.bean;

import com.google.gson.JsonElement;
import org.bdware.sc.node.YjsType;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;

public class Contract implements Serializable {
    public String key;
    public ContractStartInfo startInfo = new ContractStartInfo();
    public int shardingId;
    public String sourcePath; // 用作name
    // 通过setMask实现
    public HashMap<String, JsonElement> Mask = new HashMap<>();
    ContractExecType type;
    String script;
    String owner;
    String pubkey;
    String authInfoPersistDOI;
    String nodeCenterRepoDOI;
    int doipPort;
    int numOfCopies = 1;
    boolean isDebug;
    int consensusType;
    int responseType;
    int networkType;
    long buildTime;
    boolean isConfigUnit;
    private String id;
    private String hash;
    private boolean stateful = true; // manifest可配置
    private YjsType yjsType;

    private SerializableJson createParam;
    private int remoteDebugPort = 0;

    public int getShardingId() {
        return this.shardingId;
    }

    public void setShardingId(int shardingId) {
        this.shardingId = shardingId;
    }

    public boolean getConfigUnit() {
        return isConfigUnit;
    }

    public void setConfigUnit(boolean unit) {
        this.isConfigUnit = unit;
    }

    public String getID() {
        return id;
    }

    public void setDoipPort(int doipPort) {
        this.doipPort = doipPort;
    }

    public int getDoipPort() {
        return doipPort;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getAuthInfoPersistDOI() {
        return authInfoPersistDOI;
    }

    public void setAuthInfoPersistDOI(String authInfoPersistDOI) {
        this.authInfoPersistDOI = authInfoPersistDOI;
    }

    public String getNodeCenterRepoDOI() {
        return nodeCenterRepoDOI;
    }

    public void setNodeCenterRepoDOI(String nodeCenterRepoDOI) {
        this.nodeCenterRepoDOI = nodeCenterRepoDOI;
    }

    public ContractExecType getType() {
        return type;
    }

    public void setType(ContractExecType t) {
        type = t;
    }

    public InputStream getScript() {
        return new ByteArrayInputStream(script.getBytes());
    }

    public void setScript(String str) {
        script = str;
    }

    public String getScriptStr() {
        return script;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String rsa) {
        this.key = rsa;
    }

    public int getConsensusType() {
        return this.consensusType;
    }

    public void setConsensusType(int i) {
        this.consensusType = i;
    }

    public int getNetworkType() {
        return this.networkType;
    }

    public void setNetworkType(int i) {
        this.networkType = i;
    }

    public int getResponseType() {
        return this.responseType;
    }

    public void setResponseType(int i) {
        this.responseType = i;
    }

    public String getPublicKey() {
        return pubkey;
    }

    public void setPublicKey(String pubkey) {
        this.pubkey = pubkey;
    }

    public boolean getStateful() {
        return stateful;
    }

    public void setStateful(boolean b) {
        stateful = b;
    }

    public int getNumOfCopies() {
        return numOfCopies;
    }

    public void setNumOfCopies(int size) {
        numOfCopies = size;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public void setDebug(boolean b) {
        isDebug = b;
    }

    public YjsType getYjsType() {
        return yjsType;
    }

    public void setYjsType(YjsType yjsType) {
        this.yjsType = yjsType;
    }

    public void setMask(String FunctionName, JsonElement Mask) {
        this.Mask.put(FunctionName, Mask);
    }

    public long getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(long buildTime) {
        this.buildTime = buildTime;
    }

    public void setCreateParam(JsonElement ele) {
        createParam = new SerializableJson(ele);
    }

    public void setRemoteDebugPort(int port) {
        remoteDebugPort = port;
    }

    public JsonElement getCreateParam() {
        if (createParam != null)
            return createParam.getJsonElement();
        return null;
    }

    public int getRemoteDebugPort() {
        return remoteDebugPort;
    }
}
