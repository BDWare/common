package org.bdware.sc.bean;

import java.io.Serializable;

public enum ContractExecType implements Serializable {
    Sole, // 单点锚定合约
    RequestOnce, // 多点不同步合约
    ResponseOnce, // 多点不同步合约，超时的时候再请求一个, 直到获得一个response
    RequestAllResponseFirst, RequestAllResponseHalf, RequestAllResponseAll, Sharding, // 分片执行模式
    SelfAdaptiveSharding, // self-adaptive sharding
    PBFT, // PBFT
    RAFT;


    public static ContractExecType getContractTypeByInt(int i) {
        ContractExecType[] values = ContractExecType.values();
        if (i < 0 || i >= values.length) {
            return Sole;
        }
        return ContractExecType.values()[i];
    }

    public boolean isUnit() {
        return this != Sole;
    }

    public boolean needSeq() {
        switch (this) {
            case Sole:
            case RequestOnce:
            case ResponseOnce:
            case Sharding:
            case SelfAdaptiveSharding:
                return false;
            default:
                break;
        }
        return true;
    }
}
