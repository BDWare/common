package org.bdware.sc.bean;

import java.io.Serializable;

public class ContractStartInfo implements Serializable {
    public boolean isYPK = false;
    public boolean isPrivate = false;
    public String pubKeyPath;
    public String ypkName;

    public ContractStartInfo() {}

    public ContractStartInfo(boolean b1, boolean b2, String path, String name) {
        isYPK = b1;
        isPrivate = b2;
        pubKeyPath = path;
        ypkName = name;
    }

    public String toString() {
        return String.format(
                "isYPK=%s\n\tisPrivate=%s\n\tpubKeyPath=%s\n\typkName=%s (ContractStartInfo)",
                isYPK, isPrivate, (pubKeyPath == null ? "null" : pubKeyPath),
                (ypkName == null ? "null" : ypkName));
    }
}
