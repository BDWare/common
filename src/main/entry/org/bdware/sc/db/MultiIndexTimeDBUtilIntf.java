package org.bdware.sc.db;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public interface MultiIndexTimeDBUtilIntf {
    List<JsonObject> queryByDateAsJson(String label, long date, long end);

    long size(String label);

    List<JsonObject> queryByOffset(String label, long offset, int count);

    JsonArray countInInterval(String str, long startTime, long interval, long endTime);
}
