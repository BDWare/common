package org.bdware.sc.db;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class FIFOCache<T, V> {
    Map<T, LinkedList<V>> cache;
    int limit;

    public FIFOCache(int limit) {
        cache = new HashMap<>();
        this.limit = limit;
    }

    public void add(T key, V val) {
        LinkedList<V> list = null;
        if (cache.containsKey(key)) {
            list = cache.get(key);
        } else {
            list = new LinkedList<>();
            cache.put(key, list);
        }
        list.addFirst(val);
        if (list.size() > limit) {
            list.removeLast();
        }
    }

    public LinkedList<V> getValues(T key) {
        return cache.get(key);
    }

    public Map<T, LinkedList<V>> getCache() {
        return cache;
    }

    public void remove(T key) {
        cache.remove(key);
    }

    public void clear() {
        cache.clear();
    }
}
