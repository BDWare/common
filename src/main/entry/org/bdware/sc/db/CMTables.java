package org.bdware.sc.db;

public enum CMTables {
    LocalNodeLogDB, LocalContractLogDB, NodeRole, LockedUser, NodeTime, ApplyRole, ApplyTime, ConfigDB, ContractInfo, EventRegistry, LastExeSeq, UnitContracts, CheckPointLastHash, ProjectConfig, BCODB;

    public String toString() {
        return "CM_" + super.name();
    }
}
