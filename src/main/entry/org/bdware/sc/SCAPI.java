package org.bdware.sc;

import org.bdware.sc.bean.Contract;
import org.bdware.sc.bean.ContractExecType;
import org.bdware.sc.bean.ContractRequest;
import org.bdware.sc.conn.SocketGet;
import org.bdware.sc.util.JsonUtil;

public class SCAPI {
    public static void main(String[] args) {
        if (null == args || args.length == 0) {
            args = new String[] {""};
        }
        switch (args[0]) {
            case "exit":
            case "execContract":
            case "startContract":
            case "requestContract":
            case "stopContract":
            case "stopAllContracts":
            case "listContracts":
                SocketGet get = new SocketGet("127.0.0.1", 1615);

                if (args.length > 1)
                    System.out.println(get.syncGet("dd", args[0], args[1]));
                else
                    System.out.println(get.syncGet("dd", args[0], "{}"));

                break;
            default:
                get = new SocketGet("127.0.0.1", 18000);

                System.out.println("Usage: method arg");
                System.out.println(get.syncGet("dd", "help", ""));
                System.out.println("=========Sample1===========");
                System.out.println(
                        "java -classpath yjs.jar com.yancloud.sc.SCAPI startContract \"{\\\"type\\\":\\\"Data\\\",\\\"id\\\":\\\"656564\\\"}\\\"");
                System.out.println(
                        "java -classpath yjs.jar com.yancloud.sc.SCAPI execContract \"{\\\"arg\\\":\\\"http://www.baidu.com\\\",\\\"contractID\\\":\\\"656564\\\"}\"");
                System.out.println("=========Sample2===========");
                System.out.println(
                        "java -classpath yjs.jar com.yancloud.sc.SCAPI startContract \"{\\\"type\\\":\\\"Algorigthm\\\",\\\"id\\\":\\\"656565\\\",\\\"script\\\":\\\"contract c{function main(arg){return arg/1.0+1;}}\\\"}\"");
                System.out.println(
                        "java -classpath yjs.jar com.yancloud.sc.SCAPI execContract \"{\\\"arg\\\":\\\"101\\\",\\\"contractID\\\":\\\"656565\\\"}\"");

        }
    }

    /*
     * private static String getAlgorithmSample() { Contract c = new Contract(); c.setID("656565");
     * c.setType(Type.Algorithm); c.setScript("contract c{function main(arg){return arg+1;}}");
     * return JsonUtil.toJson(c);
     * 
     * }
     * 
     * private static String getDataSample() { Contract c = new Contract(); c.setID("656564");
     * c.setType(Type.Data); return JsonUtil.toJson(c); }
     */

    private static String getFixedSample() {
        Contract c = new Contract();
        c.setID("656565");
        c.setType(ContractExecType.Sole);
        c.setScript("contract c{function main(arg){return arg+1;}}");
        return JsonUtil.toJson(c);

    }


    private static void approveContract(String json) {
        try {
            SocketGet get = new SocketGet("127.0.0.1", 1615);
            ContractRequest app = JsonUtil.fromJson(json, ContractRequest.class);
            System.out.println(get.syncGet("dd", "approveContract", JsonUtil.toJson(app)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void startContract(String json) {
        try {
            SocketGet get = new SocketGet("127.0.0.1", 1615);
            Contract c = JsonUtil.fromJson(json, Contract.class);
            System.out.println(get.syncGet("dd", "startContract", JsonUtil.toJson(c)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
