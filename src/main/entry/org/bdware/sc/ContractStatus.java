package org.bdware.sc;

enum ContractStatus {
    Ready, Executing, Executed
}
