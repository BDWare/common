package org.bdware.sc;

import com.google.gson.JsonObject;
import org.zz.gmhelper.SM2KeyPair;

public interface NodeCenterConn {
    String[] listNodes();

    String getNodeId();

    String routeContract(String contractID);

    String reRouteContract(String contractID);

    void sendMsg(String msg);

    SM2KeyPair getNodeKeyPair();

    JsonObject checkIsContract(String requestID, String json);

    class Response {
        String responseID;
        String action;
        Object data;
        long executeTime;
    }
}
