package org.bdware.sc.bean.test;

import org.bdware.sc.bean.ContractExecType;
import org.bdware.sc.node.ContractManifest;
import org.bdware.sc.util.JsonUtil;

public class EnumDecodeTest2 {
    public static void main(String[] str) {
        ContractManifest cm = new ContractManifest();
        // cm.setType(Contract.Type.Algorithm);
        cm.setType(ContractExecType.Sole);
        System.out.println(JsonUtil.toJson(cm));
    }
}
