package org.bdware.sc.bean;

import org.bdware.sc.util.JsonUtil;
import org.zz.gmhelper.SM2KeyPair;

public class ContractRequestTest {
    public static void main(String[] args) {
        ContractRequest req = new ContractRequest();
        // req.requester = rsa.toBase64Pubkey();
        req.contractID = "123";
        req.action = "main";
        req.setArg("arg1");
        String key =
                "{\"privateKey\":\"589d94ee5688358a1c5c18430dd9c75097ddddebf769f139da36a807911d20f8\","
                        + "\"publicKey\":\"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5a"
                        + "d88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd\"}";
        SM2KeyPair pair = SM2KeyPair.fromJson(key);
        req.doSignature(pair);
        System.out.println(req.verifySignature());
        System.out.println(JsonUtil.toJson(req));
        System.out.println(req.verifySignature());
    }
}
