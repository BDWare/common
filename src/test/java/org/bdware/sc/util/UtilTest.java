package org.bdware.sc.util;

import com.google.gson.JsonObject;
import org.junit.Assert;
import org.junit.Test;


public class UtilTest {
    @Test
    public void testGetDir() {
        String fullPath = "123/456";
        String dirPath = FileUtil.getDir(fullPath);
        Assert.assertEquals("123/", dirPath);
    }

    @Test
    public void testJson() {
        JsonObject jo = new JsonObject();
        jo.addProperty("dafa", "123");
        System.out.println(jo.toString());
    }
}
