package org.bdware.sc.http.test;

import org.bdware.sc.http.HttpPostForm;

import java.util.HashMap;
import java.util.Map;

public class HttpPostFormTest {
    public static void main(String[] args) {
        String url = "http://47.106.38.23:13081/register";

        Map<String, String> params = new HashMap<>();
        String sucFix = System.currentTimeMillis() + "_ContractLog";
        params.put("DOI", "86.5000.470/" + sucFix);
        params.put("Description", "Contract Log");
        params.put("Interface", "");
        params.put("Address", "");
        params.put("PublicKey", "");
        params.put("Signature", "");
        // http://47.106.38.23:8080/idsystem/doDetail.html?doi=86.5000.470/1570626378959_ContractLog
        HttpPostForm.postForm(url, params);
    }
}
