package org.bdware.sc.encrypt.test;

import org.bdware.sc.conn.ByteUtil;
import org.bdware.sc.encrypt.RSA;

public class RSATest {
    public static void main(String[] args) {
        RSA rsa = RSA.generate();
        String base64 = rsa.toBase64();
        String pubBase64 = rsa.toBase64Pubkey();
        System.out.println("======Privkey");
        System.out.println(base64);
        System.out.println("======Pubkey");
        System.out.println(pubBase64);
        System.out.println("======Done");
        rsa = RSA.generateFromBase64(pubBase64);
        byte[] data = "hafasdfadsfas".getBytes();
        assert rsa != null;
        byte[] encodeData = rsa.encode(data);
        System.out.println(ByteUtil.encodeBASE64(encodeData).replaceAll("\n", ""));
        System.out.println(new String(rsa.decode(encodeData)) + "|||");
        // RSA rsa2 = generateFromBase64(base64);
        // System.out.println(rsa.e2 + "\n" + rsa2.e2);
    }
}
