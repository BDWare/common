package org.bdware.sc.units;

import org.bdware.sc.conn.ByteUtil;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URLEncoder;

public class ByteArrayTest {
    @Test
    public void timeOutAllFalse() throws InterruptedException {
        float i = 116.316801F;
        Constructor c = null;
        Method m = null;

        // 11100111 01001000 11011110 00101100 11110010 01001110 10110110 00010011
        System.out.println(Integer.toBinaryString(Float.floatToIntBits(i)));
    }

    @Test
    public void hexStringToBase64() {
        // http://59.110.125.211:21056/v0/ledgers/bdcontract/transaction?hash=pD587F%2F5R64efno9%2FbAiI%2FSv6BA%3D
        String str = "a43e7cec5ff947ae1e7e7a3dfdb02223f4afe810";
        System.out.println(URLEncoder.encode(ByteUtil.encodeBASE64(ByteUtils.fromHexString(str))));
    }

    @Test
    public void base64ToUtf8() {
        String str =
                "MTYyNTEyNTI3ODY4OSAtLT4geyJleHRyYUdhcyI6IjAiLCJ0b3RhbEdhcyI6IjAiLCJleGVjdXRpb25HYXMiOiIwIiwic2lnbmF0dXJlIjoiMzA0NTAyMjEwMGVlMzczMWE2NmU4N2NmZGIyZDBjNTA0YWQ2ZjE4ZTQzZDI5NTZjOTZkMjE3ZDY0NmQ0ZjViMzAzZjRkZTBlODEwMjIwMjkxMDcwYTEwMzU2OWE5Y2Q3OGU2YTRjNTExMjQ1MzVkZDliZjY3OGMyYzcwMzk1NzdmYTRlZjVjMDBkYjNmYiIsImNvc3RUaW1lIjoiNjMiLCJhcmciOiLnjokiLCJjb250cmFjdElEIjoiLTE4NjMxMjQ5ODEiLCJhY3Rpb24iOiJzZWFyY2giLCJwdWJLZXkiOiIwNGQxOTI0MzI5ZjcyY2VkMTQ4ZjZmMzMzZmI5ODVjY2JhYTMxYjFlM2FhY2YxMGJlNWY0M2Q0YTRmZjVhZDg4ODk5YTAwNWU3OWUzN2ZjMDY5OTNlMWQ2NmFkYThjZjhiNzExY2IzNmY1OTUzOGJiN2QzZTM5ZTcwZmE5MzYwZGRkIn0=+S6uuWRmFxcXCIsXFxcImNvdW50XFxcIjoyfSIsImNvbnRyYWN0SUQiOiItMTE1Mzg4NTk3NiIsImFjdGlvbiI6ImF1dGhVc2VyIiwicHViS2V5IjoiMDRkMTkyNDMyOWY3MmNlZDE0OGY2ZjMzM2ZiOTg1Y2NiYWEzMWIxZTNhYWNmMTBiZTVmNDNkNGE0ZmY1YWQ4ODg5OWEwMDVlNzllMzdmYzA2OTkzZTFkNjZhZGE4Y2Y4YjcxMWNiMzZmNTk1MzhiYjdkM2UzOWU3MGZhOTM2MGRkZCJ9";
        System.out.println(new String(ByteUtil.decodeBASE64(str)));
    }
}
