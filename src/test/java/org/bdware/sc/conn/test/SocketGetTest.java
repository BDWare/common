package org.bdware.sc.conn.test;

import org.bdware.sc.conn.SocketGet;

public class SocketGetTest {

    public static void main(String[] args) {
        System.out.println("开始测试 : ");
        int startPort = Integer.parseInt(args[0]);
        int endPort = Integer.parseInt(args[1]);
        for (int i = startPort; i < endPort; i++) {
            SocketGet get = new SocketGet("127.0.0.1", startPort);
            String cpCMI = get.syncGet("", "getContract", "");
            System.out.println(i + "-> 测试结果为 : " + cpCMI);
        }
    }
}
