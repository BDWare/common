package org.bdware.sc.test;

import org.bdware.sc.conn.SocketGet;

public class ContractProcessInfoLogger {
    public static void main(String[] args) {
        System.out.println("开始测试 : ");
        int startPort, endPort;
        if (args != null && args.length > 1) {
            startPort = Integer.parseInt(args[0]);
            endPort = Integer.parseInt(args[1]);
        } else {
            startPort = 1915;
            endPort = 1918;
        }
        for (int i = startPort; i < endPort; i++) {
            try {
                SocketGet get = new SocketGet("127.0.0.1", i);
                String cpCMI = get.syncGet("", "getContract", "");
                System.out.println("端口" + i + "-> 测试结果为 : " + cpCMI);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
