package org.bdware.sc.test;

import com.sleepycat.je.DatabaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.sc.Jedion;

import java.io.File;
import java.util.ArrayList;

public class JedionTest {
    private static final Logger LOGGER = LogManager.getLogger(JedionTest.class);

    public static void main(String[] args) throws DatabaseException {
        Jedion edion = new Jedion("ABCInstrument");
        File file = new File("/Applications/jee-2019-03/Eclipse.app/Contents/MacOS/ContractDB");
        LOGGER.info(file.getAbsolutePath());
        if (!file.exists()) {
            LOGGER.info("create directory " + file.getAbsolutePath() + ": " + file.mkdirs());
        }
        edion.configEnvironment(file);
        edion.createDatabase();
        ArrayList<String> keys = edion.getAllFromDatabase();

        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            LOGGER.info("i = " + i + " ; key = " + key);
            // edion.deleteFromDatabase(key);
            String value = edion.readFromDatabase(key);
            LOGGER.info("value = " + value);
        }

        edion.closeDatabase();
    }
}
