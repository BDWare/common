package org.bdware.sc.test;

import org.bdware.sc.YJSPacker;

public class YJSPackerTest {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: inputDir output");
            System.out.println("\tExample: ./testjs/ ./output/out.zip");
            return;
        }
        String testInput = args[0];
        String target = args[1];

        YJSPacker.pack(testInput, target);
    }
}
