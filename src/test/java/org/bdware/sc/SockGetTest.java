package org.bdware.sc;

import org.bdware.sc.conn.SocketGet;
import org.junit.Test;

public class SockGetTest {
    @Test
    public void test() {
        SocketGet get = new SocketGet("127.0.0.1", 18000);
        System.out.println("Start");
        long start = System.currentTimeMillis();
        String re = get.syncGet("", "isContractProcess", "");
        System.out.println(re);
        System.out.println("[Takes]" + (System.currentTimeMillis() - start));
    }
}
