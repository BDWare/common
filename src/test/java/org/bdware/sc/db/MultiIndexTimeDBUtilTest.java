package org.bdware.sc.db;

import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MultiIndexTimeDBUtilTest {
    private MultiIndexTimeDBUtil db;

    @Before
    public void init() {
        db = new MultiIndexTimeDBUtil("/Users/huaqiancai/lab/BDM/temp/", "CM_LocalContractLogDB");
    }

    @Test
    public void test() {
        long start = System.currentTimeMillis() - 10 * 24 * 3600 * 1000L;
        for (; start < System.currentTimeMillis(); start += 24 * 3600 * 1000)
            System.out.println(db.queryOffset("TakeoutTianjinRate", start));
        System.out.println(db.size("TakeoutTianjinRate"));
        List<JsonObject> list = db.queryByDateAsJson("TakeoutTianjinRate",
                System.currentTimeMillis() - 10 * 24 * 3600 * 1000L, System.currentTimeMillis());
        for (JsonObject jo : list)
            System.out.println(jo.toString());
    }
}
