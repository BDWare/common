package org.bdware.sc.boundary.utils;

import org.bdware.sc.bean.ContractRequest;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import org.junit.Test;
import org.zz.gmhelper.BCECUtil;
import org.zz.gmhelper.SM2KeyPair;
import org.zz.gmhelper.SM2Util;

import java.math.BigInteger;
import java.security.KeyPair;

public class SM2UtilTest {
    String keyPairStr =
            "{\"publicKey\":\"0442e39c0b1623e6f95ef896d37f41aa3cd98fbc3aea71250eedba4335c6c3aff51b5fa2352f920d1063b50a30738427c18375df886985a63e0f496293fdad22a8\",\"privateKey\":\"b7e504cf951792554bf4a9874be37cb1279d38939444388e2046ee9603b4f0c4\"}";
    SM2KeyPair keyPair = SM2KeyPair.fromJson(keyPairStr);

    @Test
    public void loadKey() throws Exception {
        String keyStr =
                "{\"privateKey\": \"b4ab74a0d8691fc45fd214bdaf45b1dc6ac288a93d1518599fc824ab29310a1a\",\n"
                        + "  \"publicKey\": \"04fef570617a3a2dfba6a4dd25050919266735f683f69aee8df4cd600f34b7440a26e90af3b92b63bad7c013f32c2665599ddde33c87736efa667eb027cdd8572f\"\n"
                        + " }";
        SM2KeyPair keyPair = SM2KeyPair.fromJson(keyStr);
        byte[] result = SM2Util.sign(keyPair.getPrivateKeyParameter(), "abc".getBytes());
        System.out.println(result.length);
    }

    @Test
    public void verify2() {
        String toVerify =
                "GeneralContract|plainTextTransmission|{\"ledger\":\"test\",\"data\":\"上链信息为json格式\"}|04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7c0a5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15";
        String sign =
                "30440220137771d7505abde700c89cfa376f67c564dfdec6ffdb08cb08ff70544be6cb6f0220587296439bedf43f4b0bfec2d7030a8250d369e53af623e50a734e3b6cb780cb";
        String pubkey =
                "04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7c0a5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15";
        ContractRequest cr = new ContractRequest();
        cr.setContractID("GeneralContract");
        cr.setAction("plainTextTransmission");
        cr.setArg("{\"ledger\":\"test\",\"data\":\"上链信息为json格式\"}");
        cr.setPublicKey(
                "04398dfde44290595cd098cd2f904b36367c69f9011719d43fb0955f823cf1386764769bc7c0a5649dcb316d552998a5c106afd268d9db8b6482ce527544a7bd15");
        cr.setSignature(
                "30440220137771d7505abde700c89cfa376f67c564dfdec6ffdb08cb08ff70544be6cb6f0220587296439bedf43f4b0bfec2d7030a8250d369e53af623e50a734e3b6cb780cb");

        System.out.println(cr.verifySignature());
    }

    @Test
    public void base64() {
        String gstr =
                "09146332716e7767327923056946406e49570e5909146332716e7767327923056946406e49570e59";
        byte[] bytes = ByteUtils.fromHexString(gstr);
        // String str = new BASE64Encoder().encode(bytes);
        // System.out.println(str);
    }

    @Test
    public void genKey() throws Exception {
        // SM2KeyPair pair = new SM2().generateKeyPair();
        AsymmetricCipherKeyPair keyy = SM2Util.generateKeyPairParameter();
        KeyPair key = SM2Util.generateKeyPair(null);
        BCECPrivateKey priKey = (BCECPrivateKey) key.getPrivate();
        BCECPublicKey pubKey = (BCECPublicKey) key.getPublic();
        priKey.getD();
        pubKey.getQ();
        System.out.println(key.getPrivate().getEncoded() + "-->" + key.getPublic().getEncoded());
        // System.out.println(pair.toJson());
    }

    @Test
    public void verify() throws Exception {
        String signature =
                "30440220694f00ae34a1d68ca3f6c54876eead4e4b830edfe5836b9910fd5b47704bab1f022020e17c5ec642e74c4a3e0845580983e771d856ad4bbd641166e0b693f7ae78b7";
        signature =
                "3045022053d6dbb2f79c8f018b7f2861685ca289a5e3b85d572e0b1a8db8bb2b75345839022100de5a95e0b35896df187fb5659c4237b8c87ab7c38aa29edb548c94ff2f0e0ce9";
        String signa =
                "66f0a2ed730f91a3f6c18d800da83e8cb21561f1a51dc649ae75fb006b6c0a4d60169ecae2b8df16a10dd55dcc93155c5d24c3f7c0667971f5d28ea650e25ecc";
        byte[] signb = ByteUtils.fromHexString(signa);
        signb = SM2Util.encodeSM2SignToDER(signb);
        signature = ByteUtils.toHexString(signb);
        signature =
                "30450221009aa04608d432dc7865274ac02e7e92b93a8a08e07bdc9678966229b15f26ff570220244c464e2c82533e280c4003ab97347d63c90052fb18b8a917a1c9ca4ed2985a";

        String toVerify =
                "DistributeContract|DBMMerger_2020-12-23-14:01:11_Auto.ypk|04f5c7cc4b202047da7aff4b93e66204d8e04d0e26435e792afcfb8b4bf918b9b4ec8550e669fb1581203fcd7187ea993391d6c3ee6e348ee17febee3bdad783c3";
        String keyPair =
                "{ \"privateKey\": \"347caf3393482440cf5a79995b2c83f567644dfa9b4804755bd093141542db96\", \"publicKey\": \"04f5c7cc4b202047da7aff4b93e66204d8e04d0e26435e792afcfb8b4bf918b9b4ec8550e669fb1581203fcd7187ea993391d6c3ee6e348ee17febee3bdad783c3\" }";
        String pubKey =
                "04f5c7cc4b202047da7aff4b93e66204d8e04d0e26435e792afcfb8b4bf918b9b4ec8550e669fb1581203fcd7187ea993391d6c3ee6e348ee17febee3bdad783c3";
        boolean ret = SM2Util.plainStrVerify(pubKey, toVerify, signature);
        System.out.println("ret:" + ret);
    }

    @Test
    public void boundrySign() throws Exception {
        String content = "hello";

        BigInteger privKey = new BigInteger(
                "b7e504cf951792554bf4a9874be37cb1279d38939444388e2046ee9603b4f0c4", 16);
        ECPrivateKeyParameters priKey =
                new ECPrivateKeyParameters(privKey, org.zz.gmhelper.SM2Util.DOMAIN_PARAMS);
        byte[] sign = org.zz.gmhelper.SM2Util.sign(priKey, content.getBytes());
        sign = org.zz.gmhelper.SM2Util.decodeDERSM2Sign(sign);

        System.out.println(ByteUtils.toHexString(sign));
    }

    @Test
    public void boundryVerify() throws Exception {
        String content = "e8dffc921bf41ec8b96c8a7bfc18b0530c667c23";
        String signature =
                "480c60524351826a080b16ffee13b84d84b55d036a31e81e11c0238c5946e2e676c7560575825a59fe76d40703ba9a309b409c6494824e1c98b0bc46f1d754bf";
        String pubKeyStr =
                "0428083afc0d61b10790560ed15963f7ecca5f6c0b896b5379029f8d09936bbc379efed9c1599d40a447b360e17ecbe0b18ed05cc0a1875ca1767be6952cc9c0b5";
        byte[] sig = ByteUtils.fromHexString(signature);
        sig = SM2Util.encodeSM2SignToDER(sig);
        ECPublicKeyParameters pubKey = BCECUtil.createECPublicKeyFromStrParameters(pubKeyStr,
                org.zz.gmhelper.SM2Util.CURVE, org.zz.gmhelper.SM2Util.DOMAIN_PARAMS);
        boolean value = org.zz.gmhelper.SM2Util.verify(pubKey, content.getBytes(), sig);
        System.out.println(value);
    }

    @Test
    public void encrypt() throws Exception {
        String pubkey =
                "0417b2fd4174ca9dcc5c5fa315febc5ea2488676f746e127fafb50a3b4c31daad863994775cac6f6139f0d6400c94dbeeb11a6962eaf8176fced641d69b475d446";
        String data =
                "dafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdfdafdsafasfasdf";
        byte[] data2 = SM2Util.encrypt(SM2KeyPair.publicKeyStr2ECPoint(pubkey), data.getBytes());

        System.out.println(ByteUtils.toHexString(data2));
    }

    @Test
    public void decrypt() throws Exception {


    }

    @Test
    public void dosignTest() throws Exception {
        String tosign =
                "action=startContract&script=contract%20shortc%7B%0A%09export%20function%20main(arg)%7B%0A%09%09return%20arg.length%3B%09%0A%09%7D%0A%7D&pubKey=041a3875bb1a8d6acfc1142c60e3cc7c11831d4878c6dbbf9bb0929716d6cf402492b9629a4ced9198ae2c419f8d3d6ae810a431458d38901e5597636b715584d4";
        // keyPair =
        // SM2KeyPair.fromJson("{\"privateKey\":\"94728f3d95f19b3b5b11a31fdb603b5b2e8b377d9520d0680ac4f270cc82ec51\",\"publicKey\":\"041a3875bb1a8d6acfc1142c60e3cc7c11831d4878c6dbbf9bb0929716d6cf402492b9629a4ced9198ae2c419f8d3d6ae810a431458d38901e5597636b715584d4\"}");
        keyPair = SM2KeyPair.fromJson(
                "{\"privateKey\":\"589d94ee5688358a1c5c18430dd9c75097ddddebf769f139da36a807911d20f8\",\"publicKey\":\"04d1924329f72ced148f6f333fb985ccbaa31b1e3aacf10be5f43d4a4ff5ad88899a005e79e37fc06993e1d66ada8cf8b711cb36f59538bb7d3e39e70fa9360ddd\"}");
        String signature = ByteUtils
                .toHexString(SM2Util.sign(keyPair.getPrivateKeyParameter(), tosign.getBytes()));
        System.out.println(signature);
        ECPublicKeyParameters pubKey = BCECUtil.createECPublicKeyFromStrParameters(
                keyPair.getPublicKeyStr(), SM2Util.CURVE, SM2Util.DOMAIN_PARAMS);
        boolean verify =
                SM2Util.verify(pubKey, tosign.getBytes(), ByteUtils.fromHexString(signature));
        System.out.println(verify);


    }
    // Deprecated
    // @Test
    // public void signAtServer() {
    // SM2 sm2 = new SM2();
    // SM2.Signature signature = sm2.sign("Hello".getBytes(), keyPair.getPrivateKey());
    // System.out.println(signature.toString());
    // }
    //
    // @Test
    // public void verifyServerToServer() {
    // String str =
    //
    // "d58ca7ade729ccaf36e8d2d22990a8e2e8d7242979e92c9d80d8abcf10924566,f3888664ed5e0fc850285b1c05f8c042497893a21939b64919dc861b382776a3";
    // SM2 sm2 = new SM2();
    // boolean ret =
    // sm2.verify("Hello", SM2.Signature.loadFromString(str), "",
    // keyPair.getPublicKey());
    // System.out.println("[VerifyServer] " + ret);
    // }
    //
    // @Test
    // public void verifyClientToServer() {
    // String sig =
    //
    // "11d9c57d8ff6db5a8662dd91540f60666c3fa50a5ef47c6529fef15e64485f155d8747455b97cff636282758aa00c5245f8de92fb4cbb280e7d9d23c1907892c";
    // SM2 sm2 = new SM2();
    // boolean ret =
    // sm2.verify("Hello", SM2.Signature.loadFromString(sig), "",
    // keyPair.getPublicKey());
    // System.out.println("[VerifyClientToServer] " + ret);
    // }
}
