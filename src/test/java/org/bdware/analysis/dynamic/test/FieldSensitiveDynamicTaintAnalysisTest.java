package org.bdware.analysis.dynamic.test;

import org.bdware.analysis.dynamic.FieldSensitiveDynamicTaintAnalysis;
import org.bdware.analysis.dynamic.TracedFile;
import org.bdware.analysis.taint.TaintCFG;
import org.bdware.analysis.taint.TaintResult;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.FileInputStream;

public class FieldSensitiveDynamicTaintAnalysisTest {
    public static void main(String[] args) throws Exception {
        String clzPath =
                "./output/traceTest/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$7$23A$contract_main_yjs7.class";
        String tracePath =
                "/Users/hulingxuan/git/SmartContract/output/traceTest/t_1554113218925.trace";
        // "/Users/damei/Documents/Project/DataContract/SmartContract/output/traceTest/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$5$23A$contract_main_yjs5.class";
        ClassReader cr = new ClassReader(new FileInputStream(clzPath));
        ClassNode cn = new ClassNode();
        TaintResult.nLocals = 15;
        TaintResult.nStack = 5;
        cr.accept(cn, ClassReader.EXPAND_FRAMES);
        for (MethodNode mn : cn.methods) {
            if (mn.name.equals("statAge")) {
                System.out.println("[NaiveTaintAnalysis] Matched Method:" + mn.name + mn.desc);
                TaintCFG cfg = new TaintCFG(mn);
                TracedFile tf = new TracedFile(new FileInputStream(tracePath));
                TaintResult.printer.setLabelOrder(cfg.getLabelOrder());
                FieldSensitiveDynamicTaintAnalysis analysis =
                        new FieldSensitiveDynamicTaintAnalysis(cfg, tf);
                analysis.analysis();
                cfg.printSelf();
            }
        }
    }
}
