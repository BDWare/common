package org.bdware.analysis.test;

import org.bdware.analysis.BasicBlock;
import org.bdware.analysis.CFGraph;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.FileInputStream;

public class CFGraphTest {
    public static void main(String[] args) throws Exception {
        // String clzFile =
        // "./input/dumpedClz/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$3$20A$contract_main_yjs3.class";
        String clzFile =
                "/Users/hulingxuan/git/SmartContract/output/traceTest/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$7$23A$contract_main_yjs7.class";
        // String clzFile =
        // "/Users/huaqiancai/java_workspace/SmartContract/output/traceTest_1552663123542/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$5$23A$contract_main_yjs5.class";
        ClassNode cn = new ClassNode();
        ClassReader cr = new ClassReader(new FileInputStream(clzFile));
        cr.accept(cn, ClassReader.EXPAND_FRAMES);
        for (MethodNode method : cn.methods) {
            if (!method.name.equals("statAge"))
                continue;
            CFGraph cf = new CFGraph(method) {
                @Override
                public BasicBlock getBasicBlock(int id) {
                    return new BasicBlock(id);
                }
            };
            cf.printSelf();
        }
    }
}
