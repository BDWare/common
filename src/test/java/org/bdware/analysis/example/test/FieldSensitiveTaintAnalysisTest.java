package org.bdware.analysis.example.test;

import org.bdware.analysis.example.FieldSensitiveTaintAnalysis;
import org.bdware.analysis.taint.TaintCFG;
import org.bdware.analysis.taint.TaintResult;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.FileInputStream;

public class FieldSensitiveTaintAnalysisTest {
    public static void main(String[] args) throws Exception {
        String clzPath =
                "./output/traceTest/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$7$23A$contract_main_yjs7.class";
        // String clzPath =
        // "/Users/damei/Documents/Project/DataContract/SmartContract/output/traceTest/wrp/jdk/nashorn/internal/scripts/Script$Recompilation$5$23A$contract_main_yjs5.class";
        ClassReader cr = new ClassReader(new FileInputStream(clzPath));
        ClassNode cn = new ClassNode();
        TaintResult.nLocals = 15; // ���þֲ�������Ĵ�С
        TaintResult.nStack = 5; // ���ò�����ջ�Ĵ�С
        cr.accept(cn, ClassReader.EXPAND_FRAMES);
        for (MethodNode mn : cn.methods) {
            if (mn.name.equals("statAge")) {
                System.out.println("Matched Method:" + mn.name + mn.desc);
                TaintCFG cfg = new TaintCFG(mn);
                TaintResult.printer.setLabelOrder(cfg.getLabelOrder());
                // ���ù��캯��ʵ���������࣬��ʼ��preResult����ʱanalysis����Ϊһ�����б��洢B0
                FieldSensitiveTaintAnalysis analysis = new FieldSensitiveTaintAnalysis(cfg);
                // ����BreadthFirstSearch��ķ�����������ʱ���е�toAnalysis�Ѿ���analysis����
                analysis.analysis();
                // ��getLocal(i)����getStack(i)Ϊnull�����ӡ0-
                cfg.printSelf();
            }
        }
    }
}
