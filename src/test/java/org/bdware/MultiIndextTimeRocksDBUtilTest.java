package org.bdware;

import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bdware.sc.db.MultiIndexTimeRocksDBUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MultiIndextTimeRocksDBUtilTest {
    static Logger LOGGER = LogManager.getLogger(MultiIndextTimeRocksDBUtilTest.class);
    MultiIndexTimeRocksDBUtil util;

    @Before
    public void init() {
        util = new MultiIndexTimeRocksDBUtil("testoutput/timeIndexDB", "defaultTable");
    }

    @Test
    public void put() {
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            util.put("abc_123", "ddd " + System.currentTimeMillis());
        }
    }

    @Test
    public void listAll() {
        List<JsonObject> tt = util.queryByOffset(null, 0, 100);
        for (int i = 0; i < tt.size(); i++) {
            long val = tt.get(i).get("timestamp").getAsLong();
            LOGGER.info(String.format("pre %d cu %d suc %d", util.queryOffset("abc_123", val - 1L),
                    util.queryOffset("abc_123", val), util.queryOffset("abc_123", val + 1L),
                    util.queryOffset("abc_123", val - 1L)));
        }

    }

    @Test
    public void run() {
        LOGGER.info(util.queryOffset("abc_123", 1641454140657L));
        LOGGER.info(util.queryOffset("abc_123", 1641454140669L));
        LOGGER.info(util.queryOffset("abc_123", 1641450028482L));
        LOGGER.info(util.queryOffset("abc_123", 1641450028483L));
    }
}
